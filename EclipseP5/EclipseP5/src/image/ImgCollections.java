package image;

import helpers.ImageNaming;

import java.io.File;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;

import processing.core.PImage;
import rap.RAP;
import rap.RAPUser;

import com.google.common.base.Predicate;

public class ImgCollections implements RAPUser {
	
	public static RAP		rap			= RAP.getRAP(ImgCollections.class);
	
	public static boolean	fullPath	= false;
	
	/**
	 * last folder
	 */
	private static File		folder;
	
	public File getLastFolder() {
		return folder;
	}
	
	/**
	 * retrieves all images(must be loadable by P5) in a folder and sorts them by their numbering.
	 * files must have the format xxx-<number>.xxx
	 * TODO make the "index numbering format more flexible"
	 * 
	 * @param folder
	 * @return array of PImage objects in the right order
	 */
	public static PImage[] loadNumberedCollection(File folder) {
		File[] fileList = folder.listFiles();
		TreeSet<String> sortedFiles = new TreeSet<String>(new Comparator<String>() {
			
			@Override
			public int compare(String o1, String o2) {
				File f1 = new File(o1);
				File f2 = new File(o2);
				int n1 = ImageNaming.getNumber(f1);
				int n2 = ImageNaming.getNumber(f2);
				return n1 - n2;
			}
		});
		for (File f : fileList) {
			try {
				ImageNaming.getNumber(f);
				sortedFiles.add(f.getAbsolutePath());
			} catch (NumberFormatException nfe) {
				System.out.println("skipping " + f.getName());
			}
		}
		PImage[] imgList = new PImage[sortedFiles.size()];
		int i = 0;
		for (Iterator<String> iter = sortedFiles.iterator(); iter.hasNext(); i++)
			imgList[i] = rap.loadImage(iter.next());
		return imgList;
	}
	
	/**
	 * gets images from a folder. puts them in a map: filename>PImage
	 * 
	 * @param folderN
	 * @return a filename>PImage map
	 */
	public static HashMap<String, PImage> openImageFolder(String folderN) {
		return openImageFolder(folderN, new ImageFileTypeFilter());
	}
	
	/**
	 * gets images from a folder,which are from a given type. puts them in a map: filename>PImage
	 * TypeFilter takes a stringarray with accepted Formats( possible are: bmp,tif,tiff,jpg,png)
	 * 
	 * @param folderN
	 *            folder
	 * @param typeFilter
	 *            filter
	 * @return a filename>PImage map
	 */
	public static HashMap<String, PImage> openImageFolder(String folderN,
			ImageFileTypeFilter typeFilter) {
		folder = new File(folderN);
		String[] files = folder.list();
		HashMap<String, PImage> map = new HashMap<String, PImage>(files.length);
		File f;
		for (String s : files) {
			f = new File(folder + "/" + s);
			if (f.isDirectory())
				continue;
			if (typeFilter.apply(s))
				map.put((fullPath ? folder + "/" : "") + s, rap.loadImage(f.getAbsolutePath()));
		}
		return map;
	}
	
	/**
	 * filter image formats
	 * TypeFilter takes a stringarray with accepted Formats( possible are: bmp,tif,tiff,jpg,png)
	 * default ImageFileTypeFilter accepts them all
	 * 
	 * also contains a static function that checks if a file is a p5 supported imageformat
	 * 
	 * @author Ramin
	 * 
	 */
	public static class ImageFileTypeFilter implements Predicate<String> {
		
		public boolean	acceptBMP	= true, acceptJPG = true, acceptPNG = true, acceptTIFF = true,
				acceptTIF = true;
		
		public ImageFileTypeFilter() {
		}
		
		public ImageFileTypeFilter(String[] acceptedFormats) {
			List<String> typeList = Arrays.asList(acceptedFormats);
			acceptBMP = typeList.contains("bmp");
			acceptJPG = typeList.contains("jpg");
			acceptPNG = typeList.contains("png");
			acceptTIFF = typeList.contains("tiff");
			acceptTIF = typeList.contains("tif");
		}
		
		@Override
		/**
		 * function that checks if a file is a p5 supported imageformat  (bmp,tif,tiff,jpg,png) 
		 * and passes the filter
		 * 
		 * @param imgN
		 *            filename
		 * @return true if accepted
		 */
		public boolean apply(String imgN) {
			return (imgN.endsWith("bmp") && acceptBMP || imgN.endsWith("jpg") && acceptJPG
					|| imgN.endsWith("png") && acceptPNG || imgN.endsWith("tiff") && acceptTIF || imgN
					.endsWith("tif") && acceptTIFF);
		}
		
		/**
		 * static function that checks if a file is a p5 supported imageformat (bmp,tif,tiff,jpg,png)
		 * 
		 * @param imgN
		 *            filename
		 * @return true if accepted
		 */
		public static boolean supportedFormat(String imgN) {
			return (imgN.endsWith("bmp") || imgN.endsWith("jpg") || imgN.endsWith("png")
					|| imgN.endsWith("tiff") || imgN.endsWith("tif"));
		}
	}
}
