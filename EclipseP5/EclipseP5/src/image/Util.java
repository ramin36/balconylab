package image;

import processing.core.PImage;
import processing.core.PVector;
import rap.RAP;
import rap.RAPUser;

public class Util implements RAPUser {
	
	public static RAP	rap	= RAP.getRAP(Util.class);
	
	/**
	 * gets the edgepoints of an image as a PV array
	 * 
	 * @param img
	 * @return 4 points (0,0),(w,0),(w,h),(0,h)
	 */
	public static PVector[] vertices(PImage img) {
		int w = img.width;
		int h = img.height;
		return new PVector[] { new PVector(), new PVector(w, 0), new PVector(w, h),
				new PVector(0, h) };
	}
}
