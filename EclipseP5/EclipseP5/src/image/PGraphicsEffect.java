package image;

import processing.core.PGraphics;

/**
 * interaface for PGraphics effects. Not used atm.
 * TODO: Use more, or use Guava Function
 * 
 * @author Ramin
 * 
 */
public interface PGraphicsEffect {
	
	void execute(PGraphics pg);
	
}
