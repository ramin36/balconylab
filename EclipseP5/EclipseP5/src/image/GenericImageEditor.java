package image;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import processing.core.PApplet;
import processing.core.PImage;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;

/**
 * see again later
 * 
 * @author Ramin
 * 
 */
public class GenericImageEditor extends PApplet {
	
	static String					folder;
	static Predicate<String>		filter;
	static Function<PImage, PImage>	transform;
	
	@Override
	public void setup() {
		super.setup();
		File folder = new File(GenericImageEditor.folder);
		List<String> files = new ArrayList<String>(Arrays.asList(folder.list()));
		// HashBiMap<String, PImage> map = HashBiMap.create(files.size() + 5);
		ImageScaler scaler = new ImageScaler(760, 460);
		for (Iterator<String> iter = Collections2.filter(files,
				new ImgCollections.ImageFileTypeFilter()).iterator(); iter.hasNext();) {
			String name = iter.next();
			PImage img = loadImage(folder + "/" + name);
			img = scaler.apply(img);
			img.save(folder + "/new/" + "f" + name);
		}
		println("done");
		System.exit(0);
	}
	
	@Override
	public void draw() {
		
	}
	
	public static void main(String[] args) {
		if (args.length == 0)
			return;
		else
			folder = args[0];
		PApplet.main(new String[] { "GenericImageEditor" });
	}
	
	public static class ImageScaler implements Function<PImage, PImage> {
		
		boolean	doScale;
		int		x, y;
		float	scale;
		
		public ImageScaler(int x, int y) {
			this.x = x;
			this.y = y;
		}
		
		public ImageScaler(float scale) {
			this.scale = scale;
			doScale = true;
		}
		
		@Override
		public PImage apply(PImage img) {
			if (doScale)
				img.resize((int) (img.width * scale), (int) (img.height * scale));
			else
				img.resize(x, y);
			return img;
		}
	}
	
	public static class ImageSaver implements Function<PImage, Boolean> {
		
		int	counter	= 0;
		String	nameing, type;
		
		public ImageSaver(String nameing, String type) {
			this.nameing = nameing;
			if (ImgCollections.ImageFileTypeFilter.supportedFormat(type))
				this.type = type;
			else
				type = "png";
		}
		
		public void resetCounter() {
			counter = 0;
		}
		
		@Override
		public Boolean apply(PImage img) {
			img.save(nameing + counter + "." + type);
			return true;
		}
	}
	
}
