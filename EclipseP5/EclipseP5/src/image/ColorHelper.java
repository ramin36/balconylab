package image;

import math.MVector;

/**
 * static methods for color manipulation
 * 
 * @author Ramin
 * 
 */
public class ColorHelper {
	
	/**
	 * calculates the distance between two colors in the 255^3 rgb color space
	 * 
	 * @param clr1
	 * @param clr2
	 * @return
	 */
	public static float colorDistRGB(int clr1, int clr2) {
		return MVector.dist(colorToRGBVector(clr1), colorToRGBVector(clr2));
	}
	
	/**
	 * returns a MVector keeping R in x, G in y and B in z
	 * 
	 * @param clr
	 * @return
	 */
	public static MVector colorToRGBVector(int clr) {
		return new MVector(red(clr), green(clr), blue(clr));
	}
	
	/**
	 * return a R,G,B String
	 * 
	 * @param clr
	 * @return
	 */
	public static String toString(int clr) {
		return red(clr) + "," + green(clr) + "," + blue(clr);
	}
	
	public static final float alpha(int rgb) {
		return (rgb >> 24) & 0xff;
	}
	
	/**
	 * Extracts the red value from a color
	 */
	public static final float red(int rgb) {
		return (rgb >> 16) & 0xff;
	}
	
	/**
	 * Extracts the green value from a color
	 * 
	 * @param rgb
	 * @return
	 */
	public static final float green(int rgb) {
		return (rgb >> 8) & 0xff;
	}
	
	/**
	 * Extracts the blue value from a color
	 * 
	 * @param rgb
	 * @return
	 */
	public static final float blue(int rgb) {
		return (rgb) & 0xff;
	}
	
	/**
	 * inverts a color
	 * 
	 * @param clr
	 * @return
	 */
	public static final int invert(int clr) {
		return color(255 - red(clr), 255 - green(clr), 255 - blue(clr));
	}
	
	/**
	 * create a rgb color (from p5.core)
	 * 
	 * @param red
	 * @param green
	 * @param blue
	 * @return
	 */
	public static final int color(float red, float green, float blue) {
		if (red > 255)
			red = 255;
		else if (red < 0)
			red = 0;
		if (green > 255)
			green = 255;
		else if (green < 0)
			green = 0;
		if (blue > 255)
			blue = 255;
		else if (blue < 0)
			blue = 0;
		
		return 0xff000000 | ((int) red << 16) | ((int) green << 8) | (int) blue;
	}
	
	/**
	 * 
	 * create a rgba color (from p5.core)
	 * 
	 * @param red
	 * @param green
	 * @param blue
	 * @param trans
	 * @return
	 */
	public static final int color(float red, float green, float blue, float trans) {
		if (red > 255)
			red = 255;
		else if (red < 0)
			red = 0;
		if (green > 255)
			green = 255;
		else if (green < 0)
			green = 0;
		if (blue > 255)
			blue = 255;
		else if (blue < 0)
			blue = 0;
		
		return ((int) trans << 24) | ((int) red << 16) | ((int) green << 8) | (int) blue;
	}
	
	/**
	 * 
	 * @param red
	 * @param green
	 * @param blue
	 * @return
	 */
	public static final int addTransparency(int c, int trans) {
		return c | (trans << 24);
	}
	
	public static final String RGBAToString(int clr) {
		return red(clr) + ", " + green(clr) + ", " + blue(clr) + ", " + alpha(clr);
	}
}
