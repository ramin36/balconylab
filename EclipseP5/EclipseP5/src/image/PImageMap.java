package image;

import image.ImgCollections.ImageFileTypeFilter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import math.structure.Rect;
import processing.core.PImage;
import rap.RAP;
import rap.RAPUser;
import toxi.geom.Vec2D;

import com.drew.lang.GeoLocation;

/**
 * holds PImages with their GeoLocations
 * 
 * @author Ramin
 * 
 */
public class PImageMap implements RAPUser {
	
	public static RAP					rap	= RAP.getRAP(PImageMap.class);
	
	public HashMap<PImage, GeoLocation>	pImageMap;
	private final int					size;
	public Rect							box;
	public GeoLocation					boxMin, boxMax;
	public GeoLocation					boxWidth, boxHeight;
	
	public PImageMap(String folderN) {
		ImgCollections.fullPath = true;
		Map<String, PImage> images = ImgCollections.openImageFolder(folderN,
				new ImageFileTypeFilter(new String[] { "jpg" }));
		Map<String, GeoLocation> geoLocations = MetaDateExtractor
				.getGPSCoordinates(new ArrayList<String>(images.keySet()));
		pImageMap = new HashMap<PImage, GeoLocation>(images.size() + 1, 1);
		for (String name : images.keySet())
			if (geoLocations.get(name) != null) {
				pImageMap.put(images.get(name), geoLocations.get(name));
				// System.out.println(geoLocations.get(name));
			}
		size = pImageMap.size();
		
		calcBox();
		// System.out.println(minmax[0] + " || " + minmax[1]);
	}
	
	private void calcBox() {
		GeoLocation[] minmax = getMinMax(pImageMap.values());
		// System.out.println(minmax[0] + " " + minmax[1]);
		// System.out.println((float) (minmax[1].getLongitude() - minmax[0].getLongitude()));
		box = new Rect(
				new Vec2D((float) minmax[0].getLongitude(), (float) minmax[0].getLatitude()),
				(float) (minmax[1].getLongitude() - minmax[0].getLongitude()),
				(float) (minmax[1].getLatitude() - minmax[0].getLatitude()));
		System.out.println(box);
	}
	
	/**
	 * returns the bounding box encapsulating all coordinates
	 * 
	 * @param locations
	 */
	public GeoLocation[] getMinMax(Collection<GeoLocation> locations) {
		
		ArrayList<Double> longitudes = new ArrayList<Double>(locations.size());
		ArrayList<Double> latitudes = new ArrayList<Double>(locations.size());
		for (Iterator<GeoLocation> iterator = locations.iterator(); iterator.hasNext();) {
			GeoLocation geoLoc = iterator.next();
			// System.out.println(geoLoc);
			longitudes.add(geoLoc.getLongitude());
			latitudes.add(geoLoc.getLatitude());
		}
		return new GeoLocation[] {
				new GeoLocation(Collections.min(latitudes), Collections.min(longitudes)),
				new GeoLocation(Collections.max(latitudes), Collections.max(longitudes)) };
	}
}
