package image;

import processing.core.PImage;
import processing.core.PVector;

import com.google.common.base.Function;

public class ImageFunctions {
	
	private static Function<PImage, PImage>	scaleToFrame	= new Function<PImage, PImage>() {
																
																@Override
																public PImage apply(PImage input) {
																	return ImageHelper
																			.scaleToFrame(
																					input,
																					(PVector) refObject);
																}
															};
	
	private static Object					refObject;
	
	/**
	 * creates a Function of of the {@link image.ImageHelper#scaleToFrame} function
	 * 
	 * @param frameSize
	 * @return Imagescaling function
	 */
	public static Function<PImage, PImage> scaleToFrame(PVector frameSize) {
		refObject = frameSize;
		return scaleToFrame;
	}
	
}
