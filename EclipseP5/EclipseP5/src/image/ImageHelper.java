package image;

import java.util.TreeSet;

import math.JTS;
import math.MVector;
import math.structure.Rect;
import processing.core.PApplet;
import processing.core.PGraphics;
import processing.core.PImage;
import processing.core.PVector;
import rap.RAP;
import rap.RAPUser;
import toxi.geom.Polygon2D;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Point;

/**
 * static Imagehelper functions
 * 
 * @author Ramin
 * 
 */
public class ImageHelper implements RAPUser {
	
	public static RAP	rap	= RAP.getRAP(ImageHelper.class);
	
	/**
	 * checks if an position in the Image dimension
	 * 
	 * @param img
	 * @param pos
	 * @return
	 */
	public static boolean inImage(PImage img, MVector pos) {
		return pos.x >= 0 && pos.x < img.width && pos.y >= 0 && pos.y < img.height;
	}
	
	/**
	 * Scales an Image to match the size defined in a vector
	 * 
	 * @param img
	 * @param frameSz
	 * @return scaled Image
	 */
	public static PImage scaleToFrame(PImage img, PVector frameSz) {
		int w = img.width;
		int h = img.height;
		float scale = 1;
		scale = (float) frameSz.x / w;
		float scaleY = (float) frameSz.y / h;
		scale = Math.min(scale, scaleY);
		PImage retImg;
		try {
			retImg = (PImage) img.clone();
			retImg.resize((int) (img.width * scale), (int) (img.height * scale));
			return retImg;
		} catch (CloneNotSupportedException e) {
			System.err.println("Couldn't clone image");
			return img;
		}
	}
	
	/**
	 * scales an images so that it fits into frameSz ( width,height ration stay same)
	 * 
	 * @param img
	 * @param frameSz
	 * @return
	 */
	public static PVector scaleToFrameVector(PImage img, MVector frameSz) {
		int w = img.width;
		int h = img.height;
		float scale = 1;
		scale = (float) frameSz.x / w;
		float scaleY = (float) frameSz.y / h;
		scale = Math.min(scale, scaleY);
		return new PVector((img.width * scale), (int) (img.height * scale));
	}
	
	/**
	 * Cuts a polygon out of an image
	 * 
	 * @param img
	 *            the input image
	 * @param poly
	 *            input polygon
	 * @return PGraphics that contains only the pixels from the image that are in the polygon
	 */
	public static PGraphics cutPolygonOutOfImage(PImage img, Polygon2D poly) {
		poly = math.structure.Polygon.verticesToInt(poly);
		Rect r = math.structure.Polygon.boundaryBox(poly);
		int imgW = img.width;
		int w = (int) PApplet.min(imgW, r.width());
		int h = (int) PApplet.min(img.height, r.height());
		PGraphics graphic = rap.createGraphics(w, h);
		w = (int) PApplet.min(imgW, r.x() + r.width());
		h = (int) PApplet.min(img.height, r.y() + r.height());
		com.vividsolutions.jts.geom.Polygon jtsPoly = JTS.polygon(poly);
		Point p;
		img.loadPixels();
		graphic.beginDraw();
		graphic.loadPixels();
		for (int x = (int) r.x(); x < w; x++)
			for (int y = (int) r.y(); y < h; y++) {
				p = JTS.gf.createPoint(new Coordinate(x, y));
				// System.out.println(x + "," + y + " > " + (x - (int) r.x()) + ", "
				// + (int) ((y - r.y())));
				try {
					if (jtsPoly.contains(p))
						graphic.pixels[x - (int) r.x() + (int) ((y - r.y()) * r.width())] = img.pixels[x
								+ y * imgW];
				} catch (Exception exc) {
					System.err.println(poly);
					// printAsNew(poly);
					break;
				}
			}
		// System.out.println(x + "/" + w);
		graphic.updatePixels();
		graphic.endDraw();
		return graphic;
	}
	
	/**
	 * flooding algorithm, gets the surrounding pixel, if their RGB distance (see ColorHelper.colorDistRGB
	 * 
	 * @param pg
	 * @param pos
	 * @param tolerance
	 * @return
	 */
	public static TreeSet<MVector> selectAroundPos(PImage pg, MVector pos, float tolerance) {
		TreeSet<MVector> list = new TreeSet<MVector>();
		TreeSet<MVector> pile = new TreeSet<MVector>();
		TreeSet<MVector> grave = new TreeSet<MVector>();
		if (!ImageHelper.inImage(pg, pos))
			return list;
		pile.add(pos);
		pg.loadPixels();
		int clr = pg.get((int) pos.x, (int) pos.y);
		while (!pile.isEmpty()) {
			MVector next = pile.pollFirst();
			list.add(next);
			for (MVector mc : next.getMooreNB()) {
				if (ImageHelper.inImage(pg, mc) && !list.contains(mc) && !pile.contains(mc)
						&& !grave.contains(mc)
						&& ColorHelper.colorDistRGB(clr, pg.get(mc.xi(), mc.yi())) <= tolerance)
					pile.add(mc);
				else
					grave.add(mc);
			}
		}
		pg.updatePixels();
		return list;
	}
	
	// private static void printAsNew(Polygon2D poly) {
	// List<Vec2D> v = poly.vertices;
	// for (Vec2D p : v)
	// System.out.println("new Vec2D(" + p.x + "f," + p.y + "f),");
	// }
	
	public static PGraphics getColoumn(PImage img, int column) {
		PGraphics pg = rap.createGraphics(1, img.height);
		pg.beginDraw();
		pg.copy(img, column, 0, 1, img.height, 0, 0, 1, img.height);
		pg.endDraw();
		return pg;
	}
	
	public static PGraphics mirror(PImage img) {
		int w = img.width, h = img.height;
		PGraphics pg = rap.createGraphics(w, h);
		int stop = w;
		pg.beginDraw();
		pg.loadPixels();
		for (int y = 0; y < h; y++)
			for (int x = 0; x < stop; x++) {
				System.out.println(x);
				pg.pixels[y * w + x] = img.pixels[(y + 1) * w - x - 1];
			}
		pg.updatePixels();
		pg.endDraw();
		return pg;
	}
	
	public static PGraphics getRow(PImage img, int row) {
		PGraphics pg = rap.createGraphics(img.width, 1);
		pg.beginDraw();
		pg.copy(img, 0, row, img.width, 1, 0, 0, img.width, 1);
		pg.endDraw();
		return pg;
	}
}
