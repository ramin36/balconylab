package image;

import processing.core.PGraphics;
import processing.core.PImage;
import rap.RAP;

public class Tests extends RAP {
	
	PImage		img;
	PGraphics	pg;
	int			maxShift	= 5;
	
	@Override
	public void setup() {
		super.setup();
		img = loadImage("C:/Users/Ramin/Pictures/imgs/tumblr_crystals.jpg");
		size(img.width, img.height);
		// this.getAppletContext();
		// System.out.println((this.getParent().getClass()));
		image(img, 0, 0);
	}
	
	@Override
	public void draw() {
		if (frameCount == 1)
			image(img, 0, 0);
		loadPixels();
		for (int i = 0; i < 50; i++) {
			PGraphics pg = null;
			if (randomBool()) {
				int coloumn = randomInt(width);
				pg = ImageHelper.getColoumn(randomBool() ? img : g, coloumn);
				image(pg, coloumn + randomInt(2 * maxShift) - maxShift, 0);
			} else {
				int row = randomInt(height);
				println(row);
				pg = ImageHelper.getRow(randomBool() ? img : g, row);
				image(pg, 0, row + randomInt(2 * maxShift) - maxShift);
			}
			removeCache(pg);
		}
		println(frameRate);
	}
}
