package image;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.lang.GeoLocation;
import com.drew.metadata.Metadata;
import com.drew.metadata.exif.GpsDirectory;

/**
 * get geolocations out of the images.
 * more metadata with this library possible
 * 
 * @author Ramin
 * 
 */
public class MetaDateExtractor {
	
	/**
	 * gets the geolocation of image files
	 * 
	 * 
	 * @param imgs
	 *            absolute paths of the images
	 * @return hashmap imagepaths and geoloation
	 */
	public static HashMap<String, GeoLocation> getGPSCoordinates(List<String> imgs) {
		HashMap<String, GeoLocation> map = new HashMap<String, GeoLocation>(imgs.size() + 2, 1);
		for (String fileName : imgs) {
			File file = new File(fileName);
			try {
				Metadata metadata = ImageMetadataReader.readMetadata(file);
				GpsDirectory gpsDirectory = metadata.getDirectory(GpsDirectory.class);
				GeoLocation geoLoc = gpsDirectory.getGeoLocation();
				map.put(fileName, geoLoc);
			} catch (ImageProcessingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (NullPointerException e) {
				System.err.println("Can't load gps for: " + fileName + " - skipping");
			}
		}
		return map;
	}
}
