package voronoi.combs;

import java.util.ArrayList;
import java.util.Collections;

import math.JTS;
import math.MVector;
import math.Voronoi.Cell;
import rap.RAP;
import toxi.geom.Polygon2D;
import toxi.geom.Vec2D;
import voronoi.CircleVoronoi;
import voronoi.VCell;

public class Comb extends Cell {
	
	public CircleVoronoi<VCell>	circVor;
	ArrayList<Polygon2D>		cutPolys	= new ArrayList<Polygon2D>();
	CombValues					values		= new CombValues();
	
	// demo
	int							baseColor;
	
	public Comb(Vec2D site, Polygon2D region) {
		super(site, region);
		baseColor = rap.randomColor();
	}
	
	public void createCirc() {
		circVor = new CircleVoronoi();
		if (values.seedLocation == CombValues.VORONOI_SEED)
			circVor.center = new MVector(site);
		else
			circVor.center = new MVector(region.getCentroid());
		circVor.spread(new Integer[] { 8, 4, 8 }, getMaxCentroidDist());
		//
		for (VCell cv : circVor.cells)
			cv.setColor(rap.color(CirclesVsInCombs.noise2D.noise(new MVector(cv.site)) * 255,
					255 - ((float) cv.ring / circVor.numRings) * 100, 200));// RAPHelper.rndColor();
	}
	
	private float getAvgCentroidDist() {
		float dist = 0;
		for (Vec2D vertex : region.vertices)
			dist += vertex.distanceTo(site);
		if (region.vertices.size() != 0)
			return dist / region.vertices.size();
		else
			return 0;
	}
	
	private float getMaxCentroidDist() {
		ArrayList<Float> dist = new ArrayList<Float>();
		for (Vec2D vertex : region.vertices)
			dist.add(vertex.distanceTo(site));
		return Collections.max(dist);
	}
	
	public void cutPolys() {
		// for (Polygon2D poly : circVor.getRegions())
		// cutPolys.add(JTS.intersection(poly, region));
		
		for (VCell cv : circVor.cells) {
			cv.setBorder((JTS.intersects(cv.getRegion(), this.region) && !JTS.contains(this.region,
					cv.getRegion())));
			cv.darkBorder();
			cv.setRegion(JTS.intersection(cv.getRegion(), this.region));
		}
	}
	
	@Override
	public void render() {
		for (VCell cv : circVor.cells)
			cv.render();
		renderNeighbourhood();
		// else
		// System.out.println(this + " n");
	}
	
	public void renderNeighbourhood() {
		if (neighbours != null)
			for (Cell c : neighbours) {
				RAP.rap.line(new MVector(site), new MVector(c.site));
			}
	}
	
	public static class CombValues {
		
		public int	seedLocation;
		public static int	VORONOI_SEED	= 0, POLYGON_CENTROID = 1;
		
		public CombValues() {
			seedLocation = 1;// (int) (Math.random() * 2);
			
		}
	}
}
