package voronoi.combs;

public interface CombAnimation {
	
	public int getLevel();
	
	public int setLevel(int lvl);
	
	public void levelUp();
	
	public void apply(Comb c);
}
