package voronoi.combs;

import java.util.ArrayList;

import math.JTS;
import math.MVector;
import math.Voronoi;
import math.Voronoi.Cell;
import math.function.Noise2D;
import math.structure.Rect;
import rap.RAP;
import toxi.geom.Polygon2D;
import toxi.geom.Vec2D;
import voronoi.CellFactory;

public class CirclesVsInCombs extends RAP {
	
	private static final long	serialVersionUID	= 2547931413455886604L;
	
	MVector						size				= new MVector(1100, 600);
	
	Rect						window				= new Rect(new Vec2D(), size.x, size.y);
	
	int							numCombs			= 10;
	Voronoi<Comb>				combVoronoi;
	
	ArrayList<Comb>				combs				= new ArrayList<Comb>();
	
	static public Noise2D		noise2D;
	
	@Override
	public void setup() {
		super.setup();
		size(size);
		noise2D = new Noise2D(new MVector(MVector.mult(size, 0.2f)));
		colorMode(HSB);
		initCombs();
	}
	
	public void initCombs() {
		combVoronoi = new Voronoi<Comb>(new CellFactory<Comb>() {
			@Override
			public Comb create(Vec2D site, Polygon2D region) {
				return new Comb(site, region);
			}
		});
		Cell.vornoi = combVoronoi;
		ArrayList<Vec2D> sites = new ArrayList<Vec2D>();
		for (int i = 0; i < numCombs; i++)
			sites.add(rap.randomPoint().toVec2D());
		combVoronoi.addPoints(sites);
		combVoronoi.createCells();
		combVoronoi.calcConnectivity();
		combs.clear();
		for (Comb comb : combVoronoi.cells) {
			cutByBorder(comb);
			combs.add(comb);
			comb.createCirc();
			comb.cutPolys();
		}
	}
	
	private void cutByBorder(Comb comb) {
		comb.region = JTS.intersection(comb.region, window.toPolygon2D());
	}
	
	@Override
	public void draw() {
		background(0);
		// stroke(255);
		noStroke();
		for (Comb comb : combs)
			comb.render();
		
		// RAP.rap.strokeWeight(3);
		// RAP.rap.stroke(0);
		// for (Comb comb : combs)
		// comb.renderNeighbourhood();
		
		// stroke(255, 0, 0);
		// for (Comb comb : combs) {
		// poly(comb.region);
		// }
		// for (Polygon2D poly : combVoronoi.getRegions()) {
		// poly(poly);
		// }
	}
	
	@Override
	public void mousePressed() {
		// saveFrame("borderAreasMarked-##.png");
		initCombs();
	}
}
