package voronoi;

import math.Voronoi;
import toxi.geom.Polygon2D;
import toxi.geom.Vec2D;

public interface CellFactory<T extends Voronoi.Cell> {
	
	T create(Vec2D site, Polygon2D region);
	
}
