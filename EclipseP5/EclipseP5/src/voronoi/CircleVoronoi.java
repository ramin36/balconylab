package voronoi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import math.Collections3;
import math.MVector;
import math.Voronoi;
import math.structure.Circle;
import processing.core.PVector;
import toxi.geom.Polygon2D;
import toxi.geom.Vec2D;

public class CircleVoronoi<T extends VCell> extends Voronoi<T> {
	
	public float						radius;
	public int							numRings;
	
	protected ArrayList<ArrayList<T>>	rings			= new ArrayList<ArrayList<T>>();
	
	public PVector						center			= new PVector();
	
	public float						globalPhase;
	
	public boolean						useCentralSeed	= true;
	
	/**
	 * is set if spread is equidistant and defines the selection of cell factory
	 */
	private boolean						equiDistant		= false;
	
	public CircleVoronoi() {
		super(null);
		this.center = new MVector();
	}
	
	public CircleVoronoi(final MVector s) {
		super(null);
		this.center = s;
	}
	
	public CircleVoronoi(CellFactory<T> factory) {
		super(factory);
	}
	
	public void spread(int numRings, float radius) {
		List<Integer> l = new ArrayList<Integer>();
		l.addAll(Arrays.asList(randomDivisors(10, 30)));
		spread(Collections3.pickN(l, numRings, true).toArray(new Integer[0]), radius);
	}
	
	public void spread(Integer[] seeds, float radius) {
		this.radius = radius;
		numRings = seeds.length;
		float div = (float) radius / (numRings + 1);
		Float[] distances = new Float[numRings];
		for (int i = 0; i < numRings; i++)
			distances[i] = div * i;
		equiDistant = true;
		spread(seeds, distances);
	}
	
	/**
	 * set the distance for each ring
	 * TODO: doesnt work atm, ( cellcreator uses distance to define the ring(index) of a cell
	 * 
	 * @param seeds
	 * @param distances
	 */
	public void spread(Integer[] seeds, Float[] distances) {
		numRings = seeds.length;
		selectFactory(distances);
		// create the rings
		ArrayList<Vec2D> sites = new ArrayList<Vec2D>();
		for (int i = 0; i < numRings; i++) {
			if (i == 0 && useCentralSeed) {
				Vec2D v = new Vec2D(center.x, center.y);
				sites.add(v);
				continue;
			}
			int ps = seeds[i];// random.nextInt(divisors.length)];
			// create the ring points, inner loop
			MVector[] points = Circle.getEqualPoints(distances[i], center, ps, globalPhase);
			for (MVector p : points) {
				Vec2D vc = new Vec2D(p.toVec2D());
				sites.add(vc);
			}
		}
		addPoints(sites);
		createCells();
		// sort into rings
		rings.clear();
		for (int i = 0; i < numRings; i++)
			rings.add(new ArrayList<T>());
		for (Object c : cells) {
			rings.get(((VCell) c).ring).add((T) c);
		}
	}
	
	private void selectFactory(Float[] distances) {
		if (equiDistant)
			factory = (CellFactory<T>) new EquiDistRingCellFactory(new Vec2D(center.x, center.y),
					radius / numRings);
		else {
			factory = (CellFactory<T>) new RingCellFactory(new Vec2D(center.x, center.y));
			((RingCellFactory) factory).setDistance(Arrays.asList(distances));
		}
	}
	
	public void connectCellsInRings() {
		for (ArrayList<T> ring : rings) {
			int ps = ring.size();
			for (int p = 0; p < ps; p++) {
				ring.get(p).prev = ring.get((p - 1 + ps) % ps);
				ring.get(p).next = ring.get((p + 1 + ps) % ps);
			}
		}
	}
	
	private Integer[] randomDivisors(int minPs, int maxPs) {
		Integer[] divisors = divisors(360);
		int deleteTo = 0;
		int deleteFrom = 0;
		if (minPs > maxPs) {
			int tmp = minPs;
			minPs = maxPs;
			maxPs = tmp;
		}
		for (int k = 0; k < divisors.length; k++) {
			if (divisors[k] < minPs)
				deleteTo = k;
			if (divisors[k] > maxPs && deleteFrom == 0)
				deleteFrom = k;
		}
		divisors = Arrays.copyOfRange(divisors, deleteTo, deleteFrom);
		return divisors;
	}
	
	private Integer[] divisors(int n) {
		ArrayList<Integer> divs = new ArrayList<Integer>();
		for (int i = 1; i < n / 2; i++)
			if (n % i == 0)
				divs.add(i);
		return divs.toArray(new Integer[divs.size()]);
	}
	
	public static Integer[] generateRings(int minRNum, int maxRNum, int minCellNum, int maxCellNum) {
		int numRings = (int) rap.random(minRNum, maxRNum);
		Integer[] ringCells = new Integer[numRings];
		for (int i = 0; i < numRings; i++)
			ringCells[i] = (int) rap.random(minCellNum, maxCellNum);
		return ringCells;
	}
	
	public void render(boolean renderOuter) {
		int last = rings.size() - (renderOuter ? 0 : 1);
		for (int i = 0; i < last; i++)
			for (VCell c : rings.get(i))
				c.render();
	}
	
	public void render(Integer[] ringsToRender) {
		List<Integer> list = Arrays.asList(ringsToRender);
		for (int i = 0; i < rings.size(); i++)
			if (list.contains(i))
				for (VCell c : rings.get(i))
					c.render();
	}
	
	public void removeLastRing() {
		numRings--;
		for (Cell c : rings.get(numRings))
			cells.remove(c);
		rings.remove(numRings - 1);
	}
	
	public class RingCellFactory implements CellFactory<VCell> {
		
		final Vec2D			center;
		private List<Float>	distances;
		
		public void setDistance(List<Float> distances) {
			this.distances = distances;
			if (useCentralSeed)
				distances.set(0, 0f);
		}
		
		public RingCellFactory(Vec2D center) {
			this.center = center;
		}
		
		@Override
		public VCell create(Vec2D site, Polygon2D region) {
			float d = Math.round(center.distanceTo(site));
			int index = distances.indexOf(d);
			if (index == -1) {
				System.err.println("Husten we have a problem");
				System.err.println(d + " is not in " + distances.toString());
			}
			return new VCell(site, region, index);
		}
	}
	
	public static class EquiDistRingCellFactory implements CellFactory<VCell> {
		
		final Vec2D	center;
		final float	div;
		
		public EquiDistRingCellFactory(Vec2D center, float div) {
			this.center = center;
			this.div = div;
		}
		
		@Override
		public VCell create(Vec2D site, Polygon2D region) {
			float d = site.distanceTo(center) / div;
			return new VCell(site, region, Math.round(d));
		}
	}
}
