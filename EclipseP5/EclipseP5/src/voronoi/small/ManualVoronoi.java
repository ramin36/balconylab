package voronoi.small;

import java.util.ArrayList;

import math.MVector;
import math.Voronoi;
import math.Voronoi.Cell;
import rap.RAP;
import toxi.geom.Polygon2D;
import toxi.geom.Triangle2D;
import toxi.geom.Vec2D;
import drawing.Polygon2DRenderer;

public class ManualVoronoi extends RAP {
	
	Voronoi<Cell>		voronoi			= new Voronoi<Cell>();
	ArrayList<MVector>	points			= new ArrayList<MVector>();
	
	boolean				displaySites	= true;
	boolean				displayRegions	= true;
	boolean				displayTriangles;
	
	Polygon2DRenderer	renderer		= Polygon2DRenderer.defaultRenderer;
	
	// test 1: show the flaw in voronoi, getSite(index) not inside getRegion(index)
	// int select = -1;
	
	@Override
	public void setup() {
		super.setup();
		size(800, 700, P2D);
		renderer.renderStyle = Polygon2DRenderer.VERTEXCOLORS;
		smooth();
	}
	
	@Override
	public void draw() {
		background(0);
		if (displayRegions) {
			noFill();
			stroke(255);
			for (Polygon2D region : voronoi.getRegions())
				poly(region);
		}
		if (displayTriangles) {
			// A
			noFill();
			stroke(0, 255, 255);
			for (Triangle2D triangle : voronoi.getTriangles())
				this.triangle(triangle);
			// B
			// noStroke();
			// for (Triangle2D triangle : voronoi.getTriangles()) {
			// int[] vertexColors = new int[3];
			// for (int i = 0; i < vertexColors.length; i++) {
			// int clr = randomColor();
			// clr = ColorHelper.addTransparency(clr, (int) random(50));
			// vertexColors[i] = clr;
			// weirdPolygon(triangle, vertexColors);
			// }
			// }
			// end B
		}
		if (displaySites) {
			noStroke();
			fill(255, 0, 0);
			for (Vec2D site : voronoi.getSites())
				ellipse(site.x, site.y, 7, 7);
		}
		// // test 1: show the flaw in voronoi, getSite(index) not inside getRegion(index)
		// if (select >= 0) {
		// stroke(255);
		// noFill();
		// ellipse(points.get(select).x, points.get(select).y, 10, 10);
		// ellipse(voronoi.getSites().get(select).x, voronoi.getSites().get(select).y, 13, 13);
		// fill(180, 100);
		// poly(voronoi.getRegions().get(select));
		// }
	}
	
	private void weirdPolygon(Triangle2D triangle, int[] vertexColors) {
		renderer.render(triangle.toPolygon2D(), vertexColors);
	}
	
	@Override
	public void keyPressed() {
		if (key == 's')
			displaySites = !displaySites;
		else if (key == 't')
			displayTriangles = !displayTriangles;
		else if (key == 'r')
			displayRegions = !displayRegions;
		// else if (key == 'n') // test 1: show the flaw in voronoi, getSite(index) not inside getRegion(index)
		// select = (select + 1) % points.size();
	}
	
	@Override
	public void mousePressed() {
		if (mouseButton == LEFT) {
			MVector point = new MVector(mouseX, mouseY);
			points.add(point);
			voronoi.addPoint(point);
		} else if (mouseButton == RIGHT) {
			for (int i = 0; i < points.size(); i++)
				if (points.get(i).selected(mousePos())) {
					points.remove(i);
					removeSite(i);
				}
		}
	}
	
	// private void calcVoronoi() {
	// if(points.size()>2)
	// voronoi.
	// }
	
	private void removeSite(int i) {
		voronoi = new Voronoi<Voronoi.Cell>();
		for (MVector p : points)
			voronoi.addPoint(p);
	}
}
