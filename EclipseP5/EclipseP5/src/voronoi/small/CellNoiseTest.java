package voronoi.small;

import processing.core.PImage;
import rap.RAP;
import CellNoise.CellDataStruct;
import CellNoise.CellNoise;

public class CellNoiseTest extends RAP {
	
	CellNoise		cn;
	CellDataStruct	cd;
	float			total	= 0.0f, start = 0.0f;
	float[]			freq	= { 0.08f, 0.08f, 0.05f };
	double[]		at		= { 0, 0, 0 };
	PImage			im;
	int				frames	= 0, t = 0;
	double			Fs		= 0, Fsum = 0;
	
	@Override
	public void setup() {
		size(600, 600);
		background(0);
		cn = new CellNoise(this);
		cd = new CellDataStruct(this, 2, at, cn.EUCLIDEAN);
		start = millis();
	}
	
	@Override
	public void draw() {
		loadPixels();
		// background(0);
		at[2] += freq[2];
		float diff;
		// t += freq[2];
		
		for (int x = 0; x < width; x++) {
			at[0] = freq[0] * (x + t);
			for (int y = 0; y < height; y++) {
				at[1] = freq[1] * (y + t);
				
				cd.at = at;
				cn.noise(cd);
				
				// pixel color
				pixels[x + y * width] = color((float) (cd.F[1] * cd.F[0]) * 150);
				
				// avrage F[0]
				Fsum += cd.F[0];
				Fs++;
			}
		}
		updatePixels();
		frames++;
	}
	
	@Override
	public void mouseReleased() {
		total = millis() - start;
		println("fps: " + frames * 1000 / total);
		println("avgF: " + Fsum / Fs);
		println();
	}
	
}
