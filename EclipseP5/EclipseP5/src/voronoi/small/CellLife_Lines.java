package voronoi.small;

import math.Voronoi.Cell;
import math.structure.Polygon;
import rap.RAP;
import voronoi.animation1.SpecCircleVoronoi;
import drawing.Polygon2DRenderer;

public class CellLife_Lines extends RAP {
	
	SpecCircleVoronoi	vor;
	
	int					rings	= 6, minCells = 4, maxCells = 12;
	
	@Override
	public void setup() {
		super.setup();
		size(800, 800);
		noFill();
		stroke(255);
		newVor();
	}
	
	@Override
	public void draw() {
		background(0);
		translate(center());
		vor.render(false);
	}
	
	@Override
	public void keyPressed() {
		newVor();
	}
	
	private void newVor() {
		vor = new SpecCircleVoronoi();
		vor.spread((int) random(3, 20), height * 0.6f);// or.generateRings(rings, rings, minCells, maxCells));
		Cell.renderer.renderStyle = Polygon2DRenderer.LINES_TO_SINGLE;
		Cell.renderer.reference = Polygon.center;
	}
	
}
