package voronoi;

import java.util.ArrayList;

import lombok.Getter;
import lombok.Setter;
import math.Voronoi.Cell;
import toxi.geom.Polygon2D;
import toxi.geom.Vec2D;

public class VCell extends Cell {
	
	private static final long	serialVersionUID	= 1L;
	
	public int					ring;
	VCell						prev, next;
	ArrayList<VCell>			kids;
	
	public static int			numRings;
	
	@Setter
	int							color;
	
	@Getter
	@Setter
	boolean						border;
	
	public VCell(Vec2D site, Polygon2D region, int ring) {
		super(site, region);
		this.ring = ring;
		kids = new ArrayList<VCell>();
	}
	
	public void darkBorder() {
		color = rap.color(rap.hue(color), 200, border ? 100 : 200);
	}
	
	@Override
	public void render() {
		rap.fill(color);
		super.render();
	}
	
	//
	// public void addSand() {
	// nextSand++;
	// if (!showIntermediateSteps)
	// sand++;
	// }
	//
	// public void update() {
	// // if (ring == 0)
	// // System.out.println("update: " + sand + "/ " + nextSand);
	// sand = nextSand;
	// if (deactivateOuterRing && ring == numRings) {
	// sand = 0;
	// return;
	// }
	// if (sand >= toppleTresh) {
	// if (kids.isEmpty())
	// nextSand = 0;
	// ArrayList<Cell> kidsClone = new ArrayList<>();
	// kidsClone.addAll(kids);
	// while (nextSand > 0 && !kidsClone.isEmpty()) {
	// Cell kid = kidsClone.get((int) random(kidsClone.size()));
	// kid.nextSand++;
	// nextSand--;
	// kidsClone.remove(kid);
	// }
	// move(ring, (random(-3, 3)));
	// }
	// }
	//
	// @Override
	// public void drawRegion() {
	// if (hideOuterRing && ring == numRings)
	// return;
	// // regions
	// int c = (int) max(0, (256 - sand / (float) toppleTresh * 256));
	// fill(c);
	// stroke(c);
	//
	// if (getRenderer().equals("PGraphics2D")) {
	// draw(region);
	// // System.out.println(ring);
	// beginShape();
	// for (Cell k : kids) {
	// List<Vec2D> ed = findCommonEdges(k);
	// // System.out.print(ed.size() + " ");
	// }
	// // println();
	// // System.out.println("*****");
	// } else { // 3D
	// float realActH = sand * lvlHeight;
	// if (showIntermediateSteps) {
	// float nextH = nextSand * lvlHeight;
	// realActH = map(actStep, numIntermediateSteps, 0, realActH, nextH);
	// int cNext = (int) max(0, (256 - nextSand / (float) toppleTresh * 256));
	// c = (int) map(actStep, numIntermediateSteps, 0, c, cNext);
	// fill(c);
	// stroke(c);
	// }
	//
	// if (drawSlope) {
	// MVector center = new MVector(region.getCentroid());
	// for (Cell k : kids) {
	// ArrayList<Vec2D> v = findCommonEdges(k);
	// if (v.size() < 2)
	// return;
	// beginShape();
	// for (int i = 0; i < v.size() - 2; i++)
	// vertex(new MVector(v.get(i)));
	// vertex(center);
	// endShape(CLOSE);
	// }
	// } else { // NORMAL MODE
	// beginShape();
	// List<Vec2D> list = region.vertices;
	// // the top
	// for (Vec2D v : list) {
	// vertex(v.x, v.y, realActH);
	// }
	// endShape(CLOSE);
	// // the side
	// beginShape(QUAD_STRIP);
	// for (Vec2D v : list) {
	// vertex(v.x, v.y, realActH);
	// vertex(v.x, v.y, 0);
	// }
	// Vec2D v = list.get(0);
	// vertex(v.x, v.y, realActH);
	// vertex(v.x, v.y, 0);
	// endShape();
	// }
	// //
	//
	// // System.out.println(ring);
	// // beginShape(QUAD_STRIP);
	// // for (Cell k : kids) {
	// // List<Vec2D> ed = findCommonEdges(k);
	// // // System.out.print(ed.size() + " ");
	// // for (int i = 0; i < ed.size(); i++)
	// // vertex(ed.get(i).x, ed.get(i).y, sand * lvlHeight);
	// // for (int i = ed.size() - 1; i >= 0; i--)
	// // vertex(ed.get(i).x, ed.get(i).y, k.sand * lvlHeight);
	// // }
	// // endShape(CLOSE);
	// // println();
	// // System.out.println("*****");
	// }
	// }
	//
	// public void drawPoint() {
	// // point
	// for (MVector v : allPoints)
	// ellipse((MVector) v, 4, 4);
	// }
	//
	// public void drawConnections() {
	// // neighbours
	// stroke(255, 0, 0);
	// line(this, prev);
	// line(this, next);
	// }
	//
	// public void drawTriangles() {
	// stroke(0, 255, 0);
	// noFill();
	// for (Triangle2D tria : triangles)
	// draw(tria);
	// fill(0);
	// }
	//
	// public ArrayList<Vec2D> findCommonEdges(Cell c) {
	// ArrayList<Vec2D> comPs = new ArrayList<>();
	// if (!kids.contains(c) && c != prev && c != next)
	// return comPs;
	// for (Vec2D v : region.vertices)
	// for (Vec2D cv : c.region.vertices)
	// if (v.equals(cv))
	// comPs.add(v);
	// // for (Vec2D v : comPs)
	// // ellipse(v.x, v.y, 4, 4);
	// return comPs;
	// }
	//
	// public void edgeMark() {
	// for (Cell c : kids)
	// selected.findCommonEdges(c);
	// }
}