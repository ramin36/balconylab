package voronoi.animation1;

import java.util.ArrayList;
import java.util.Arrays;

import math.MVector;
import voronoi.CircleVoronoi;
import voronoi.VCell;

public class SpecCircleVoronoi extends CircleVoronoi {
	
	public Integer[]	ringSeeds;
	
	public boolean		morph;
	
	public boolean[]	ringDisplay;
	
	public SpecCircleVoronoi() {
		super(new MVector());
	}
	
	@Override
	public void spread(Integer[] seeds, float radius) {
		ringSeeds = seeds;
		ringDisplay = new boolean[seeds.length];
		Arrays.fill(ringDisplay, true);
		super.spread(seeds, radius);
	}
	
	public void renderOuter(boolean outer) {
		ringDisplay[ringDisplay.length - 1] = false;
	}
	
	public void onlyDisplayLast() {
		Arrays.fill(ringDisplay, false);
		if (ringDisplay.length > 1)
			ringDisplay[ringDisplay.length - 2] = true;
	}
	
	@Override
	public void render() {
		for (int i = 0; i < rings.size(); i++)
			if (ringDisplay[i]) {
				ArrayList<VCell> cells = (ArrayList<VCell>) rings.get(i);
				for (VCell c : cells)
					c.render();
			}
	}
	
	// counts from the outer border (0), negative values from inside (-1: inner rings)
	// TODO TEST
	public void displayRing(int i, boolean b) {
		i = getRing(i);
		ringDisplay[i] = b;
	}
	
	public void displayOnly(int i) {
		Arrays.fill(ringDisplay, false);
		i = getRing(i);
		ringDisplay[i] = true;
	}
	
	private int getRing(int r) {
		int rl = ringDisplay.length;
		return (rl - ((r + 1) % rl)) % rl;
	}
}
