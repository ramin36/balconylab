package voronoi.animation1;

import helpers.AppImgSaver;

import java.util.LinkedList;

import rap.RAP;
import rap.Resolution;
import voronoi.CircleVoronoi;

public class CircleVoronoiScaleAnimation extends RAP {
	
	int								layers						= 200;
	LinkedList<SpecCircleVoronoi>	cvs							= new LinkedList<SpecCircleVoronoi>();
	
	CircVorVideoValues				values;
	
	AppImgSaver						saver;
	
	int								minNumRings					= 7;
	int								maxNumRings					= 7;
	
	int								minNumSeeds					= 2;
	int								maxNumSeeds					= 11;
	
	float							scalePerLayer				= .00001f;
	int								strokeWeight				= 3;
	
	int								maxMorphs					= 1;
	float							newMorphProb				= 1f;
	float							ringMorphProb				= 1f;
	float							morphStrengh				= 2;
	
	float							globalPhaseShift			= 0;									// HALF_PI;
	float							phaseShiftSpeed				= 0.0005f;
	
	boolean							animate						= true;
	boolean							opacityLayerDecrease		= true;
	// TODO fix this crazy shit
	boolean							randomPhaseOfNewVorCircs	= true;
	
	// defined in setup
	private float					voronoiSize;
	private int						opacPerLayer;
	
	// variables
	private int						numberOfMorphs				= 0;
	
	@Override
	public void setup() {
		super.setup();
		size(Resolution.hd720p, P3D);
		voronoiSize = height * 0.9f;
		opacPerLayer = Functions.opacReducePerLayer(layers);
		if (!opacityLayerDecrease)
			stroke(255);
		strokeWeight(strokeWeight);
		values = new CircVorVideoValues();
		noFill();
		for (int i = 0; i < layers; i++)
			if (i == 0)
				cvs.add(newCircV());
			else {
				cvs.add(newVorDependingOnMaxMorp());
			}
		AppImgSaver.autoSaveAllToFrameAndExit(25 * 20);
		frameRate(100);
	}
	
	@Override
	public void draw() {
		background(0);
		translate(center());
		for (int i = 0; i < layers; i++) {
			if (opacityLayerDecrease)
				stroke(255, 255 - opacPerLayer * i);
			scale(1 - scalePerLayer * layers);
			if (cvs.get(i).morph)
				fill(255, 155 - opacPerLayer * .5f * i);
			else if (i == 0)
				fill(0, 200);
			else
				noFill();
			cvs.get(i).render();
		}
		if (animate) {
			cvs.add(newVorDependingOnMaxMorp());
			removeFirst();
		}
		// System.out.println(frameRate);
		// bezierDetail((int) map(mouseX, 0, width, 0, 50));
		// saver.save();
		// System.out.println(frameCount + " " + (frameCount / 30));
	}
	
	private void removeFirst() {
		if (cvs.removeFirst().morph)
			numberOfMorphs--;
	}
	
	private SpecCircleVoronoi newVorDependingOnMaxMorp() {
		if (numberOfMorphs < maxMorphs && random(1) < newMorphProb) {
			SpecCircleVoronoi cv = newCircV(morphRings(cvs.getLast().ringSeeds, ringMorphProb),
					newPhase(true));
			cv.morph = true;
			numberOfMorphs++;
			return cv;
		} else
			return newCircV(cvs.getLast().ringSeeds, newPhase(false));
	}
	
	private float newPhase(boolean morph) {
		if (morph) {
			globalPhaseShift = random(TWO_PI);
			return globalPhaseShift;
		} else {
			globalPhaseShift += phaseShiftSpeed;
			return globalPhaseShift;
		}
	}
	
	private SpecCircleVoronoi newCircV() {
		return newCircV(
				CircleVoronoi.generateRings(minNumRings, maxNumRings, minNumSeeds, maxNumSeeds),
				newPhase(true));
	}
	
	private SpecCircleVoronoi newCircV(Integer[] rings) {
		SpecCircleVoronoi cv = new SpecCircleVoronoi();
		cv.spread(rings, voronoiSize);
		cv.renderOuter(false);
		// cv.onlyDisplayLast();
		// cv.displayOnly(2);
		// cv.displayRing(2, true);
		return cv;
	}
	
	private SpecCircleVoronoi newCircV(Integer[] rings, float phase) {
		SpecCircleVoronoi cv = new SpecCircleVoronoi();
		cv.globalPhase = phase;
		cv.spread(rings, voronoiSize);
		cv.renderOuter(false);
		return cv;
	}
	
	private Integer[] morphRings(Integer[] rings, float strength) {
		Integer[] morphed = new Integer[rings.length];
		for (int i = 0; i < morphed.length; i++) {
			int seeds = rings[i];
			if (random(1) < strength) {
				seeds += (int) random(-morphStrengh, morphStrengh);
				seeds = RAP.constrain(seeds, minNumSeeds, maxNumSeeds);
			}
			morphed[i] = seeds;
		}
		return morphed;
	}
	
	public static class CircVorVideoValues {
		
		// @Value(min = 10, max = 100)
		int		layers			= 40;
		float	morphStrength	= 0.4f;
		int		frameRate		= 30;
		float	scalePerLayer	= 0.0002f;
	}
}
