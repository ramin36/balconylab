package voronoi;

import helpers.AppImgSaver;
import image.ImgCollections;
import processing.core.PImage;
import rap.RAP;
import toxi.geom.Polygon2D;

public class VoronoiTest extends RAP {
	
	CircleVoronoi	cv;
	int				layers	= 6;
	PImage[]		imgs;
	AppImgSaver		saver;
	
	@Override
	public void setup() {
		super.setup();
		size(800, 800, P3D);
		noStroke();
		ImgCollections.rap = this;
		// imgs = ImgCollections.loadNumberedCollection(new File("C:\\Users\\Ramin\\Pictures\\bears"), new String[]{"jpg","png");
		// imgs = ImgCollections.loadNumberedCollection(new File("C:\\Users\\Ramin\\Pictures\\bears"));
		noFill();
		newCircV();
		// saver = new AppImgSaver(this);
	}
	
	private void newCircV() {
		cv = new CircleVoronoi();
		// cv.shift = center();
		cv.numRings = (int) random(3, 10);
		Integer[] ringCells = new Integer[cv.numRings];
		for (int i = 0; i < cv.numRings; i++)
			ringCells[i] = (int) random(3, 30);
		cv.spread(ringCells, 550);
	}
	
	@Override
	public void draw() {
		background(0);
		// poly(cv.getRegions().getFirst(), img);
		translate(center());
		for (int i = 0; i < layers; i++) {
			fill(255 - i * 30, 150 + sin(PI / 6f * i) * 50, (100 + i * 30) % 250, 30);
			// translate(PVector.sub(new PVector(), center()));
			scale(1 - i * mouseX / (float) width * 1f);
			int img = 0;
			noFill();
			stroke(255);
			tint(150, 53, 70, 150 - 2 * i);
			for (Polygon2D poly : cv.getRegions()) {
				// poly(poly, imgs[img]);
				// img = (img + 1) % imgs.length;
				// poly(poly, imgs[(int) random(imgs.length)]);
				// fill(cv.clr);
				poly(poly);
			}
		}
		// saveFrame("frame-##.jpg");
	}
	
	@Override
	public void mousePressed() {
		// saveFrame("f-##.png");
		// saver.save();
		newCircV();
	}
}
