package helpers;

import java.util.Random;

public class MyRnd {
	
	public static Integer		seed;
	private static final Random	rnd;
	
	static {
		if (seed == null)
			rnd = new Random();
		else
			rnd = new Random(seed);
	}
	
	public static int nextInt() {
		return rnd.nextInt();
	}
	
	public static int nextInt(int v) {
		return rnd.nextInt(v);
	}
}
