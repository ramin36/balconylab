package helpers;

public class General {
	
	public enum ValueType {
		integer, floating
	};
	
	/**
	 * Checks if a value is in the bunds
	 * 
	 * @param value
	 * @param min
	 * @param max
	 * @return
	 */
	public static boolean inBounds(int value, int min, int max) {
		return value >= min && value <= max;
	}
	
	/**
	 * checks if all value are in their specfic bounds
	 * 
	 * @param value
	 * @param min
	 * @param max
	 * @return
	 */
	public static boolean inBounds(int[] value, int[] min, int[] max) {
		for (int i = 0; i < value.length; i++)
			if (value[i] < min[i] || value[i] > max[i])
				return false;
		return true;
	}
	
}
