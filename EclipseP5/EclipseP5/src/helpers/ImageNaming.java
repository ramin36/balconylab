package helpers;

import java.io.File;
import java.util.ArrayList;
import java.util.logging.Logger;

import processing.core.PImage;
import rap.RAP;

public class ImageNaming {
	
	public static RAP	rap;
	
	static String		folderN		= "C:/Users/Ramin/UDK Bewerbung/DVD/Bildserien/Erweiterte Raute";
	static String		ending[]	= { ".tif", ".bmp", ".png", ".jpg" };
	static String		basicName	= "er";
	static String		endingNum	= "####";
	static int			autoadd		= 0;
	static boolean		deleteOld	= true;
	
	Logger				logger		= Logger.getLogger(ImageNaming.class.getSimpleName());
	
	public static void main(String[] args) {
		
		noZeros("C:/Users/Ramin/Documents/Processing/git/Flocksoundss/pics");
		// classic renameProgramm
		// String s = getFileFormat(new File("a.tif"));
		// s = getFileFormat(new File[] { new File("bla.bmp") });
		//
		// File[] list = fileList(folderN);
		// int no = autoadd;
		// DecimalFormat nf = new DecimalFormat(endingNum);
		// String end = getFileFormat(list);
		// for (File f : list) {
		// try {
		// FileUtils.copyFile(f, new File(folderN + "/new/" + basicName + "-" + nf.format(no) + end));
		// no++;
		// f.delete();
		// } catch (IOException e) {
		// e.printStackTrace();
		// }
		// System.out.println(folderN + "/new/" + basicName + "-" + nf.format(no) + end);
		// }
		// System.out.println("done");
	}
	
	public static void noZeros(String folder) {
		noZeros(new File(folder));
	}
	
	public static void noZeros(File folder) {
		// new
		File[] fileList = folder.listFiles();
		String format = getFileFormat(folder);
		String folderName = folder.getAbsolutePath();
		for (File f : fileList) {
			int n = getNumber(f);
			f.renameTo(new File(folderName + "\\" + basicName + "-" + n + format));
		}
	}
	
	public static PImage[] getImgs(File folder, String format) {
		File[] fileList = folder.listFiles();
		fileList = formatFilter(fileList, format);
		PImage[] ret = new PImage[fileList.length];
		for (int i = 0; i < ret.length; i++)
			ret[i] = rap.loadImage(fileList[i].getAbsolutePath());
		return ret;
	}
	
	private static File[] formatFilter(File[] fileList, String format) {
		ArrayList<File> filtered = new ArrayList<File>();
		for (File f : fileList)
			if (f.getName().endsWith(format))
				filtered.add(f);
		return filtered.toArray(new File[filtered.size()]);
	}
	
	public static File[] fileList(String folderName) {
		return new File(folderName).listFiles();
	}
	
	public static String getFileFormat(File... f) {
		if (f.length == 1 && f[0].isDirectory())
			return getFileFormat(f[0].listFiles());
		for (File file : f)
			for (int e = 0; e < ending.length; e++)
				if (file.getName().endsWith(ending[e]))
					return ending[e];
		return "";
	}
	
	public static String[] splitNameFormat(String file) {
		int dotIndex = file.lastIndexOf(".");
		String name = file.substring(0, dotIndex);
		String type = file.substring(dotIndex);
		return new String[] { name, type };
	}
	
	public static String basicName(File... f) {
		String name = f[0].getName();
		int index = name.indexOf("-");
		return name.substring(0, index);
	}
	
	public static int getNumber(File f) throws NumberFormatException {
		String name = f.getName();
		int index = name.lastIndexOf("-");
		int dotIndex = name.indexOf(".");
		return Integer.valueOf(name.substring(index + 1, dotIndex));
	}
	
}
