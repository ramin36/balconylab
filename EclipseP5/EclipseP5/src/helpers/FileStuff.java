package helpers;

public class FileStuff {
	
	/**
	 * takes a absolutePath and gives a new path with which is like
	 * path + newFolder + fileName
	 * 
	 * @param fileN
	 * @param newFolder
	 * @return
	 */
	public static String fileToNewFolder(String fileN, String newFolder) {
		int li = fileN.lastIndexOf("/");
		return fileN.substring(0, li) + "/" + newFolder + fileN.substring(li);
	}
	
	/**
	 * changes the ending of a filename to make it another type
	 * 
	 * @param fileN
	 * @param newType
	 * @return
	 */
	public static String changeType(String fileN, String newType) {
		return fileN.substring(0, fileN.lastIndexOf(".")) + newType;
	}
}
