package helpers.test;

import helpers.AppImgSaver;
import rap.RAP;

public class AppImgSaverTest extends RAP {
	
	@Override
	public void setup() {
		super.setup();
		// AppImgSaver.createVideo();
		AppImgSaver.autoSaveAllToFrameAndExit(10);
	}
	
	@Override
	public void draw() {
		background(0);
	}
	
}
