package helpers;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import rap.RAP;
import rap.RAPUser;

public class AppImgSaver implements RAPUser {
	
	private static AppImgSaver	saver;
	
	public static RAP			rap	= RAP.getRAP(AppImgSaver.class);
	private final int			endAfterFrame;
	private final boolean		exitAfterLastFrame;
	
	private int					i	= 0;
	
	static boolean				recording;
	
	public static void autoSaveAll() {
		saver = new AppImgSaver(-1, false);
	}
	
	public static void autoSaveAllToFrame(int frame) {
		saver = new AppImgSaver(frame, false);
	}
	
	public static void autoSaveAllToFrameAndExit(int frame) {
		saver = new AppImgSaver(frame, true);
	}
	
	private AppImgSaver(int endAfterSave, boolean exitAfterLastFrame) {
		this.endAfterFrame = endAfterSave;
		this.exitAfterLastFrame = exitAfterLastFrame;
		System.out.println("saving in: " + RAP.runDir);
		rap.registerMethod("post", this);
		recording = true;
	}
	
	public static void save() {
		rap.save(RAP.runDir + "\\s-" + (saver.i++));
		System.out.println(saver.i + "/" + saver.endAfterFrame);
		if (saver.i == saver.endAfterFrame && saver.exitAfterLastFrame) {
			// createVideo();
			rap.exit();
			recording = false;
		}
	}
	
	public void post() {
		if (recording)
			save();
	}
	
	public static void createVideo() {
		File f = new File(rap.sketchPath).getParentFile().getParentFile();
		// System.out.println(f);
		String ffmpeg = f + "\\ffmpeg.exe";
		// rap.open(f
		// + File.pathSeparator
		// + "ffmpeg.exe -start_number 0 -r 30 -i c:/v/s-%d.tif  -vcodec mpeg4 -q 1 c:/v/test2.avi");
		System.out.println(ffmpeg + " -start_number 0 -r 30 -i " + rap.runDir
				+ "\\s-%d.tif  -vcodec mpeg4 -q 1 " + rap.runDir + "\\movie.avi");
		try {
			Process process = Runtime.getRuntime().exec(
					ffmpeg + " -start_number 0 -r 30 -i " + rap.runDir
							+ "s-%d.tif  -vcodec mpeg4 -q 1 " + rap.runDir + "movie2.avi");
			final BufferedReader reader = new BufferedReader(new InputStreamReader(
					process.getInputStream()));
			String line;
			while ((line = reader.readLine()) != null) {
				System.out.println(line);
			}
			
			// new Thread(new Runnable() {
			//
			// @Override
			// public void run() {
			// while (true) {
			// try {
			// if (reader.ready())
			// System.out.println(reader.readLine());
			// } catch (IOException e) {
			// e.printStackTrace();
			// }
			// }
			// }
			//
			// }).start();
		} catch (IOException e1) {
			e1.printStackTrace();
			return;
		}
	}
}
