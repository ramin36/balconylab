package neighbourhood;

import image.PImageMap;
import math.MVector;
import math.structure.Look;
import math.structure.Rect;
import rap.RAP;
import toxi.geom.Vec2D;

import com.drew.lang.GeoLocation;

/**
 * this program takes the images from the virtual book folder and stores them with their gps adress in an imagemap object
 * then it scales their position into the screen rectangle and indicates their location
 * known error , scaling from something, which is not a square into a square
 * 
 * @author Ramin
 * 
 */
public class Imagetravel extends RAP {
	
	PImageMap	imageMap;
	float		geoLatScale, geoLongScale;
	Rect		frame;
	
	@Override
	public void setup() {
		super.setup();
		size(600, 600);
		
		imageMap = new PImageMap("C:/the virtual book/cross photos");
		frame = new Rect(new Vec2D(0, 0), 500, 500);
		Look.quickDraw = true;
		frameRate(1);
	}
	
	@Override
	public void draw() {
		background(0);
		translate(50, 50);
		stroke(255);
		noFill();
		frame.draw();
		for (GeoLocation geoLoc : imageMap.pImageMap.values()) {
			MVector v = new MVector((float) geoLoc.getLongitude(), (float) geoLoc.getLatitude());
			// System.out.println(v);
			ellipse(imageMap.box.map(v, frame), 6, 6);
		}
	}
}
