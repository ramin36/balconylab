package test;

import image.ColorHelper;
import processing.core.PApplet;

public class FadeTest extends PApplet {
	
	int	li		= 40, sx;
	int	black	= color(0);
	int	bg;
	
	@Override
	public void setup() {
		super.setup();
		size(800, 400);
		bg = color(3);
		background(bg);
		sx = width / li;
	}
	
	@Override
	public void draw() {
		pushMatrix();
		loadPixels();
		for (int i = 0; i < li; i++) {
			bg = ColorHelper.addTransparency(bg, min(255, i * 2));
			stroke(bg);
			fill(bg);
			rect(0, 0, sx, frameCount - 1);
			stroke(255);
			line(0, frameCount, sx, frameCount);
			// print(" " + i + ":" + ((get(sx * i + 2, 2) == black) ? "x" : "o"));
			translate(sx, 0);
		}
		// println();
		popMatrix();
	}
	
	@Override
	public void mousePressed() {
		int clr = get(mouseX, mouseY);
		println(red(clr) + "," + green(clr) + "," + blue(clr));
	}
}
