package test;

import processing.core.PVector;
import rap.ExtraWindow;
import rap.RAP;
import rap.extraWindows.PolygonMaker;

public class ExtraWindowTest extends RAP {
	
	ExtraWindow	x1, x2;
	
	@Override
	public void setup() {
		super.setup();
		size(600, 300);
		x1 = ExtraWindow.getExtraWindow(new PVector(600, 300), "x1", PolygonMaker.class);
		// x2 = ExtraWindow.getExtraWindow(new PVector(200, 200), "x2"); works
	}
	
	@Override
	public void draw() {
		
	}
	
}
