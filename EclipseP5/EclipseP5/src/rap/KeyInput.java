package rap;



public interface KeyInput {
	
	
	void keyPressed(char key);
	void keyReleased(char key);
	void keyTyped(char key);
	
}
