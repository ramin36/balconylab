package rap;

import java.lang.reflect.Field;
import java.util.ArrayList;

import processing.core.PGraphics;
import processing.core.PImage;

public class RAPHelper implements RAPUser {
	
	public static RAP	rap;
	
	static public final int	HORIZONTAL	= 0, VERTICAL = 1;
	
	public static int rndColor() {
		return rap.color((int) rap.random(255), (int) rap.random(255), (int) rap.random(255));
	}
	
	public static int newRed(int clr, int red) {
		return rap.color(red, rap.green(clr), rap.blue(clr));
	}
	
	public static int newGreen(int clr, int green) {
		return rap.color(rap.red(clr), green, rap.blue(clr));
	}
	
	public static int newBlue(int clr, int blue) {
		return rap.color(rap.red(clr), rap.green(clr), blue);
	}
	
	public static int addAlpha(int clr, int alpha) {
		return rap.color(rap.red(clr), rap.green(clr), rap.blue(clr), alpha);
	}
	
	public static int setAlpha(int clr, int alpha) {
		clr = clr & (0 << 24);
		clr = clr | (alpha << 24);
		return clr;
	}
	
	// 0 1 2
	// 3 4 5
	//
	// 2 1 0
	// 5 4 3
	// TODO: scale does that...
	public static PImage mirrorImage(PImage img, int direction) {
		img.loadPixels();
		PImage result = rap.createImage(img.width, img.height, img.format);
		result.loadPixels();
		int l = img.pixels.length, w = img.width, h = img.height;
		switch (direction) {
			case HORIZONTAL:
				for (int y = 0; y < h; y++)
					for (int x = 0; x < w; x++)
						// int u = (l - (w - x) - y * w);
						// System.out.println(x + "," + y + ":" + (x + y * w) +
						// " > " + (u % w) + "," + (u / w) + ":"
						// + (l - (w - x) - y * w));
						result.pixels[l - (w - x) - y * w] = img.pixels[x + y * w];
				break;
			case VERTICAL:
				for (int y = 0; y < h; y++)
					for (int x = 0; x < w; x++)
						// int u = (w - x - 1) + y * w;
						// System.out.println(x + "," + y + ":" + (x + y * w) +
						// " > " + (u % w) + "," + (u / w) + ":" + u);
						result.pixels[(w - x) + y * w] = img.pixels[x + y * w];
		}
		return result;
	}
	
	public static PImage curArc(PImage img, float start, float end) {
		int w = img.width, h = img.height;
		PImage ret = rap.createImage(w, h, img.format);
		PGraphics pg = rap.createGraphics(w, h);
		pg.beginDraw();
		pg.stroke(0, 0, 255);
		pg.fill(0, 0, 255);
		pg.ellipseMode(rap.DIAMETER);
		pg.arc(0, 0, w, h, start, end);
		pg.endDraw();
		ret.copy(img, 0, 0, w, h, 0, 0, w, h);
		// ret.mask(pg);
		return ret;
	}
	
	private static ArrayList<Class<? extends RAPUser>>	rapUsers	= new ArrayList<Class<? extends RAPUser>>();
	
	public static void initRAP(RAP _rap) {
		rap = _rap;
		for (Class<? extends RAPUser> user : rapUsers) {
			Field f;
			try {
				f = user.getDeclaredField("rap");
				f.set(user, rap);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public static RAP getRAP(Class<? extends RAPUser> e) {
		if (rap != null)
			return rap;
		else {
			rapUsers.add(e);
			return null;
		}
	}
	
}
