package rap;

import image.Util;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import math.MVector;
import math.structure.Rect;
import processing.core.PApplet;
import processing.core.PImage;
import processing.core.PVector;
import toxi.geom.Polygon2D;
import toxi.geom.Triangle2D;
import toxi.geom.Vec2D;

public class RAP extends PApplet {
	
	private static final long	serialVersionUID	= 1L;
	
	static public KeyInputMgmt	keyMgmt;
	// static public InputDistr;
	
	public static RAP			rap;
	
	public static String		workingDirectory;
	/**
	 * dir specific for each run
	 */
	public static String		runDir;
	
	protected boolean			fullScreen;
	
	private String				renderer;
	
	public static String		jpg					= "jpg";
	
	@Override
	public void setup() {
		rap = this;
		RAPHelper.rap = this;
		RAPHelper.initRAP(this);
		MVector.rap = this;
		if (workingDirectory == null)
			workingDirectory = getAppRefFolder();
		sketchPath = workingDirectory;
		
		runDir = workingDirectory + new SimpleDateFormat("yyMMdd-kk-mm").format(new Date());
		for (int i = 1; new File(runDir).exists(); i++) {
			runDir += "-" + i;
		}
	}
	
	public void fullscreen() {
		size(Toolkit.getDefaultToolkit().getScreenSize());
	}
	
	public void size(PVector sz) {
		size((int) sz.x, (int) sz.y);
	}
	
	protected void size(Dimension size) {
		size(size.width, size.width);
	}
	
	/**
	 * Applet size as MVector
	 * 
	 * @return
	 */
	public MVector getSizeMV() {
		return new MVector(width, height);
	}
	
	public void draw(Triangle2D tria) {
		triangle(tria.a.x, tria.a.y, tria.b.x, tria.b.y, tria.c.x, tria.c.y);
	}
	
	protected void size(MVector sz, String mode) {
		size((int) sz.x, (int) sz.y, mode);
	}
	
	public void vertex(PVector v) { // doesnt get P2D
		if (getRenderer().equals("Graphics2D"))
			vertex(v.x, v.y);
		else
			vertex(v.x, v.y, v.z);
	}
	
	public void vertex2D(PVector v) {
		vertex(v.x, v.y);
	}
	
	protected String getRenderer() {
		if (renderer == null)
			renderer = g.getClass().getSimpleName();
		return renderer;
	}
	
	protected void size(float x, float y) {
		size((int) x, (int) y);
	}
	
	public void ellipse(PVector mid, float a, float b) {
		ellipse(mid.x, mid.y, a, b);
	}
	
	public MVector center() {
		return new MVector(width / 2, height / 2);
	}
	
	public void line(PVector a, PVector b) {
		line(a.x, a.y, b.x, b.y);
	}
	
	public void triangle(PVector a, PVector b, PVector c) {
		triangle(a.x, a.y, b.x, b.y, c.x, c.y);
	}
	
	public void bezier(MVector a, MVector b, MVector c, MVector d) {
		bezier(a.x, a.y, b.x, b.y, c.x, c.y, d.x, d.y);
	}
	
	// public void bezier(MVector a,MVector b,MVector c,MVector d){
	// bezier(a.x, a.y, a.z, b.x, b.y, b.z, c.x, c.y, c.z,d.x, d.y, d.z);
	// }
	//
	public void circle(toxi.geom.Circle c) {
		ellipseMode(RADIUS);
		ellipse(c.x, c.y, c.getRadius(), c.getRadius());
	}
	
	public MVector randomEdgePoint() {
		return new Rect(new Vec2D(), width, height).getRandomPointOnEdge();
	}
	
	public MVector randomPoint() {
		return new MVector(random(width), random(height));
	}
	
	public void addKeyInput(KeyInput input) {
		if (keyMgmt == null) {
			System.err.println("Initializing Key Input Mgmt");
			keyMgmt = new KeyInputMgmt(this);
		}
		keyMgmt.add(input);
	}
	
	@Override
	public void keyPressed() {
		if (keyMgmt != null)
			keyMgmt.keyPressed(key);
	}
	
	public MVector mousePos() {
		return new MVector(mouseX, mouseY);
	}
	
	/**
	 * returns the width and height of an image as MVector
	 * 
	 * @param img
	 *            the image
	 * @return a MVector holiding width and height of the image
	 */
	static public MVector imageSize(PImage img) {
		return new MVector(img.width, img.height);
	}
	
	/**
	 * calls image method, MVector instead of x,y
	 * 
	 * @param img
	 * @param pos
	 */
	public void image(PImage img, MVector pos) {
		image(img, pos.x, pos.y);
	}
	
	public void image(PImage img, MVector pos, MVector size) {
		image(img, pos.x, pos.y, size.x, size.y);
	}
	
	public void point(MVector pos) {
		point(pos.xi(), pos.yi());
	}
	
	public int get(MVector pos) {
		return get(pos.xi(), pos.yi());
	}
	
	public void set(MVector pos, int clr) {
		set(pos.xi(), pos.yi(), clr);
	}
	
	public boolean inFrame(MVector v) {
		return v.x >= 0 && v.x < width && v.y >= 0 && v.y < height;
	}
	
	public void fade(int color) {
		fill(color);
		rect(-10, -10, width + 10, height + 10);
	}
	
	/**
	 * get the folder in the worksace & project with the same name as the class
	 * 
	 * @return
	 */
	public String getAppRefFolder() {
		String path = new File("").getAbsolutePath();
		// get out of "bin"
		return path.substring(0, path.length() - 3) + "apps" + File.separator
				+ getClass().getSimpleName() + File.separator;
	}
	
	protected void size(int sqSize) {
		size(sqSize, sqSize);
	}
	
	public void imageCentered(PImage img) {
		int w = img.width;
		int h = img.height;
		float scale = 1;
		if (w > width)
			scale = (float) w / width;
		if (h > height)
			scale = min(scale, (float) h / height);
		image(img, center().x, center().y, img.width * scale, img.height * scale);
	}
	
	public void translate(PVector v) {
		translate(v.x, v.y);
	}
	
	public void poly(Polygon2D poly) {
		beginShape();
		for (Vec2D v : poly.vertices)
			vertex(v.x, v.y);
		endShape(CLOSE);
	}
	
	public void poly(Polygon2D poly, PImage texture) {
		int numVert = poly.vertices.size();
		PVector[] vertices = Util.vertices(texture);
		// System.out.println(Arrays.toString(vertices));
		beginShape();
		texture(texture);
		float i = 0;
		for (Vec2D v : poly.vertices) {
			int k = (int) ((i / numVert) * 4);
			PVector edgeV = vertices[k];
			// System.out.println(v + "," + edgeV + "/" + k);
			vertex(v.x, v.y, edgeV.x, edgeV.y);
			i++;
		}
		endShape();
	}
	
	protected void size(Point size, String mode) {
		size(size.x, size.y, mode);
	}
	
	public static RAP getRAP(Class<? extends RAPUser> user) {
		return RAPHelper.getRAP(user);
	}
	
	public void textBox(String text, PVector location) {
		rectMode(CENTER);
		float w = textWidth(text);
		
		rap.stroke(255);
		rap.noFill();
		rap.rect(location.x, location.y, w + 20, this.g.textSize + 10);
		text(text, location.x, location.y);
	}
	
	public Rect getTextBox(String text, PVector location) {
		float w = textWidth(text);
		return new Rect(new Vec2D(location.x - (w + 20) / 2, location.y - (this.g.textSize + 10)
				/ 2), w + 20, this.g.textSize + 10);
	}
	
	public void text(String text, PVector location) {
		text(text, location.x, location.y);
	}
	
	// should already be somewhere else
	public int randomColor() {
		return color((int) random(255), (int) random(255), (int) random(255));
	}
	
	public void triangle(Triangle2D triangle) {
		triangle(triangle.a.x, triangle.a.y, triangle.b.x, triangle.b.y, triangle.c.x, triangle.c.y);
	}
	
	public void vertex(Vec2D v) {
		vertex(v.x, v.y);
	}
	
	public Rect frameAsRect() {
		return new Rect(width, height);
	}
	
	public float random() {
		return random(1);
	}
	
	public int randomInt(int max) {
		return (int) random(max);
	}
	
	public boolean randomBool() {
		return random() > .5f;
	}
}
