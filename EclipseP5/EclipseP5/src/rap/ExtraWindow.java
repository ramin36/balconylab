package rap;

import generalController.library.values.Values;

import java.awt.Frame;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.lang.reflect.Constructor;

import processing.core.PVector;

public abstract class ExtraWindow extends RAP implements RAPUser {
	
	public static RAP	rap	= RAP.getRAP(ExtraWindow.class);
	
	Frame				f;
	
	public int			w, h;
	
	private Values		values;
	
	public static ExtraWindow getExtraWindow(PVector size, String name,
			Class<? extends ExtraWindow> xwClazz) {
		try {
			Constructor<? extends ExtraWindow> cstr = xwClazz.getConstructor(new Class[] {
					Integer.TYPE, Integer.TYPE });
			ExtraWindow p = cstr.newInstance((int) size.x, (int) size.y);
			p.f = new Frame(name);
			p.f.add(p);
			p.init();
			p.f.setSize(p.w, p.h);
			p.f.setLocation(100, 100);
			// p.f.setResizable(false);
			p.f.setVisible(true);
			return p;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public ExtraWindow(int theWidth, int theHeight) {
		w = theWidth;
		h = theHeight;
	}
	
	@Override
	public void setup() {
		super.setup();
		size(w, h);
		sketchPath = rap.sketchPath;
		workingDirectory = rap.workingDirectory;
		runDir = rap.runDir;
		f = (Frame) getAccessibleContext().getAccessibleParent();
		f.addWindowListener(new WindowListener() {
			
			@Override
			public void windowActivated(WindowEvent arg0) {
			}
			
			@Override
			public void windowClosed(WindowEvent e) {
			}
			
			@Override
			public void windowClosing(WindowEvent e) {
				println("close");
				f.setVisible(false);
			}
			
			@Override
			public void windowDeactivated(WindowEvent e) {
			}
			
			@Override
			public void windowDeiconified(WindowEvent e) {
			}
			
			@Override
			public void windowIconified(WindowEvent e) {
			}
			
			@Override
			public void windowOpened(WindowEvent e) {
			}
		});
	}
	
}
