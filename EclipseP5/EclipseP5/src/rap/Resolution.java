package rap;

import java.awt.Point;

import math.MVector;

public class Resolution {
	
	static final public Point	hd720p			= new Point(1280, 720);
	static final public Point	hd1080p			= new Point(1920, 1080);
	
	static final public MVector	dinA2_300ppi	= new MVector(7016, 4961);
	static final public MVector	dinA3_300ppi	= new MVector(4961, 3508);
	static final public MVector	dinA4_300ppi	= new MVector(3508, 2480);
	static final public MVector	dinA5_300ppi	= new MVector(2480, 1748);
}
