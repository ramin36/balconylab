package rap;



import java.util.ArrayList;


public class KeyInputMgmt {
	
	public static RAP	ap;
	
	ArrayList<KeyInput>	inputs;
	
	KeyInput			active;
	
	boolean				fKeySwitch	= true;

	private int	keyCode;
	
	public KeyInputMgmt(RAP ap) {
		super();
		KeyInputMgmt.ap = ap;
	}
	
	public void add(KeyInput input) {
		if (inputs == null)
			inputs = new ArrayList<KeyInput>();
		inputs.add(input);
		active = input;
	}
	
	public void keyPressed(char key) {
//		System.out.println((int)key);
//		 System.out.println(ap.keyCode);
		 keyCode = ap.keyCode;
		if (keyCode >= 112 && keyCode <= 123) {
			int fkey = ap.keyCode - 111;
			if (inputs.size() >= fkey) {
				active = inputs.get(fkey - 1);
				System.out.println("switching to "+active.getClass().getSimpleName());
			}
			else 
				System.out.println("no KeyInput");
		} else
			active.keyPressed(key);
	}
}
