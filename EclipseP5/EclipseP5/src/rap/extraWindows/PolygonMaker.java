package rap.extraWindows;

import java.util.ArrayList;

import math.MVector;
import math.structure.Polygon;
import rap.ExtraWindow;
import toxi.geom.Polygon2D;
import toxi.geom.Vec2D;

import com.jogamp.newt.event.KeyEvent;

/**
 * left button: add a point
 * left drag + shift: move a point
 * right button + shift: remove a point
 * backspace: start new
 * 
 * @author Ramin
 * 
 */
public class PolygonMaker extends ExtraWindow {
	
	Polygon2D			poly;
	
	ArrayList<MVector>	points	= new ArrayList<MVector>();
	MVector				selected;
	
	public PolygonMaker(int theWidth, int theHeight) {
		super(theWidth, theHeight);
	}
	
	@Override
	public void setup() {
		super.setup();
		textAlign(CENTER, CENTER);
		cursor(CROSS);
		ellipseMode(CENTER);
	}
	
	private void polyStuff() {
		if (points.size() > 2)
			poly = new Polygon2D(MVector.MVsToVec2Ds(points));
	}
	
	@Override
	public void draw() {
		background(0);
		setCursor();
		drawPrePolyStuff();
		drawSelected();
		drawPoly();
	}
	
	private void drawPrePolyStuff() {
		noFill();
		stroke(255);
		if (points.size() == 0) {
			fill(255);
			text("click!", width / 2, height / 2);
		} else if (points.size() == 1)
			point(new MVector(points.get(0)));
		else if (points.size() == 2)
			line(new MVector(points.get(0)), new MVector(points.get(1)));
		// else if (points.size() > 2 && recreatePG) {
		// polyStuff();
		// recreatePG = false;
		// }
	}
	
	private void drawSelected() {
		if (selected != null) {
			stroke(0, 200, 0);
			ellipse(selected, 4, 4);
		}
	}
	
	private void drawPoly() {
		if (poly != null) {
			stroke(255);
			poly(poly);
			stroke(200, 0, 0);
			poly(Polygon.boundaryBox(poly).toPolygon2D());
			Vec2D c = poly.getCentroid();
			ellipse(c.x, c.y, 6, 6);
		}
	}
	
	private void setCursor() {
		if (keyPressed && keyCode == KeyEvent.VK_SHIFT) {
			noCursor();
			MVector near = MVector.selectFrom(points, this.mousePos());
			if (near != null)
				stroke(255, 0, 0);
			else
				stroke(255);
			fill(255, 100);
			ellipse(mousePos(), 4, 4);
		} else
			cursor(CROSS);
	}
	
	@Override
	public void mouseDragged() {
		if (selected != null) {
			selected.set(mousePos());
			// recreatePG = true;
			polyStuff();
		}
	}
	
	@Override
	public void mousePressed() {
		if (mouseButton == LEFT) {
			if (keyPressed && keyCode == KeyEvent.VK_SHIFT) { // shift, not KeyEvent.SHIFT_MASK, different
				selected = MVector.selectFrom(points, this.mousePos());
				return;
			}
			// if (newPoly) {
			// newPoly = false;
			// points.clear();
			// }
			points.add(new MVector(mouseX, mouseY));
			polyStuff();
			// recreatePG = true;
		} else { // RIGHT
			if (keyPressed && keyCode == KeyEvent.VK_SHIFT) { // shift, not KeyEvent.SHIFT_MASK, different
				selected = MVector.selectFrom(points, this.mousePos());
				points.remove(selected);
				// recreatePG = true;
				polyStuff();
				selected = null;
			}
		}
	}
	
	private void clear() {
		points.clear();
		selected = null;
		poly = null;
	}
	
	@Override
	public void keyPressed() {
		// println(keyCode);
		if (keyCode == KeyEvent.VK_BACK_SPACE)
			clear();
	}
}
