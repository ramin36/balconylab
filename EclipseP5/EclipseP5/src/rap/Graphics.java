package rap;


import java.util.TreeSet;

import math.MVector;
import math.structure.Rect;
import processing.core.PGraphics;

public class Graphics {
	
	static int	p5White	= -1;
	static int	p5Black	= -16777216;
	
	/**
	 * should use  import toxi.geom.PointQuadtree but getPoints return null
	 * @param graphic
	 * @return
	 */
	public static TreeSet<MVector> createPointSetFromWhite(PGraphics graphic) {
//		PointQuadtree tree = new PointQuadtree(new Vec2D(graphic.width, graphic.height), Math.max(graphic.width,
//				graphic.height));
		TreeSet<MVector> tree = new TreeSet<MVector>();
		int count = 0;
		for (int i = 0; i < graphic.width * graphic.height; i++) {
			if (graphic.pixels[i] == p5White) {
				tree.add(Rect.getPosOfInt(graphic.width, graphic.height, i));
				count++;
			}
			// System.out.println(graphic.pixels[i]);
		}
		System.out.println(count);
		// System.out.println(tree.getSize());
		return tree;
	}
	
}
