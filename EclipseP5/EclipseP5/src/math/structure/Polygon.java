package math.structure;

import java.util.ArrayList;
import java.util.List;

import math.MVector;
import toxi.geom.Polygon2D;
import toxi.geom.Vec2D;

import com.google.common.base.Function;

public class Polygon extends Polygon2D {
	
	public static Function<Polygon2D, Vec2D>	center	= new Function<Polygon2D, Vec2D>() {
															
															@Override
															public Vec2D apply(Polygon2D input) {
																return input.getCentroid();
															}
															
														};
	
	/**
	 * Scales the polygon in respect to the centroid
	 * 
	 * @param polygon
	 * @param scale
	 * @return
	 */
	public static Polygon2D resize(Polygon2D polygon, float scale) {
		List<Vec2D> vecs = polygon.vertices;
		List<Vec2D> result = new ArrayList<Vec2D>(vecs.size());
		Vec2D center = polygon.getCentroid();
		for (Vec2D v : vecs)
			result.add(v.sub(center).scale(scale).add(center));
		return new Polygon2D(result);
	}
	
	/**
	 * get the topLeft point of the boundarybox
	 * 
	 * @param poly
	 * @return
	 */
	public static MVector getMinPoint(Polygon2D poly) {
		MVector min = new MVector(poly.vertices.get(0));
		for (Vec2D v : poly.vertices) {
			if (v.x < min.x)
				min.x = v.x;
			if (v.y < min.y)
				min.y = v.y;
		}
		return min;
	}
	
	public static Polygon2D verticesToInt(Polygon2D poly) {
		
		List<Vec2D> points = poly.vertices;
		for (Vec2D p : points)
			p.set(Math.round(p.x), Math.round(p.y));
		return new Polygon2D(points);
	}
	
	public static Rect boundaryBox(Polygon2D poly) {
		float minX = poly.vertices.get(0).x;
		float maxX = minX;
		float minY = poly.vertices.get(0).y;
		float maxY = minY;
		for (Vec2D v : poly.vertices) {
			if (v.x < minX)
				minX = v.x;
			else if (v.x > maxX)
				maxX = v.x;
			if (v.y < minY)
				minY = v.y;
			else if (v.y > maxY)
				maxY = v.y;
		}
		return new Rect(new MVector(minX, minY), new MVector(maxX, maxY));
	}
}
