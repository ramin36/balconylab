package math.structure;
import rap.RAPHelper;




/**
 * also see processing.core.PStyle
 * 
 * @author Ramin
 * 
 */
public class Look {
	
	public boolean	fill;
	public int		fillColor;
	public boolean	stroke;
	public int		strokeColor;
	public float	strokeWeight;
	
	public static boolean quickDraw; // ignore look
	
	public Look() {
		fill = true;
		fillColor = RAPHelper.rap.color(255, 50);
		stroke = true;
		strokeColor = 255;
		strokeWeight = 1;
	}
	
	public Look(boolean fill, int fillColor, boolean stroke, int strokeColor, float strokeWeight) {
		super();
		this.fill = fill;
		this.fillColor = fillColor;
		this.stroke = stroke;
		this.strokeColor = strokeColor;
		this.strokeWeight = strokeWeight;
	}

	static Look	defaultLook	= new Look();
}
