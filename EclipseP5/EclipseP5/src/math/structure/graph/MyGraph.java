package math.structure.graph;

import java.util.ArrayList;
import java.util.List;

public class MyGraph<T extends Node<T>> {
	
	// Node baseNode;
	
	protected ArrayList<T>	nodes		= new ArrayList<T>();
	
	boolean					directed	= false;
	
	public MyGraph() {
		Node.defaultGraph = this;
	}
	
	public void addNode(T n) {
		nodes.add(n);
	}
	
	public void removeNode(T n) {
		nodes.remove(n);
		for (int i = 0; i < n.nbs.size(); i++)
			n.removeNode(n.nbs.get(i));
	}
	
	public int size() {
		return nodes.size();
	}
	
	public List<T> getNodes() {
		return nodes;
	}
}
