package math.structure.graph;

import java.util.ArrayList;

public class Node<T extends Node<T>> {
	
	public static MyGraph	defaultGraph;
	protected MyGraph<T>	graph;
	protected ArrayList<T>	nbs	= new ArrayList<T>();
	
	public Node() {
		this.graph = defaultGraph;
	}
	
	public Node(MyGraph<T> graph) {
		this.graph = graph;
	}
	
	public void addNode(T n) {
		nbs.add(n);
		n.nbs.add((T) this);
	}
	
	public void removeNode(T n) {
		nbs.remove(n);
		if (!graph.directed)
			n.nbs.remove(this);
	}
}
