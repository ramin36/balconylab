package math.structure;

import math.MVector;
import processing.core.PVector;
import rap.RAP;
import toxi.geom.Polygon2D;
import toxi.geom.Vec2D;

public class Rect extends Structure {
	
	public toxi.geom.Rect	rect;
	
	public Rect(float width, float height) {
		super();
		rect = new toxi.geom.Rect(new Vec2D(), new Vec2D(width, height));
	}
	
	public Rect(Vec2D p, float width, float height) {
		super();
		rect = new toxi.geom.Rect(p, new Vec2D(p.x + width, p.y + height));
	}
	
	public Rect(MVector pMin, MVector pMax) {
		super();
		rect = new toxi.geom.Rect(pMin.toVec2D(), pMax.toVec2D());
	}
	
	public Rect(toxi.geom.Rect rect) {
		this.rect = rect;
	}
	
	public MVector getRandomPoint() {
		return new MVector((float) (rect.x + Math.random() * rect.width),
				(float) (rect.y + Math.random() * rect.height));
	}
	
	// public static Rect boundaryBoxOf(Polygon2D poly) {
	// float minX = poly.vertices.get(0).x;
	// float maxX = minX;
	// float minY = poly.vertices.get(0).y;
	// float maxY = minY;
	// for (Vec2D v : poly.vertices) {
	// if (v.x < minX)
	// minX = v.x;
	// else if (v.x > maxX)
	// maxX = v.x;
	// if (v.y < minY)
	// minY = v.y;
	// else if (v.y > maxY)
	// maxY = v.y;
	// }
	// return new Rect(new MVector(minX, minY), new MVector(maxX, maxY));
	// }
	
	@Override
	public MVector getRandomPointOnEdge() {
		
		float pl = (int) rap.random(rect.getCircumference());
		MVector vector;
		// System.out.println(rect.getEdge(1).a+ " "+rect.getEdge(1).b);
		// System.out.println(rect.getEdge(2).a+ " "+rect.getEdge(2).b);
		if (pl < rect.width)
			vector = new Line(rect.getEdge(0)).getRandomPointOnEdge();
		else {
			pl -= rect.width;
			if (pl < rect.height) {
				vector = new Line(rect.getEdge(1)).getRandomPointOnEdge();
			} else {
				pl -= rect.height;
				if (pl < rect.width) {
					vector = new Line(rect.getEdge(2)).getRandomPointOnEdge();
				} else {
					pl -= rect.width;
					vector = new Line(rect.getEdge(3)).getRandomPointOnEdge();
				}
			}
		}
		// if(vector.x!=0 && vector.y != 0)
		// System.out.println("aha "+d);
		return vector;
		
		// float pl = (int)ap.random(rect.getCircumference());
		// if (pl < rect.width)
		// return new Line(rect.getEdge(0)).getRandomPoint();
		// pl -= rect.width;
		// if (pl < rect.height)
		// return new Line(rect.getEdge(1)).getRandomPoint();
		// pl -= rect.height;
		// if (pl < rect.width)
		// return new Line(rect.getEdge(2)).getRandomPoint();
		// pl -= rect.width;
		// return new Line(rect.getEdge(3)).getRandomPoint();
	}
	
	public Polygon2D toPolygon2D() {
		return rect.toPolygon2D();
	}
	
	@Override
	public float getLength() {
		return rect.getCircumference();
		// return rect.width * 2 + rect.height * 2;
	}
	
	public MVector getPosOfInt(int l) {
		return new MVector(l % rect.width, l / rect.width);
	}
	
	public static MVector getPosOfInt(int width, int height, int l) {
		return new MVector(l % width, l / width);
	}
	
	public PVector map(PVector point, Rect intoRect) {
		return new PVector(RAP.map(point.x, rect.x, rect.x + rect.width, intoRect.x(), intoRect.x()
				+ intoRect.width()), RAP.map(point.y, rect.y, rect.y + rect.height, intoRect.y(),
				intoRect.y() + intoRect.height()));
	}
	
	@Override
	public void draw() {
		rap.rectMode(RAP.CORNER);
		rap.rect(rect.x, rect.y, rect.width, rect.height);
	}
	
	public float y() {
		return rect.y;
	}
	
	public float width() {
		return rect.width;
	}
	
	public float height() {
		return rect.height;
	}
	
	public float x() {
		return rect.x;
	}
	
	@Override
	public String toString() {
		return rect.toString();
	}
	
	public boolean isInside(PVector v) {
		return rect.containsPoint(new Vec2D(v.x, v.y));
	}
	
	public MVector center() {
		return new MVector(rect.getCentroid());
		// return new PVector(rect.x + rect.width * .5f, rect.y + rect.height * .5f);
	}
	
	public static Rect randomRectWithin(PVector size) {
		return randomRectWithin(new PVector(), size);
	}
	
	public static Rect randomRectWithin(PVector from, PVector to) {
		Vec2D topLeft = new Vec2D(rap.random(to.x - from.x), rap.random(to.y - from.y));
		float width = rap.random(to.x - topLeft.x);
		float height = rap.random(to.y - topLeft.y);
		return new Rect(topLeft, width, height);
	}
	
	public Vec2D getVertex(int index) {
		switch (index) {
			case 0:
				return rect.getTopLeft();
			case 1:
				return getTopRight();
			case 2:
				return rect.getBottomRight();
			case 3:
				return getBottomLeft();
			default:
				return rect.getTopLeft();
		}
	}
	
	private Vec2D getBottomLeft() {
		return new Vec2D(rect.x, rect.y + rect.height);
	}
	
	private Vec2D getTopRight() {
		return new Vec2D(rect.x + rect.width, rect.y);
	}
	
}
