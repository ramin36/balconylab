package math.structure;

import math.MVector;
import processing.core.PVector;
import rap.RAP;

public class Circle extends Structure {
	
	public toxi.geom.Circle	circle;
	
	public Circle(MVector mid, float r) {
		super();
		this.circle = new toxi.geom.Circle(mid.x, mid.y, r);
		points = new MVector[] { mid };
	}
	
	public Circle(toxi.geom.Circle circle) {
		super();
		this.circle = circle;
	}
	
	public MVector center() {
		return getbasePoint();
	}
	
	@Override
	public void draw() {
		super.draw();
		rap.circle(circle);
	}
	
	public static Circle getCircleBetween(PVector a, PVector b) {
		MVector c = new MVector(a);
		c.lerp(b, 0.5f);
		return new Circle(c, c.dist(b));
	}
	
	public float radius() {
		return circle.getRadius();
	}
	
	@Override
	public MVector getRandomPointOnEdge() {
		float w = (float) rap.random(RAP.TWO_PI);
		return new MVector(circle.x + RAP.cos(w) * circle.getRadius(), circle.y + RAP.sin(w) * circle.getRadius());
	}
	
	@Override
	public float getLength() {
		return circle.getCircumference();
	}
	
	public MVector[] getEqualPoints(int number, float offset) {
		return getEqualPoints(radius(), new MVector(circle.x, circle.y), number, offset);
	}
	
	public static MVector[] getEqualPoints(float r, int number, float offset) {
		return getEqualPoints(r, new MVector(), number, offset);
	}
	
	public static MVector[] getEqualPoints(float r, PVector shift, int number, float offset) {
		MVector[] ps = new MVector[number];
		float div = RAP.TWO_PI / number;
		for (int i = 0; i < number; i++)
			ps[i] = new MVector(shift.x + RAP.cos(div * i + offset) * r, shift.y + RAP.sin(div * i + offset) * r);
		return ps;
	}
}
