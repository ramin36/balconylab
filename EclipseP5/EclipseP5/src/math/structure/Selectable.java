package math.structure;

import processing.core.PVector;

public interface Selectable {
	
	public boolean selected(PVector click);
	
}
