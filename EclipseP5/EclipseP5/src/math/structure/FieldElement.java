package math.structure;

import java.awt.Point;

public abstract class FieldElement {
	
	protected Point	p;
	
	public FieldElement(Point p) {
		super();
		this.p = p;
	}
	
}
