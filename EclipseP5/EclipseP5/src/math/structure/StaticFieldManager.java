package math.structure;

import helpers.General;

import java.util.ArrayList;

public class StaticFieldManager<T> {
	
	T[][]	field;
	int		szX, szY;
	
	public T get(int x, int y) {
		return field[x][y];
	}
	
	public StaticFieldManager(T[][] field) {
		szX = field.length;
		szY = field[0].length;
	}
	
	public void set(int x, int y, T value) {
		field[x][y] = value;
	}
	
	public ArrayList<T> getNeuMannNB(int x, int y) {
		ArrayList<T> nbs = new ArrayList<T>();
		System.out.println(x + "," + y);
		for (int i = x - 1; i <= x + 1; i++)
			for (int j = y - 1; j <= y + 1; j++)
				if (General.inBounds(new int[] { i, j }, new int[] { 0, 0 }, new int[] { szX, szY }) && Math.abs(i - x) + Math.abs(j - y) < 2 && !(i == x && j == y))
					nbs.add(field[i][j]);
		return nbs;
	}
	
	public ArrayList<T> getMooreNB(int x, int y) {
		ArrayList<T> nbs = new ArrayList<T>();
		for (int i = x - 1; i < x + 1; i++)
			for (int j = y - 1; j < y + 1; j++)
				if (General.inBounds(new int[] { i, j }, new int[] { 0, 0 }, new int[] { szX, szY }) && !(i == x && j == y))
					nbs.add(field[i][j]);
		return nbs;
	}
}
