package math.structure;

import helpers.General;

import java.awt.Point;
import java.util.ArrayList;

public class FieldManager<T extends FieldElement> {
	
	ArrayList<ArrayList<T>>	field;
	int						szX, szY;
	ArrayList<T>			all;
	
	public T get(int x, int y) {
		return field.get(x).get(y);
	}
	
	public FieldManager(int x, int y) {
		szX = x;
		szY = y;
		field = new ArrayList<ArrayList<T>>();
		all = new ArrayList<T>();
		for (int i = 0; i < x; i++)
			for (int j = 0; j < y; j++)
				field.add(new ArrayList<T>());
	}
	
	public ArrayList<T> getNeuMannNB(int x, int y) {
		ArrayList<T> nbs = new ArrayList<T>();
		System.out.println(x + "," + y);
		for (int i = x - 1; i <= x + 1; i++)
			for (int j = y - 1; j <= y + 1; j++)
				if (General
						.inBounds(new int[] { i, j }, new int[] { 0, 0 }, new int[] { szX, szY })
						&& Math.abs(i - x) + Math.abs(j - y) < 2 && !(i == x && j == y))
					nbs.add(field.get(i).get(j));
		return nbs;
	}
	
	public ArrayList<T> getMooreNB(int x, int y) {
		ArrayList<T> nbs = new ArrayList<T>();
		for (int i = x - 1; i < x + 1; i++)
			for (int j = y - 1; j < y + 1; j++)
				if (General
						.inBounds(new int[] { i, j }, new int[] { 0, 0 }, new int[] { szX, szY })
						&& !(i == x && j == y))
					nbs.add(field.get(i).get(j));
		return nbs;
	}
	
	public ArrayList<T> getNeuMannNBMod(int x, int y) {
		ArrayList<T> nbs = new ArrayList<T>();
		System.out.print(x + "," + y);
		for (int i = x - 1; i <= x + 1; i++)
			for (int j = y - 1; j <= y + 1; j++) {
				int ni = modX(i);
				int nj = modY(j);
				if (Math.abs(i - x) + Math.abs(j - y) < 2 && !(i == x && j == y))
					nbs.add(field.get(ni).get(nj));
			}
		System.out.println("  :" + nbs.toString());
		return nbs;
	}
	
	public ArrayList<T> getMooreNBMod(int x, int y) {
		ArrayList<T> nbs = new ArrayList<T>();
		for (int i = x - 1; i < x + 1; i++)
			for (int j = y - 1; j < y + 1; j++) {
				int ni = modX(i);
				int nj = modY(j);
				if (Math.abs(i) + Math.abs(j) < 2 && !(i == x && j == y))
					nbs.add(field.get(ni).get(nj));
			}
		return nbs;
	}
	
	public boolean checkNeumannNB(int xa, int ya, int x, int y) {
		if (Math.abs(x - xa) + Math.abs(y - ya) == 1)
			return true;
		return false;
	}
	
	public int modX(float x) {
		return ((int) x + szX) % szX;
	}
	
	public int modY(float y) {
		return ((int) y + szY) % szY;
	}
	
	public int[] mod(int x, int y) {
		return new int[] { (x + szX) % szX, (y + szY) % szY };
	}
	
	/**
	 * add a coloumn (needs new arrayList)
	 */
	public void addX() {
		field.add(new ArrayList<T>());
		szX++;
	}
	
	/**
	 * inserts an element
	 * 
	 * @param x
	 * @param y
	 * @param e
	 */
	public void add(int x, int y, T e) {
		while (x > szX)
			addX();
		while (y > szY)
			szY++;
		while (y > field.get(x).size())
			field.get(x).add(null);
		field.get(x).add(e);
		all.add(e);
		e.p = new Point(x, y);
	}
	
	public ArrayList<T> all() {
		return all;
	}
	
}
