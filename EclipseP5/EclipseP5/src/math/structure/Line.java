package math.structure;

import java.util.ArrayList;
import java.util.List;

import math.MVector;
import toxi.geom.Line2D;
import toxi.geom.ReadonlyVec2D;
import toxi.geom.Vec2D;

public class Line extends Structure {
	
	public Line2D	line;
	
	public Line(Line2D l) {
		line = l;
	}
	
	public Line(MVector p1, MVector p2) {
		line = new Line2D(p1.toVec2D(), p2.toVec2D());
	}
	
	public Line(ReadonlyVec2D arg0, ReadonlyVec2D arg1) {
		line = new Line2D(arg0, arg1);
	}
	
	@Override
	public MVector getRandomPointOnEdge() {
		float scale = rap.random(1);
		MVector ab = new MVector(line.b.sub(line.a));
		ab.mult(scale);
		MVector p = new MVector(line.a.add(ab.toVec2D()));
		return p;
	}
	
	@Override
	public float getLength() {
		return line.getLength();
	}
	
	public MVector interSection(Line l2) {
		return new MVector(line.intersectLine(l2.line).getPos());
	}
	
	@Override
	public void draw() {
		super.draw();
		rap.line(line.a.x, line.a.y, line.b.x, line.b.y);
	}
	
	@Override
	public void draw(Look look) {
		super.draw(look);
		rap.line(line.a.x, line.a.y, line.b.x, line.b.y);
	}
	
	@Override
	public String toString() {
		return line.a + " | " + line.b;
	}
	
	public static List<Vec2D> nEqualPoints(Line2D line, int n) {
		final float div = line.getLength() / (n - 1);
		final Vec2D add = line.b.sub(line.a).normalizeTo(div);
		final ArrayList<Vec2D> points = new ArrayList<Vec2D>();
		if (n == 1) {
			points.add(new Vec2D(line.a.x + line.b.x / 2, line.a.y + line.b.y / 2));
			return points;
		}
		Vec2D lastAdded = line.a.copy();
		for (int i = 0; i < n - 1; i++) {
			points.add(lastAdded);
			lastAdded = lastAdded.add(add);
		}
		points.add(line.b);
		return points;
	}
	
	public Line invert() {
		return new Line(line.b, line.a);
	}
	
	public Line invertSelf() {
		line = new Line2D(line.b, line.a);
		return this;
	}
	
	public MVector lerp(float factor) {
		return new MVector(line.a.x + (line.b.x - line.a.x) * factor, line.a.y
				+ (line.b.y - line.a.y) * factor);
	}
}
