package math.structure;

import math.MVector;
import rap.RAP;
import rap.RAPUser;

public abstract class Structure implements RAPUser {
	
	public Look			look;
	MVector[]			points;
	public static RAP	rap	= RAP.getRAP(Structure.class);
	public boolean		designError;
	
	public Structure() {
		
	}
	
	public Structure(MVector[] points) {
		this.points = points;
	}
	
	public Structure(MVector[] points, int pnb) {
		this.points = new MVector[pnb];
		for (int i = 0; i < points.length; i++)
			this.points[i] = points[i];
	}
	
	public void draw() {
		if (!Look.quickDraw) {
			if (look == null)
				look = new Look();
			draw(look);
		}
	}
	
	public MVector getbasePoint() {
		return points[0];
	}
	
	public abstract MVector getRandomPointOnEdge();
	
	public abstract float getLength();
	
	public void draw(Look look) {
		if (look.fill)
			rap.fill(look.fillColor);
		if (look.stroke) {
			rap.strokeWeight(look.strokeWeight);
			rap.stroke(look.strokeColor);
		}
	}
}
