package math;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import lombok.Getter;
import lombok.Setter;
import rap.RAP;
import rap.RAPUser;
import toxi.geom.Polygon2D;
import toxi.geom.Triangle2D;
import toxi.geom.Vec2D;
import voronoi.CellFactory;
import drawing.Polygon2DRenderer;

public class Voronoi<T extends Voronoi.Cell> extends toxi.geom.mesh2d.Voronoi implements RAPUser {
	
	public static RAP			rap		= RAP.getRAP(Voronoi.class);
	
	protected CellFactory<T>	factory;
	
	public ArrayList<T>			cells;
	
	Logger						logger	= Logger.getLogger(Voronoi.class.getName());
	
	public Voronoi(CellFactory<T> factory) {
		super();
		cells = new ArrayList<T>();
		this.factory = factory;
		logger.setLevel(Level.WARNING);
	}
	
	public Voronoi() {
		factory = (CellFactory<T>) new CellFactory<Voronoi.Cell>() {
			@Override
			public Cell create(Vec2D site, Polygon2D region) {
				return new Cell(site, region);
			}
			
		};
	}
	
	@Override
	public void addPoint(Vec2D p) {
		super.addPoint(p);
		logger.info("added: " + p);
	}
	
	public void addPoint(MVector p) {
		addPoint(p.toVec2D());
		logger.info("added: " + p);
	}
	
	@Override
	public void addPoints(List<Vec2D> ps) {
		super.addPoints(ps);
		for (Vec2D p : ps)
			logger.info("added: " + p);
	}
	
	// private void clearPoints() {
	// super.
	// }
	
	public void createCells() {
		cells.clear();
		List<Polygon2D> regions = getRegions();
		List<Vec2D> sites = new LinkedList<Vec2D>();
		sites.addAll(getSites());
		for (Polygon2D region : regions)
			for (int i = 0; i < sites.size(); i++)
				if (region.containsPoint(sites.get(i))) {
					cells.add(factory.create(sites.get(i), region));
					sites.remove(i);
					break;
				}
	}
	
	public int size() {
		return cells.size();
	}
	
	public void calcConnectivity() {
		HashMap<Cell, TreeSet<Cell>> neighbours = new HashMap<Voronoi.Cell, TreeSet<Cell>>(
				cells.size() + 2, 1);
		MVector[] tps = new MVector[3];
		for (Triangle2D tr : getTriangles()) {
			tps[0] = new MVector(tr.a);
			tps[1] = new MVector(tr.b);
			tps[2] = new MVector(tr.c);
			Cell[] cls = new Cell[3];
			// get the cells to the triangs
			for (int t = 0; t < 3; t++) {
				for (Cell c : cells)
					if (c.site.distanceTo(tps[t].toVec2D()) < 2) {
						cls[t] = c;
						break;
					}
			}
			// set the neighbourhoods
			for (Cell c : cls) {
				ArrayList<Cell> otherTwo = new ArrayList<Cell>();
				for (Cell c2 : cls)
					if (c != c2 && c2 != null)
						otherTwo.add(c2);
				if (neighbours.containsKey(c))
					neighbours.get(c).addAll(otherTwo);
				else {
					TreeSet<Cell> nbs = new TreeSet<Cell>();
					nbs.addAll(otherTwo);
					neighbours.put(c, nbs);
				}
			}
		}
		for (Cell c : cells)
			c.neighbours = neighbours.get(c).toArray(new Cell[] {});
	}
	
	public void render() {
		for (Cell c : cells)
			c.render();
	}
	
	public static class Cell implements Comparable<Cell>, RAPUser {
		
		public static RAP				rap			= RAP.getRAP(Cell.class);
		
		public static Voronoi			vornoi;
		
		public Vec2D					site;
		@Getter
		@Setter
		public Polygon2D				region;
		@Getter
		@Setter
		protected Cell[]				neighbours;
		
		public static Polygon2DRenderer	renderer	= Polygon2DRenderer.defaultRenderer;
		
		public Cell(Vec2D site, Polygon2D region) {
			this.site = site;
			this.region = region;
		}
		
		public void set(Vec2D site, Polygon2D region) {
			this.site = site;
			this.region = region;
		}
		
		@Override
		public String toString() {
			return "Cell: " + site;
		}
		
		@Override
		public int compareTo(Cell o) {
			return new MVector(site).compareTo(new MVector(o.site));
		}
		
		public void render() {
			renderer.render(region);
		}
		
	}
	
	// TODO doesnt work. toxic.voronoi must be decorator of this class
	public void removeSite(int i) {
		List<Vec2D> sites = getSites();
		sites.remove(i);
		this.sites = sites;
	}
	
}
