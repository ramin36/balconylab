package math;

import java.util.List;

import processing.core.PVector;
import toxi.geom.Polygon2D;
import toxi.geom.Vec2D;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.geom.TopologyException;

public class JTS {
	
	public static GeometryFactory	gf	= new GeometryFactory();
	
	public static Polygon2D difference(Polygon2D poly1, Polygon2D poly2) {
		try {
			Geometry geo = polygon(poly1).difference(polygon(poly2));
			return onePolyFirstOrEmpty(geo);
		} catch (TopologyException tExec) {
			return poly1;
		}
	}
	
	public static Polygon2D intersection(Polygon2D poly1, Polygon2D poly2) {
		try {
			Geometry geo = polygon(poly1).intersection(polygon(poly2));
			return onePolyFirstOrEmpty(geo);
		} catch (TopologyException tExec) {
			return poly1;
		}
	}
	
	public static Polygon2D union(Polygon2D poly1, Polygon2D poly2) {
		Geometry geo = polygon(poly1).union(polygon(poly2));
		return onePolyFirstOrEmpty(geo);
	}
	
	private static Polygon2D onePolyFirstOrEmpty(Geometry geo) {
		if (geo.getNumGeometries() == 1)
			return polygon2D((Polygon) geo);
		else if (geo.getNumGeometries() > 1)
			return polygon2D((Polygon) geo.getGeometryN(0));
		else
			return new Polygon2D();
	}
	
	public static boolean intersects(Polygon2D poly1, Polygon2D poly2) {
		return polygon(poly1).intersects(polygon(poly2));
	}
	
	public static boolean contains(Polygon2D poly1, Polygon2D poly2) {
		return polygon(poly1).contains(polygon(poly2));
	}
	
	public static Coordinate coordinate(PVector p) {
		return new Coordinate(p.x, p.y);
	}
	
	public static Coordinate coordinate(Vec2D p) {
		return new Coordinate(p.x, p.y);
	}
	
	public static Vec2D vec2D(Coordinate coord) {
		return new Vec2D((float) coord.x, (float) coord.y);
	}
	
	public static Polygon polygon(final Polygon2D poly2D) {
		List<Vec2D> ps = poly2D.vertices;
		Coordinate[] cs = new Coordinate[ps.size() + 1];
		int i = 0;
		for (Vec2D v : ps) {
			cs[i++] = coordinate(v);
		}
		cs[i] = coordinate(ps.get(0));
		Polygon poly = gf.createPolygon(cs);
		return poly;
	}
	
	public static Polygon2D polygon2D(final Polygon poly) {
		Coordinate[] cs = poly.getCoordinates();
		Vec2D[] ps = new Vec2D[cs.length];
		int i = 0;
		for (Coordinate c : cs) {
			ps[i++] = vec2D(c);
		}
		return new Polygon2D(ps);
	}
}
