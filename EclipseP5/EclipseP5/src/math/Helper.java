package math;

import java.util.Random;

public class Helper {
	
	 static private Random	internalRandom;

	public static final float random(final float high) {
		    // for some reason (rounding error?) Math.random() * 3
		    // can sometimes return '3' (once in ~30 million tries)
		    // so a check was added to avoid the inclusion of 'howbig'

		    // avoid an infinite loop
		    if (high == 0) return 0;

		    // internal random number object
		    if (internalRandom == null) internalRandom = new Random();

		    float value = 0;
		    do {
		      //value = (float)Math.random() * howbig;
		      value = internalRandom.nextFloat() * high;
		    } while (value == high);
		    return value;
		  }
	
	 public static final float random(final float low, final float high) {
		    if (low >= high) return low;
		    float diff = high - low;
		    return random(diff) + low;
		  }


	 
}
