package math.function.test;

import java.util.ArrayList;

import math.function.Guava;
import math.structure.Rect;
import rap.RAP;

public class GuavaTest extends RAP {
	
	@Override
	public void setup() {
		super.setup();
		ArrayList<Rect> rects = new ArrayList<Rect>();
		for (int i = 0; i < 20; i++) {
			rects.add(Rect.randomRectWithin(this.getSizeMV()));
			System.out.println(rects.get(i));
		}
		float maxWidth = Guava.max(rects, Guava.getFloatMethod(Rect.class, "width"));
		float maxheight = Guava.max(rects, Guava.getFloatMethod(Rect.class, "height"));
		System.out.println("max: " + maxWidth + "," + maxheight);
	}
	
	@Override
	public void draw() {
		
	}
	
}
