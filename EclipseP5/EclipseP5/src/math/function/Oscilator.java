package math.function;

public class Oscilator extends IterativeFunction {
	
	int				length;
	int				amp;
	int				type, mod;
	
	private float	div;
	
	static public final int	SINUS	= 0, COSINUS = 1, SQUARE = 2, SAWTOOTH = 3;
	static public final int	ABS		= 1, INV = 2, ABS_INV = 3;
	
	public Oscilator(int length, int amp, int type) {
		this(length, amp, type, 0);
	}
	
	public Oscilator(int length, int amp, int type, int mod) {
		super();
		this.length = length;
		this.amp = amp;
		this.type = type;
		if (type == SINUS || type == COSINUS)
			div = (float) (Math.PI * 2 / length);
		else if (type == SAWTOOTH)
			div = (float) amp / length * 2;
		this.mod = mod;
	}
	
	@Override
	public void init() {
		val = 0;
	}
	
	@Override
	public float next() {
		float ret;
		switch (type) {
			case COSINUS:
				ret = (float) (Math.cos((val++) * div) * amp);
				break;
			case SQUARE:
				ret = (val++) < length / 2 ? -amp : amp;
				val %= length;
				break;
			case SAWTOOTH:
				ret = (val++) < length / 2 ? div : -div;
			case SINUS:
			default:
				ret = (float) (Math.sin((val++) * div) * amp);
		}
		if (mod == ABS)
			ret = Math.abs(ret);
		if (mod == INV)
			ret = -ret;
		return ret;
	}
	
}
