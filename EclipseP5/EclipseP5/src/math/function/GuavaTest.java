package math.function;

import java.util.ArrayList;

import math.MVector;
import processing.core.PVector;
import rap.RAP;

import com.google.common.collect.Lists;

public class GuavaTest extends RAP {
	
	@Override
	public void setup() {
		super.setup();
		ArrayList<MVector> list = new ArrayList<MVector>();
		for (int i = 0; i < 3; i++)
			list.add(this.randomPoint());
		for (Float d : Lists.transform(list, Guava.getDistanceTo(new PVector())))
			System.out.println(d);
		System.out.println(Guava.average(list, Guava.getDistanceTo(new PVector())));
	}
	
	@Override
	public void draw() {
		
	}
	
}
