package math.function;

public abstract class IterativeFunction {
	
	int val=0;
	
	
	public abstract void init();
	
	public abstract float next();
	
}
