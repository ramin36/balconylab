package math.function;

import rap.RAP;

public class OscTest extends RAP {
	
	Oscilator	osc;
	
	@Override
	public void setup() {
		super.setup();
		size(800, 600);
		background(255);
		fill(0);
		// osc = new Oscilator(width / 2, 200, Oscilator.SINUS, Oscilator.ABS);
		osc = new Oscilator(width / 2, 200, Oscilator.SINUS);
		line(0, center().y, width, center().y);
		line(center().x, 0, center().x, height);
	}
	
	@Override
	public void draw() {
		ellipse(frameCount, center().y + osc.next(), 3, 3);
	}
	
}
