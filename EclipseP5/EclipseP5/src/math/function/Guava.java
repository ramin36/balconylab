package math.function;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import processing.core.PVector;

import com.google.common.base.Function;
import com.google.common.collect.Lists;

public class Guava {
	
	private static Function<PVector, Float>	distance	= new Function<PVector, Float>() {
															
															@Override
															public Float apply(PVector input) {
																return input
																		.dist((PVector) refObject);
															}
															
														};
	
	private static Function<Object, Float>	floatValue	= new Function<Object, Float>() {
															
															@Override
															public Float apply(Object input) {
																try {
																	return (Float) refField
																			.get(input);
																} catch (IllegalArgumentException e) {
																	e.printStackTrace();
																} catch (IllegalAccessException e) {
																	e.printStackTrace();
																}
																return null;
															}
															
														};
	
	private static Function<Object, Float>	floatMethod	= new Function<Object, Float>() {
															
															@Override
															public Float apply(Object input) {
																try {
																	return (Float) refMethod
																			.invoke(input,
																					new Object[0]);
																} catch (IllegalAccessException e) {
																	e.printStackTrace();
																} catch (IllegalArgumentException e) {
																	e.printStackTrace();
																} catch (InvocationTargetException e) {
																	e.printStackTrace();
																}
																return null;
															}
															
														};
	
	private static Object					refObject;
	private static Field					refField;
	private static Method					refMethod;
	
	/**
	 * returns the Function that takes PVectors and returns the distance to Vector p
	 * 
	 * @param p
	 *            the vector to calculate the distance to
	 * @return the Function calculating the distance
	 */
	static public Function<PVector, Float> getDistanceTo(PVector p) {
		refObject = p;
		return distance;
	}
	
	/**
	 * Function that gets any Float field from objects of a specific class
	 * 
	 * TODO: can this be generalized? without passing the class? actually.
	 * -saving the fieldName-taking the clazz of the object in the function- and then the field with the respective name
	 * 
	 * @param clazz
	 *            name of the class
	 * @param valueName
	 * @return
	 * @throws SecurityException
	 * @throws NoSuchMethodException
	 */
	public static Function<Object, Float> getFloat(Class<?> clazz, String valueName) {
		try {
			refField = clazz.getDeclaredField(valueName);
			return floatValue;
		} catch (NoSuchFieldException e) {
			try {
				refMethod = clazz.getMethod("get" + Character.toUpperCase(valueName.charAt(0))
						+ valueName.substring(1), new Class<?>[0]);
				return floatMethod;
			} catch (NoSuchMethodException e1) {
				e1.printStackTrace();
			} catch (SecurityException e1) {
				e1.printStackTrace();
			}
		} catch (SecurityException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static Function<Object, Float> getFloatMethod(Class<?> clazz, String methodName) {
		try {
			refMethod = clazz.getMethod(methodName, new Class<?>[0]);
			return floatMethod;
		} catch (NoSuchMethodException e1) {
			e1.printStackTrace();
		} catch (SecurityException e1) {
			e1.printStackTrace();
		}
		return null;
	}
	
	public static <T> float average(List<? extends T> list, Function<T, Float> function) {
		float avg = 0;
		if (list.size() == 0)
			return avg;
		for (Float value : Lists.transform(list, function))
			avg += value;
		return avg / list.size();
	}
	
	public static <T> float min(List<? extends T> list, Function<T, Float> function) {
		float min = Float.MAX_VALUE;
		if (list.size() == 0)
			return min;
		for (Float value : Lists.transform(list, function))
			if (value < min)
				min = value;
		return min;
	}
	
	public static <T> float max(List<? extends T> list, Function<T, Float> function) {
		float max = Float.MIN_VALUE;
		if (list.size() == 0)
			return max;
		for (Float value : Lists.transform(list, function))
			if (value > max)
				max = value;
		return max;
	}
	
}
