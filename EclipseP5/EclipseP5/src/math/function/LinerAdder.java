package math.function;

import java.util.ArrayList;

public class LinerAdder extends IterativeFunction {
	
	ArrayList<IterativeFunction> funcs;
	
	public LinerAdder(IterativeFunction f1) {
		funcs = new ArrayList<IterativeFunction>(3);
		funcs.add(f1);
	}
	
	public void add(IterativeFunction fu) {
		funcs.add(fu);
		init();
	}
	
	@Override
	public void init() {
		for(IterativeFunction f : funcs)
			f.init();
	}
	
	@Override
	public float next() {
		float sum=0;
		for(IterativeFunction f : funcs)
			sum+=f.next();
		return 0;
	}
	
}
