package math.function;

import math.MVector;
import rap.RAP;
import rap.RAPUser;

public class Noise2D implements RAPUser {
	
	RAP		rap	= RAP.getRAP(Noise2D.class);
	
	MVector	scale;
	
	public Noise2D() {
		this(new MVector());
	}
	
	public Noise2D(MVector scale) {
		this.scale = scale;
	}
	
	public float noise(MVector value) {
		return rap.noise(value.x / scale.x, value.y / scale.y);
	}
}
