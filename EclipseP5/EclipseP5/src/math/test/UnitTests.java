package math.test;

import java.util.ArrayList;

import math.MVector;
import math.structure.Polygon;
import rap.RAP;
import toxi.geom.Polygon2D;
import toxi.geom.Vec2D;

public class UnitTests extends RAP {
	
	int	n	= 10;
	
	@Override
	public void setup() {
		super.setup();
		size(500, 500);
		stroke(255);
		noFill();
		
		doit();
	}
	
	ArrayList<Polygon2D>	polys;
	
	@Override
	public void draw() {
		background(0);
		int toPoly = (int) map(mouseX, 0, width, 0, n);
		for (int i = 0; i < toPoly; i++) {
			stroke((i + sq(i)) % n, n - 1, n - 1);
			pushMatrix();
			translate(new MVector(polys.get(0).getCentroid()));
			rotate(TWO_PI / 360 * i);
			MVector c = new MVector(polys.get(0).getCentroid());
			c.mult(-1);
			translate(c);
			poly(polys.get(i));
			popMatrix();
		}
	}
	
	@Override
	public void mousePressed() {
		n = (int) map(mouseX, 0, width, 0, 50);
		doit();
	}
	
	void doit() {
		ArrayList<Vec2D> vecs = new ArrayList<Vec2D>();
		for (int i = 0; i < 4; i++)
			vecs.add(this.randomPoint().toVec2D());
		polys = new ArrayList<Polygon2D>();
		polys.add(new Polygon2D(vecs));
		do {
			polys.add(Polygon.resize(polys.get(polys.size() - 1), .98f));
		} while (polys.get(polys.size() - 1).getArea() > 3);
		n = polys.size();
		colorMode(HSB, polys.size());
	}
}
