package math;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Collections3 {
	
	/**
	 * picks n elements from the list
	 * 
	 * @param list
	 *            the list
	 * @param number
	 *            number of elements to pick
	 * @param putBack
	 *            true, if element should be pickable multiple times
	 * @return list of picked elements
	 */
	public static <T> List<T> pickN(List<T> list, int number, boolean putBack) {
		Random r = new Random();
		
		List<T> listDuplicate = new ArrayList<T>();
		listDuplicate.addAll(list);
		
		List<T> pickedList = new ArrayList<T>();
		
		for (int i = 0; i < number; i++)
			if (putBack)
				pickedList.add(listDuplicate.get(r.nextInt(listDuplicate.size())));
			else
				pickedList.add(listDuplicate.remove(r.nextInt(listDuplicate.size())));
		return pickedList;
	}
}
