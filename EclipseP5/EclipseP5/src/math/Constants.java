package math;

public class Constants {
	
	public static float	phi		= (float) ((1 + Math.sqrt(5)) * 0.5f);
	public static float	phi_inv	= (float) (1 / ((1 + Math.sqrt(5)) * 0.5f));
}
