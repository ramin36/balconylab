package math;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import math.structure.Selectable;
import processing.core.PApplet;
import processing.core.PVector;
import rap.RAP;
import rap.RAPUser;
import toxi.geom.Vec2D;

public class MVector extends processing.core.PVector implements Comparable<MVector>, Serializable,
		RAPUser, Selectable {
	
	/** */
	private static final long	serialVersionUID	= 1L;
	int							valueType;					// 0: int, 1: float,2 int 3d,3 float 3d
	public static RAP			rap;
	
	static final int			N					= 0, E = 1, S = 2, W = 3;
	
	static int					selectionRange		= 5;
	
	public MVector(PVector p) {
		if (p == null) {
			x = y = z = 0;
			return;
		}
		x = p.x;
		y = p.y;
		z = p.z;
	}
	
	public MVector() {
		super();
	}
	
	public MVector(int i, int j) {
		super(i, j);
		valueType = 0;
	}
	
	public MVector(float i, float j) {
		super(i, j);
		valueType = 1;
	}
	
	public MVector(float x, float y, float z) {
		super(x, y, z);
		valueType = 3;
	}
	
	public MVector(Vec2D pos) {
		x = pos.x;
		y = pos.y;
	}
	
	@Override
	public MVector clone() {
		return new MVector(this);
	}
	
	@Override
	public int compareTo(MVector v) {
		float diff = z - v.z;
		if (diff < 0)
			return -1;
		else if (diff > 0)
			return 1;
		diff = y - v.y;
		if (diff < 0)
			return -1;
		else if (diff > 0)
			return 1;
		diff = x - v.x;
		if (diff < 0)
			return -1;
		else if (diff > 0)
			return 1;
		return 0;
	}
	
	@Override
	public String toString() {
		if (valueType == 0)
			return (int) x + "," + (int) y;
		else if (valueType == 1)
			return x + "," + y;
		if (valueType == 2)
			return (int) x + "," + (int) y + "," + (int) z;
		else
			// if (valueType == 3)
			return x + "," + y + "," + z;
		
	}
	
	public Vec2D toVec2D() {
		return new Vec2D(x, y);
	}
	
	public float angleTo(PVector p) {
		float w = (float) Math.atan2(p.y - y, p.x - x);
		return w < 0 ? RAP.TWO_PI + w : w;
	}
	
	public static MVector rndPoint(PApplet ap) {
		return new MVector((int) ap.random(ap.width), (int) ap.random(ap.height));
	}
	
	public void set(float x, float y) {
		set(x, y, 0);
	}
	
	public void drawRegion() {
		rap.pushStyle();
		rap.stroke(255);
		rap.fill(255);
		rap.ellipse(x, y, 4, 4);
		rap.popStyle();
	}
	
	public void reset() {
		set(0, 0, 0);
	}
	
	public boolean isZero() {
		return x == 0 && y == 0 && z == 0;
	}
	
	/**
	 * returns a list of vectors which are in the neumann neighourhood (4 nbs)
	 * 
	 * @return
	 */
	public ArrayList<MVector> getNeuMannNB() {
		ArrayList<MVector> nbs = new ArrayList<MVector>();
		for (float i = x - 1; i <= x + 1; i++)
			for (float j = y - 1; j <= y + 1; j++)
				if (Math.abs(i - x) + Math.abs(j - y) < 2 && !(i == x && j == y))
					nbs.add(new MVector(i, j));
		return nbs;
	}
	
	/**
	 * returns a list of vectors which are in the moore neighourhood (8 nbs)
	 * 
	 * @return
	 */
	public ArrayList<MVector> getMooreNB() {
		ArrayList<MVector> nbs = new ArrayList<MVector>();
		for (float i = x - 1; i <= x + 1; i++)
			for (float j = y - 1; j <= y + 1; j++)
				if (!(i == x && j == y))
					nbs.add(new MVector(i, j));
		return nbs;
	}
	
	public int xi() {
		return (int) x;
	}
	
	public int yi() {
		return (int) y;
	}
	
	public int zi() {
		return (int) z;
	}
	
	public void modulo(MVector modV) {
		x = (x + modV.x) % modV.x;
		y = (y + modV.y) % modV.y;
	}
	
	/**
	 * shift on one axis, heading a direction
	 * 
	 * @param dir
	 */
	public void shift(int dir, int steps) {
		switch (dir) {
			case N:
				y -= steps;
				break;
			case E:
				x += steps;
				break;
			case S:
				y += steps;
				break;
			case W:
				x -= steps;
				break;
		}
		
	}
	
	public void toInt() {
		x = xi();
		y = yi();
		z = zi();
	}
	
	public boolean equals(PVector v) {
		return x == v.x && y == v.y && z == v.z;
	}
	
	public MVector lerpTowards(MVector destination, float lerp) {
		return new MVector(x + (destination.x - x) * lerp, y + (destination.y - y) * lerp);
	}
	
	@Override
	public boolean selected(PVector click) {
		return click.dist(this) < selectionRange;
	}
	
	public static MVector selectFrom(List<MVector> vecs, PVector click) {
		MVector selected = null;
		for (MVector v : vecs)
			if (v.selected(click))
				if (selected != null)
					selected = selected.dist(click) < v.dist(click) ? selected : v;
				else
					selected = v;
		return selected;
	}
	
	/**
	 * TODO: there should be guava functions...
	 * 
	 * @param vecs
	 * @return
	 */
	public static ArrayList<MVector> Vec2DsToMVs(List<Vec2D> vecs) {
		ArrayList<MVector> list = new ArrayList<MVector>(vecs.size());
		for (Vec2D v : vecs)
			list.add(new MVector(v));
		return list;
	}
	
	public static ArrayList<Vec2D> MVsToVec2Ds(List<MVector> vecs) {
		ArrayList<Vec2D> list = new ArrayList<Vec2D>(vecs.size());
		for (MVector v : vecs)
			list.add(v.toVec2D());
		return list;
	}
	
	/**
	 * not tested yet. something similar must be somewhere else.
	 * 
	 * @param vec
	 * @return
	 */
	public MVector fitInto(MVector vec) {
		float scaleX = x / vec.x;
		float scaleY = y / vec.y;
		
		float area1 = 0, area2 = 0;
		float tryOpposite = x / scaleY;
		if (tryOpposite <= vec.y) {
			area1 = vec.x * tryOpposite;
		}
		tryOpposite = y / scaleX;
		if (tryOpposite <= vec.x) {
			area2 = vec.y * tryOpposite;
		}
		if (area1 > area2)
			return new MVector(vec.x, x / scaleY);
		else
			return new MVector(vec.y, tryOpposite);
	}
}
