package oscCom;

import oscP5.OscMessage;
import oscP5.OscP5;
import oscP5.OscProperties;
import processing.core.PApplet;

public class Client {
	
	PApplet		applet;
	OscP5		osc;
	
	final int	port	= 47036;
	
	String		myName;
	
	public Client(PApplet applet, String myName) {
		osc = new OscP5(applet, port, OscProperties.TCP);
		this.myName = myName;
	}
	
	public void connect() {
		OscMessage msg = new OscMessage("/connect");
		msg.add(myName);
		// osc.sen
	}
	
	public void disconnect() {
		OscMessage msg = new OscMessage("/disconnect");
		msg.add(myName);
	}
}
