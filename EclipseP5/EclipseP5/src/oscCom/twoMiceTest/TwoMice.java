package oscCom.twoMiceTest;

import netP5.NetAddress;
import oscP5.OscMessage;
import oscP5.OscP5;
import processing.core.PApplet;
import processing.core.PImage;
import processing.core.PVector;

public class TwoMice extends PApplet {
	
	PVector		lastPoint;
	
	OscP5		oscP5;
	NetAddress	theOther;
	
	PImage		img;
	boolean		sending		= false;
	
	String		OTHER_IP	= "192.168.2.113";
	int			OTHER_PORT	= 12000;
	int			MY_PORT		= 12001;
	
	String		IMG_PATH	= "C:/Users/Ramin/Pictures/2 fretchen.jpg";
	
	@Override
	public void setup() {
		background(255);
		noStroke();
		size(800, 600);
		frameRate(25);
		
		// img = loadImage(IMG_PATH);
		// image(img, 0, 0, width, height);
		
		oscP5 = new OscP5(this, MY_PORT);
		theOther = new NetAddress(OTHER_IP, OTHER_PORT);
		
		OscMessage msg = new OscMessage("/hello");
		msg.add("http://piratepad.net/pdmGz0lg9b");
		oscP5.send(msg, theOther);
	}
	
	@Override
	public void draw() {
		if (mousePressed) {
			OscMessage msg = new OscMessage("/notatest");
			msg.add(mouseX);
			msg.add(mouseY);
			oscP5.send(msg, theOther);
			stroke(0x27FF72);
			line(mouseX, mouseY, pmouseX, pmouseY);
		}
		println(frameRate);
		
		if (sending)
			for (int i = 0; i < 50; i++)
				senScreenPart();
	}
	
	@Override
	public void mouseMoved() {
	}
	
	@Override
	public void keyPressed() {
		if (key == ' ') {
			sending = !sending;
			println(sending ? "Sending" : "Not sending");
		}
	}
	
	int	pixelsSend	= 0;
	
	void senScreenPart() {
		if (pixelsSend >= width * height - 1)
			return;
		loadPixels();
		OscMessage msg = new OscMessage("/imagePart");
		msg.add(pixelsSend);
		for (int i = 0; i < 300; i++)
			msg.add(pixels[pixelsSend++]);
		oscP5.send(msg, theOther);
		println(pixelsSend + "/" + pixels.length);
	}
	
	void oscEvent(OscMessage msg) {
		if (msg.checkAddrPattern("/hello")) {
			String a = msg.get(0).stringValue();
			println(a);
		}
		if (msg.checkAddrPattern("/notatest")) {
			int x = msg.get(0).intValue();
			int y = msg.get(1).intValue();
			stroke(0xFF0572);
			if (lastPoint != null) {
				line(x, y, lastPoint.x, lastPoint.y);
			}
			lastPoint = new PVector(x, y);
		}
		if (msg.checkAddrPattern("/pixels")) {
			// println(msg);
			loadPixels();
			for (int i = 0; i < 150; i++) {
				int which = msg.get(i * 2).intValue();
				int val = msg.get(i * 2 + 1).intValue();
				pixels[which] = val;
			}
			updatePixels();
		}
		if (msg.checkAddrPattern("/imagePart")) {
			// println(msg);
			loadPixels();
			int loc = msg.get(0).intValue();
			for (int i = 0; i < 300; i++) {
				int val = msg.get(i + 1).intValue();
				pixels[loc + i] = val;
			}
			updatePixels();
		}
	}
}
