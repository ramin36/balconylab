package oscCom;

import oscP5.OscMessage;
import oscP5.OscP5;
import processing.core.PApplet;

public class Server {
	
	PApplet		applet;
	OscP5		osc;
	
	final int	port	= 47036;
	
	public Server(PApplet applet) {
		osc = new OscP5(applet, port);
	}
	
	public void connect(String myName) {
		OscMessage msg = new OscMessage("/connect");
		msg.add(myName);
	}
	
	void oscEvent(OscMessage theOscMessage) {
		theOscMessage.print();
	}
	
}
