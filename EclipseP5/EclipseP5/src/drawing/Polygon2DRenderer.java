package drawing;

import java.util.ArrayList;

import processing.core.PApplet;
import rap.RAP;
import rap.RAPUser;
import toxi.geom.Polygon2D;
import toxi.geom.Vec2D;

import com.google.common.base.Function;

/**
 * Draws Polygons in different styles. So far there are three styles
 * 
 * @author Ramin
 * 
 */
public class Polygon2DRenderer implements RAPUser {
	
	public static Polygon2DRenderer	defaultRenderer	= new Polygon2DRenderer();
	
	public int						renderStyle		= POLYGON;
	
	public static final int			POLYGON			= 0, BEZIER = 1, LINES_TO_SINGLE = 2,
			VERTEXCOLORS = 3;
	
	public static RAP				rap				= RAP.getRAP(Polygon2DRenderer.class);
	
	public Object					reference;
	
	public static void renderBezier(Polygon2D poly) {
		Vec2D[] points = ((ArrayList<Vec2D>) poly.vertices).toArray(new Vec2D[0]);
		int npoints = points.length;
		
		rap.beginShape();
		rap.vertex((points[npoints - 1].x + points[0].x) / 2,
				(points[npoints - 1].y + points[0].y) / 2);
		for (int i = 0; i < npoints; i++)
			rap.bezierVertex(points[i].x, points[i].y, points[i].x, points[i].y, (points[(i + 1)
					% npoints].x + points[i].x) / 2,
					(points[(i + 1) % npoints].y + points[i].y) / 2);
		rap.endShape(RAP.CLOSE);
	}
	
	public static void renderPolygon(Polygon2D poly) {
		rap.poly(poly);
	}
	
	public void render(Polygon2D poly, Object reference) {
		this.reference = reference;
		render(poly);
	}
	
	public void render(Polygon2D poly) {
		if (poly == null) {
			System.err.println("Null-Polygon");
			return;
		}
		switch (renderStyle) {
			case POLYGON:
				renderPolygon(poly);
				break;
			case BEZIER:
				renderBezier(poly);
				break;
			case LINES_TO_SINGLE:
				drawLinesTo(poly, (Function<Polygon2D, Vec2D>) reference);
			case VERTEXCOLORS:
				renderVertices(poly, (int[]) reference);
			default:
				renderPolygon(poly);
				break;
		}
	}
	
	public static void renderVertices(Polygon2D poly, int[] colors) {
		if (poly.vertices.size() > colors.length) {
			System.err.println("To many vertices. Not enough colors defined(vs:"
					+ poly.vertices.size() + ",clrs: " + colors.length + ")");
			return;
		}
		rap.beginShape(PApplet.TRIANGLE_FAN);
		int i = 0;
		rap.fill(0);
		rap.vertex(poly.getCentroid());
		for (Vec2D v : poly.vertices) {
			rap.fill(colors[i++]);
			rap.vertex(v);
		}
		rap.endShape();
	}
	
	public static void drawLinesTo(Polygon2D poly, Function<Polygon2D, Vec2D> function) {
		Vec2D v = function.apply(poly);
		for (Vec2D p : poly.vertices)
			rap.line(p.x, p.y, v.x, v.y);
	}
}
