package LargeGraphics;

import java.awt.Point;

import processing.core.PVector;
import rap.RAP;

public class LargeGraphicsTest extends RAP {
	
	@Override
	public void setup() {
		super.setup();
		
		LargeGraphic lg = new LargeGraphic(new Point(300, 200), new Point(3, 2));
		lg.line(new PVector(140, 120), new PVector(290, 150));
		lg.save("lg.png");
		exit();
	}
	
	@Override
	public void draw() {
		
	}
	
}
