package LargeGraphics;

import helpers.ImageNaming;

import java.awt.Point;
import java.util.ArrayList;

import processing.core.PGraphics;
import processing.core.PVector;
import rap.RAP;
import rap.RAPUser;

public class LargeGraphic extends PGraphics implements RAPUser {
	
	public static RAP	rap	= RAP.getRAP(LargeGraphic.class);
	
	Point				totalSize;
	Point				division;
	Point				graphicsSize;
	PGraphics[][]		graphics;
	
	public LargeGraphic(Point totalSize, Point division) {
		if (totalSize.x % division.x != 0 || totalSize.y % division.y != 0)
			throw new RuntimeException("size and division do not match");
		this.totalSize = totalSize;
		this.division = division;
		graphicsSize = new Point(totalSize.x / division.x, totalSize.y / division.y);
		graphics = new PGraphics[division.x][division.y];
		for (int x = 0; x < graphics.length; x++)
			for (int y = 0; y < graphics[0].length; y++)
				graphics[x][y] = rap.createGraphics(graphicsSize.x, graphicsSize.y);
	}
	
	public PGraphics getGraphicsOfAddress(Point p) {
		return graphics[p.x][p.y];
	}
	
	public Point getAddressOfLoc(PVector loc) {
		int x = (int) Math.min(loc.x / graphicsSize.x, division.x);
		int y = (int) Math.min(loc.y / graphicsSize.y, division.y);
		return new Point(x, y);
	}
	
	@Override
	public boolean save(String name) {
		String[] nameFormat = ImageNaming.splitNameFormat(name);
		for (int x = 0; x < graphics.length; x++)
			for (int y = 0; y < graphics[0].length; y++)
				graphics[x][y].save(nameFormat[0] + "-" + x + "-" + y + nameFormat[1]);
		return true;
	}
	
	public PVector getShiftLocOfAdr(Point adr) {
		return new PVector(graphicsSize.x * adr.x, graphicsSize.y * adr.y);
	}
	
	public void line(PVector p1, PVector p2) {
		ArrayList<Point> graphics = getRelateAddresses(p1, p2);
		for (Point pga : graphics) {
			PVector s1 = PVector.sub(p1, getShiftLocOfAdr(pga));
			PVector s2 = PVector.sub(p2, getShiftLocOfAdr(pga));
			System.out.println(s1 + " " + s2);
			PGraphics pg = getGraphicsOfAddress(pga);
			pg.beginDraw();
			pg.line(s1.x, s1.y, s2.x, s2.y);
			pg.endDraw();
		}
	}
	
	public ArrayList<Point> getRelateAddresses(PVector p1, PVector p2) {
		float minX = Math.min(p1.x, p2.x);
		float maxX = Math.max(p1.x, p2.x);
		float minY = Math.min(p1.y, p2.y);
		float maxY = Math.max(p1.y, p2.y);
		Point minG = new Point(getAddressOfLoc(new PVector(minX, minY)));
		Point maxG = new Point(getAddressOfLoc(new PVector(maxX, maxY)));
		ArrayList<Point> graphics = new ArrayList<Point>();
		for (int x = minG.x; x <= maxG.x; x++)
			for (int y = minG.y; y <= maxG.y; y++) {
				graphics.add(new Point(x, y));
				System.out.println(x + "," + y);
			}
		return graphics;
	}
	
}
