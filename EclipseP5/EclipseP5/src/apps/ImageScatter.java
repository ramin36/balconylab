package apps;

import generalController.library.main.GeneralController;
import image.ImageHelper;

import java.util.ArrayList;
import java.util.List;

import math.JTS;
import math.MVector;
import math.Voronoi;
import math.Voronoi.Cell;
import math.structure.Polygon;
import math.structure.Rect;
import processing.core.PGraphics;
import processing.core.PImage;
import rap.RAP;
import toxi.geom.Polygon2D;
import toxi.geom.Vec2D;

public class ImageScatter extends RAP {
	
	PImage				img;
	int					numFragPoints	= 60;
	ArrayList<MVector>	fragPoints		= new ArrayList<MVector>();
	ArrayList<Fragment>	fragments		= new ArrayList<Fragment>();
	
	final int			PLAINIMAGE		= 0, VORONOI_OVERLY = 1, FRAGMENTS = 2, TEST = 3;
	
	int					mode			= FRAGMENTS;
	
	public float		maxRot			= HALF_PI / 2;
	public int			tintS			= 100;
	public int			repeats			= 6;
	public float		frameCountRot	= .5f;
	public float		layerRot		= .1f;
	public float		layerScale		= .02f;
	public boolean		multiply;
	
	List<Polygon2D>		polys;
	int					selectedInd		= 0;
	
	int[]				modes			= { ADD };
	
	Voronoi<Cell>		voronoi;
	
	GeneralController	gc;
	
	@Override
	public void setup() {
		super.setup();
		img = loadImage("C:/Users/Ramin/Pictures/methedog.jpg");
		gc = new GeneralController(this);
		gc.load("C:/imageRot.xml");
		// AppImgSaver ais = new AppImgSaver(this, 90);
		// img = loadImage("C:/Users/Ramin/Pictures/dao.gif");
		size(img.width + 100, img.height + 100, P2D);
		Rect rect = new Rect(img.width, img.height);
		for (int i = 0; i < numFragPoints; i++)
			fragPoints.add(rect.getRandomPoint());
		voronoi = new Voronoi<Cell>();
		for (MVector fp : fragPoints)
			voronoi.addPoint(fp);
		Polygon2D rectP = rect.toPolygon2D();
		polys = new ArrayList<Polygon2D>();
		for (Polygon2D poly : voronoi.getRegions()) {
			Polygon2D fpoly = JTS.intersection(poly, rectP);
			PGraphics pg = ImageHelper.cutPolygonOutOfImage(img, fpoly);
			polys.add(fpoly);
			fragments.add(new Fragment(fpoly, pg));
		}
		smooth();
		// frameRate(2);
	}
	
	public void addCell(MVector newp) {
		Rect rect = new Rect(img.width, img.height);
		fragPoints.add(newp);
		voronoi.addPoint(newp);
		Polygon2D rectP = rect.toPolygon2D();
		polys = new ArrayList<Polygon2D>();
		fragments.clear();
		for (Polygon2D poly : voronoi.getRegions()) {
			Polygon2D fpoly = JTS.intersection(poly, rectP);
			PGraphics pg = ImageHelper.cutPolygonOutOfImage(img, fpoly);
			polys.add(fpoly);
			fragments.add(new Fragment(fpoly, pg));
		}
	}
	
	@Override
	public void draw() {
		background(0);
		// translate(50, 0);
		if (mode == PLAINIMAGE || mode == VORONOI_OVERLY)
			image(img, 0, 0);
		if (mode == VORONOI_OVERLY) {
			displayVoronoi();
		}
		if (mode == FRAGMENTS) {
			if (multiply)
				blendMode(BLEND);
			else
				blendMode(ADD);
			for (Fragment frag : fragments) {
				pushMatrix();
				translate(frag.location.x, frag.location.y);
				image(frag.graphic, 0, 0);
				popMatrix();
			}
			for (int i = 0; i < repeats; i++) {
				for (Fragment frag : fragments) {
					pushMatrix();
					Vec2D c = frag.fpoly.getCentroid();
					translate(c.x, c.y);
					// rotate(noise(c.x + 0 * .5f + i * .1f) * 2 * maxRot - maxRot);// random(-maxRot, maxRot));
					rotate(noise(c.x + frameCount * frameCountRot + i * layerRot) * 2 * maxRot
							- maxRot);// random(-maxRot, maxRot));
					scale(1 + i * layerScale);
					translate(frag.location.x - c.x, frag.location.y - c.y);
					// println(tintS);
					tint(255, tintS);
					image(frag.graphic, 0, 0);
					popMatrix();
				}
			}
		}
		if (mode == TEST) {
			Fragment frag = fragments.get(selectedInd);
			pushMatrix();
			Vec2D c = frag.fpoly.getCentroid();
			translate(c.x, c.y);
			rotate(random(-HALF_PI / 6, HALF_PI / 6));
			translate(frag.location.x - c.x, frag.location.y - c.y);
			image(frag.graphic, 0, 0);
			popMatrix();
		}
		// println(frameRate);
	}
	
	int pick() {
		return modes[(int) random(modes.length)];
	}
	
	private void displayVoronoi() {
		noFill();
		stroke(255);
		strokeWeight(2);
		for (Polygon2D poly : polys)
			poly(poly);
	}
	
	@Override
	public void keyPressed() {
		// selectedInd = (selectedInd + 1) % numFragPoints;
		// Rect rect = new Rect(img.width, img.height);
		// addCell(rect.getRandomPoint());
		if (key == 'e')
			gc.openControlWindow();
	}
	
	@Override
	public void mousePressed() {
		// addCell(new MVector(mouseX, mouseY));
	}
	
	private class Fragment {
		
		Polygon2D	fpoly;
		MVector		location;
		PGraphics	graphic;
		
		public Fragment(Polygon2D fpoly, PGraphics graphic) {
			this.location = Polygon.getMinPoint(fpoly);
			this.graphic = graphic;
			this.fpoly = fpoly;
			// System.out.println(location);
		}
		
		public void render() {
			
		}
		
	}
	
}
