package apps.testies;

import image.ColorHelper;

import java.util.List;

import math.structure.Line;
import rap.RAP;
import toxi.geom.Vec2D;

/**
 * DAMN doesnt work. whyyyy!?!?
 * 
 * @author Ramin
 * 
 */
public class ColorfullLine extends RAP {
	
	int	n	= 10;
	
	@Override
	public void setup() {
		super.setup();
		size(500, 500, P2D);
		Line line = new Line(frameAsRect().getVertex(0), frameAsRect().getVertex(2));
		List<Vec2D> lineSegs = Line.nEqualPoints(line.line, n);
		background(0);
		colorMode(RGB, n);
		beginShape();
		vertex(lineSegs.get(n - 1));
		for (int i = 0; i < n; i++) {
			stroke(i, n - 1, n - 1);
			System.out.println(ColorHelper.toString(color(i, n - 1, n - 1)));
			vertex(lineSegs.get(i));
		}
		endShape();
	}
	
	@Override
	public void draw() {
		
	}
	
}
