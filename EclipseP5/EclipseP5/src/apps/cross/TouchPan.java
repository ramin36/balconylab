package apps.cross;

import java.util.HashMap;

import math.MVector;
import math.function.Oscilator;
import processing.core.PApplet;
import processing.core.PImage;
import rap.RAP;

public class TouchPan extends RAP {
	
	private static final long	serialVersionUID	= 2366494899084510720L;
	
	ImageSource					s1, s2;
	int							left				= 1;
	int							mode				= 2;
	int							minSlice			= 5, maxSlice = 20;
	int							freqSec				= 8;
	Oscilator					osc;
	PImage						pg;
	int							partW;
	HashMap<Integer, Integer>	modes				= new HashMap<Integer, Integer>();
	
	@Override
	public void setup() {
		super.setup();
		
		s1 = new ImageSource("p3.jpg");
		s2 = new ImageSource("p1.jpg");
		osc = new Oscilator(freqSec * 30, 1, Oscilator.SAWTOOTH);
		frameRate(30);
		MVector imgSize = s1.getSize();
		size((int) imgSize.x * 2, (int) imgSize.y);
		// fullscreen();
		// pg = createGraphics((int) (imgSize.x * .5f), (int) imgSize.y);
		pg = createImage((int) (imgSize.x * .5f), (int) imgSize.y, RGB);
		partW = (int) (s1.getSize().xi() * .5f);
		modes.put(0, BLEND);
		modes.put(1, ADD);
		modes.put(2, SUBTRACT);
		modes.put(3, DARKEST);
		modes.put(4, LIGHTEST);
		modes.put(5, DIFFERENCE);
		modes.put(6, EXCLUSION);
		modes.put(7, MULTIPLY);
		modes.put(8, SCREEN);
		modes.put(9, OVERLAY);
		modes.put(10, HARD_LIGHT);
		modes.put(11, SOFT_LIGHT);
		modes.put(10, DODGE);
		modes.put(11, BURN);
	}
	
	public static void main(String[] args) {
		PApplet.main(new String[] { "--present", "app.TouchPan" });
	}
	
	@Override
	public void draw() {
		// println(frameCount / 30);
		float oscV = osc.next();
		left = oscV > 0 ? 1 : -1;
		rectMode(CENTER);
		fill(0);
		rect(width / 2, 100, 500, 30);
		fill(255);
		rectMode(CORNER);
		rect(width / 2, 90, oscV * 250, 20);
		int sliceW = (int) map(abs(oscV), 0, 1, minSlice, maxSlice);
		Object[] leftG = getSlice(getPart(false, left == 1 ? s1 : s2), sliceW);
		Object[] rightG = getSlice(getPart(true, left == 1 ? s2 : s1), sliceW);
		// int t = (int) ((float) mouseY / height * 255);
		tint(255, 30);
		image((PImage) leftG[0], (Integer) leftG[1], 0);
		image((PImage) rightG[0], width - partW + (Integer) rightG[1], 0);
		int mode = modes.get((int) random(modes.size()));
		// int mode = modes.get((int) ((mouseX / (float) width) * modes.size()));
		// blend((PImage) leftG[0], 0, 0, sliceW, height, (int) leftG[1], 0, sliceW, height, mode);
		// blend((PImage) rightG[0], 0, 0, sliceW, height, width - partW + (int) rightG[1], 0, sliceW, height, mode);
		
		g.removeCache((PImage) leftG[0]);
		g.removeCache((PImage) rightG[0]);
	}
	
	private Object[] getSlice(PImage pg, int size) {
		PImage slice = createImage(size, pg.height, RGB);
		int x = (int) random(pg.width - size + 1);
		slice.copy(pg, x, 0, size, pg.height, 0, 0, size, pg.height);
		return new Object[] { slice, x };
	}
	
	private PImage getPart(boolean left, ImageSource imageSource) {
		MVector sz = imageSource.getSize();
		int pgX = partW;
		int pgY = sz.yi();
		if (left)
			pg.copy(imageSource.getImage(), 0, 0, pgX, pgY, 0, 0, pgX, pgY);
		else
			pg.copy(imageSource.getImage(), sz.xi() - pgX, 0, pgX, pgY, 0, 0, pgX, pgY);
		return pg;
	}
	
	public class ImageSource {
		
		PImage	img;
		
		public ImageSource(String imgFile) {
			// println(sketchPath);
			this.img = loadImage(imgFile);
		}
		
		public ImageSource(PImage img) {
			this.img = img;
		}
		
		public MVector getSize() {
			return new MVector(img.width, img.height);
		}
		
		public PImage getImage() {
			return img;
		}
		
	}
	
}
