package apps.small;

import processing.core.PGraphics;
import processing.core.PImage;
import rap.RAP;

public class Flowers extends RAP {
	
	PImage		f0;
	int			border	= 50;
	PGraphics	f0g;
	
	@Override
	public void setup() {
		size(800, 600);
		background(255);
		f0 = loadImage("C:/Users/Ramin/Documents/Processing/flowers/data/flower1.png");
		imageMode(CENTER);
		float d = 10;
		f0g = circleTransparency(f0, d);
		image(f0g, random(border, width - border), random(border, height - border));
		fill(255, 3);
	}
	
	@Override
	public void draw() {
		
		rect(-2, -2, width, height);
		// float d = max(15 - frameCount / 100, 0.1f);
		float d = 10;
		f0g = circleTransparency(f0, d);
		image(f0g, random(border, width - border), random(border, height - border));
		println(frameCount + " " + d);
	}
	
	int addTransparency(int c, int trans) {
		return color(red(c), green(c), blue(c), trans);
	}
	
	PGraphics	ret;
	
	PGraphics circleTransparency(PImage img, float transDistMult) {
		int w = img.width;
		int h = img.height;
		float mx = w / 2;
		float my = h / 2;
		ret = createGraphics(w, h);
		ret.beginDraw();
		ret.loadPixels();
		for (int x = 0; x < w; x++)
			for (int y = 0; y < h; y++) {
				int cal = addTransparency(img.get(x, y), 255 - (int) (dist(mx, my, x, y) * transDistMult));
				ret.pixels[x + y * w] = cal;
			}
		ret.updatePixels();
		ret.endDraw();
		return ret;
	}
	
}
