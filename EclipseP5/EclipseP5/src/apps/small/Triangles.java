package apps.small;

import java.util.Arrays;

import math.Constants;
import math.MVector;
import math.structure.Line;
import processing.core.PVector;
import rap.RAP;
import toxi.geom.Polygon2D;

public class Triangles extends RAP {
	
	EquilateralTriangle	tri, tri2;
	Polygon2D			hexa1, hexa2;
	
	@Override
	public void setup() {
		super.setup();
		size(500, 500);
		stroke(255);
		strokeWeight(1);
		noFill();
		tri = new EquilateralTriangle(center(), .7f * width / sqrt(3));
		tri2 = new EquilateralTriangle(center(), tri.getr(), PI);
		strokeCap(ROUND);
		hexa1 = createPolygon(tri);
		hexa2 = createPolygon(tri2);
	}
	
	private Polygon2D createPolygon(EquilateralTriangle triangle) {
		Line[] sides = triangle.getSides();
		MVector[] points = new MVector[6];
		int i = 0;
		for (Line l : sides) {
			points[i++] = l.invert().lerp(Constants.phi_inv);
			points[i++] = l.lerp(Constants.phi_inv);
			// points[i++] = l.lerp(Constants.phi);
		}
		return new Polygon2D(MVector.MVsToVec2Ds(Arrays.asList(points)));
	}
	
	@Override
	public void draw() {
		background(0);
		tri.render();
		tri2.render();
		poly(hexa1);
		poly(hexa2);
	}
	
	public class EquilateralTriangle {
		
		PVector	center;
		float	R, sideL;
		MVector	vertex[]	= new MVector[3];
		float	phase;
		
		public EquilateralTriangle(PVector center, float R) {
			this(center, R, 0);
		}
		
		public EquilateralTriangle(PVector center, float R, float phase) {
			super();
			this.center = center;
			this.R = R;
			sideL = R * 3f / sqrt(3);
			this.phase = phase;
			generatePoints();
		}
		
		private void generatePoints() {
			for (int a = 30, i = 0; a <= 270; a += 120, i++)
				vertex[i] = new MVector(center.x + RAP.cos(RAP.radians(a) + phase) * R, center.y
						+ RAP.sin(RAP.radians(a) + phase) * R);
		}
		
		public void render() {
			for (int i = 0; i < 3; i++)
				line(vertex[i], vertex[(i + 1) % 3]);
			// beginShape();
			// for (MVector v : vertex)
			// vertex2D(v);
			// endShape(CLOSE);
		}
		
		public float getArea() {
			return (float) (sqrt(3) * .25 * Math.pow(sideL, 2));
		}
		
		public float getr() {
			return (float) (sqrt(3) / 6 * sideL);
		}
		
		public Line[] getSides() {
			Line[] sides = new Line[3];
			for (int i = 0; i < 3; i++)
				sides[i] = new Line(vertex[i], vertex[(i + 1) % 3]);
			return sides;
		}
	}
	
}
