package apps.small;

import helpers.AppImgSaver;

import java.util.ArrayList;
import java.util.List;

import math.Collections3;
import math.structure.Line;
import math.structure.Rect;
import rap.RAP;
import toxi.geom.Vec2D;

public class Liner extends RAP {
	
	Rect					frame;
	
	ArrayList<List<Vec2D>>	points		= new ArrayList<List<Vec2D>>();
	Vec2D					ecken[]		= new Vec2D[4];
	int						n			= 300;
	
	float					r			= 150;
	float					actPhase	= 0;
	float					speed		= 0.05f;
	
	@Override
	public void setup() {
		super.setup();
		size(600, 600);
		frame = frameAsRect();
		for (int i = 0; i < 4; i++) {
			points.add(Line.nEqualPoints(frame.rect.getEdge(i), n));
			ecken[i] = frame.getVertex(i);
		}
		background(0);
		stroke(255);
		colorMode(HSB, n);
		strokeWeight(2);
		ellipseMode(RADIUS);
		// AppImgSaver.autoSaveAllToFrameAndExit(200);
	}
	
	@Override
	public void draw() {
		// System.out.println("**********");
		r = (abs(sin(actPhase * .5f))) * width / 2;
		background(0);
		actPhase += speed;
		int n = (int) (abs(sin(actPhase)) * this.n);
		float freq = actPhase;
		Vec2D v, e1, e2;
		for (int i = 0; i < 4; i++) {
			List<Vec2D> p = Collections3.pickN(points.get(i), n, false);
			e1 = ecken[(i + 2) % 4];
			e2 = ecken[(i + 3) % 4];
			// System.out.println(i + " > " + ((i + 2) % 4) + " | " + ((i + 3) % 4));
			for (int j = 0; j < n; j++) {
				v = p.get(j);
				stroke(j, this.n, this.n);
				// line(v.x, v.y, e1.x, e1.y);
				line(v.x, v.y, width / 2 + cos(freq) * r, height / 2 + sin(freq) * r);
				// line(v.x, v.y, e2.x, e2.y);
			}
		}
		noStroke();
		fill(0);
		ellipse(width / 2, height / 2, r, r);
		if (!recording)
			speed = mouseX / (float) width * 0.5f;
	}
	
	boolean	recording	= false;
	
	@Override
	public void mousePressed() {
		super.mousePressed();
		recording = true;
		AppImgSaver.autoSaveAllToFrameAndExit((int) (TWO_PI / speed * 10));
	}
}
