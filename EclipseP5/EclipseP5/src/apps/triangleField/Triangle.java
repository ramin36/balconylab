package apps.triangleField;

import java.awt.Point;

import math.MVector;
import math.structure.FieldElement;
import processing.core.PShape;
import rap.RAP;
import rap.RAPUser;
import toxi.geom.Triangle2D;

public class Triangle extends FieldElement implements RAPUser {
	
	public static RAP	rap		= RAP.getRAP(Triangle.class);
	
	final MVector		pos;
	final int			orientation;
	
	static final int	UP		= 0, DOWN = 1;
	
	static float		r;
	
	PShape				shape;
	MVector				ps[]	= new MVector[3];
	Triangle2D			tri;
	
	public Triangle(Point p, MVector pos, int orientation) {
		super(p);
		this.pos = pos;
		this.orientation = orientation;
		shape = new PShape();
		
		for (int a = 30, i = 0; a <= 270; a += 120, i++) {
			ps[i] = new MVector(pos.x + RAP.cos(RAP.radians(a)) * r, pos.y
					+ RAP.sin(RAP.radians(a)) * r);
			shape.vertex(ps[i].x, ps[i].y);
			System.out.println(ps[i]);
		}
		shape.end();
		tri = new Triangle2D(ps[0].toVec2D(), ps[1].toVec2D(), ps[2].toVec2D());
		System.out.println("++++++");
	}
	
	public void render() {
		if (orientation == UP) {
			// rap.shape(shape, 0, 0);
			rap.triangle(tri);
		}
	}
}
