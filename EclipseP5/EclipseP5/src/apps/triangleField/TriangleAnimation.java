package apps.triangleField;

import java.awt.Point;

import math.MVector;
import math.structure.FieldManager;
import rap.RAP;

public class TriangleAnimation extends RAP {
	
	float					sideL	= 40;
	FieldManager<Triangle>	field;
	
	@Override
	public void setup() {
		super.setup();
		size(800, 600);
		initField();
		stroke(255);
		noFill();
	}
	
	/**
	 * okay this stuff doesnt look to good yet and there is a much simpler way to build it up
	 * more algorthmic then mathematical, just create the points first(easy) and connect them to
	 * create the triangles
	 */
	private void initField() {
		// Triangle.r = sideL;
		float r = sqrt(3) / 3f * sideL;
		Triangle.r = r;
		float mx = sideL * .5f;
		float h = sqrt(3) / 2 * sideL;
		// sq(r) = sq(x) + sq(y) > y = sqrt(sq(r) - sq(x))
		float my = sqrt(sq(r) - sq(mx));
		// System.out.println(sq(mx) + "," + sq(r) + " " + my);
		MVector mUp = new MVector(mx, my);
		int xs = (int) (width / sideL);
		int ys = (int) (height / sideL);
		field = new FieldManager<Triangle>(2, 2);
		for (int y = 0; y < 2; y++)
			for (int x = 0; x < 2; x++) {
				float myy = y / 2 * h + (y % 2 == 0 ? my : -mx);
				Triangle tri = new Triangle(new Point(x, y), new MVector(sideL / 2 * x, myy), (x
						+ y + 1) % 2);
				field.add(x, y, tri);
			}
	}
	
	@Override
	public void draw() {
		background(0);
		// for (int y = 0; y < ys; y++)
		// for (int x = 0; x < xs; x++)
		for (Triangle tri : field.all())
			tri.render();
	}
	
}
