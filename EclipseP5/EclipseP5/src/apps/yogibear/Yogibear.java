package apps.yogibear;

import rap.RAP;
import voronoi.CircleVoronoi;
import voronoi.VCell;

public class Yogibear extends RAP {
	
	CircleVoronoi<VCell>	voronoi	= new CircleVoronoi<VCell>();
	
	@Override
	public void setup() {
		super.setup();
		size(650, 650);
		initVoronoi();
		stroke(255);
	}
	
	private void initVoronoi() {
		voronoi.numRings = 4;
		Float distances[] = { 20f, 40f, 100f, 540f };
		Integer[] ringCells = new Integer[voronoi.numRings];
		for (int i = 0; i < voronoi.numRings; i++)
			ringCells[i] = (int) random(12, 24);
		ringCells[2] = 12;
		voronoi.useCentralSeed = false;
		voronoi.spread(ringCells, distances);
		voronoi.removeLastRing();
	}
	
	@Override
	public void draw() {
		background(0);
		translate(center());
		voronoi.render();
		// voronoi.render(new Integer[] { 3 });
	}
}
