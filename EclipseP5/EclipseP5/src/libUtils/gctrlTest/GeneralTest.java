package libUtils.gctrlTest;

import generalController.library.main.GeneralController;
import processing.core.PApplet;

public class GeneralTest extends PApplet {
	
	private static final long	serialVersionUID	= 1L;
	GeneralController			gc;
	public int					n;
	
	@Override
	public void draw() {
		// System.out.println(n);
	}
	
	public void n() {
		// println("callback!");
	}
	
	@Override
	public void setup() {
		super.setup();
		gc = new GeneralController(this);
		// 1
		// gc.loadValues("C:/values.xml");
		// gc.loadMap("C:/externalMap.xml");
		// 2
		// gc.load("C:/values.xml", "C:/externalMap.xml");
		// 3
		gc.load("C:/imageRot.xml");
		
		// gc.printValues();
		// gc.printMap();
		// exit();
	}
	
}
