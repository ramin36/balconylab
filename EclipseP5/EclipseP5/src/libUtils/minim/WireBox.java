package libUtils.minim;

import math.structure.Rect;
import processing.core.PVector;

public class WireBox extends Wireable {
	
	// Object object;
	Rect	rect;
	boolean	draw	= true;
	
	public WireBox(Object object, Rect rect) {
		this.object = object;
		this.rect = rect;
		location = rect.center();
	}
	
	@Override
	public boolean isTouched(PVector v) {
		return rect.isInside(v);
	}
	
	@Override
	public void draw() {
		if (!draw)
			return;
		rap.stroke(255);
		rap.fill(0);
		rect.draw();
		rap.fill(255);
		rap.text(object.getClass().getSimpleName(), location);
	}
	
	public void drawWires() {
		rap.stroke(255);
		for (Wireable wiresTo : to)
			rap.line(location, wiresTo.location);
	}
	
	public void wireTo(Wireable wireable) {
		to.add(wireable);
	}
	
	public void wireFrom(Wireable wireable) {
		
	}
}
