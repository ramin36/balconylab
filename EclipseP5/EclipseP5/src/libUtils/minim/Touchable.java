package libUtils.minim;

import processing.core.PVector;
import rap.RAPUser;

public interface Touchable extends RAPUser {
	
	// PVector getLocation();
	
	boolean isTouched(PVector v);
	
	void draw();
}
