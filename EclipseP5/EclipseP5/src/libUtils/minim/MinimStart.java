package libUtils.minim;

import java.util.ArrayList;

import processing.core.PVector;
import rap.RAP;
import ddf.minim.Minim;

/**
 * Visual Minim editor
 * atm.
 * while presing t and mouse a random Oscil is generated. these can be connected to the output
 * 
 * @author Ramin
 * 
 */
public class MinimStart extends RAP {
	
	ArrayList<UGenSymbol>	ugens	= new ArrayList<UGenSymbol>();
	WireBox					outBox;
	
	Minim					minim;
	
	UGenSymbol				actual;
	
	@Override
	public void setup() {
		super.setup();
		size(800, 600);
		rectMode(CENTER);
		stroke(255);
		noFill();
		textSize(15);
		textAlign(CENTER, CENTER);
		//
		minim = new Minim(this);
		outBox = new WireBox(minim.getLineOut(), getTextBox("OUT", new PVector(30, 30)));
	}
	
	@Override
	public void draw() {
		background(0);
		for (UGenSymbol ugen : ugens)
			ugen.drawWires();
		for (UGenSymbol ugen : ugens)
			ugen.draw();
		outBox.draw();
		if (outBox.isTouched(mousePos())) {
			stroke(255, 0, 0);
			ellipse(mousePos(), 5, 5);
		}
	}
	
	@Override
	public void mousePressed() {
		if (keyPressed && key == 't') {
			addUGen(createRandomWaveform());
		}
	}
	
	@Override
	public void mouseDragged() {
		for (UGenSymbol ugen : ugens)
			if (ugen.isTouched(mousePos()))
				actual = ugen;
	}
	
	@Override
	public void mouseReleased() {
		if (actual != null) {
			if (outBox.isTouched(mousePos()))
				actual.wireTo(outBox);
			actual = null;
		}
	}
	
	private void addUGen(Oscil waveform) {
		ugens.add(new UGenSymbol(waveform, new PVector(mouseX, mouseY)));
	}
	
	private Oscil createRandomWaveform() {
		return new Oscil(random(2000, 5000), .1f, getRandomType());
	}
	
	private Wavetable getRandomType() {
		int rand = (int) random(5);
		switch (rand) {
			case 0:
				return Waves.SINE;
			case 1:
				return Waves.TRIANGLE;
			case 2:
				return Waves.SQUARE;
			case 3:
				return Waves.randomNoise();
			case 4:
				return Waves.pulse(0.5f);
			default:
				return Waves.SINE;
		}
	}
	
}
