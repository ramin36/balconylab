package libUtils.minim;

import processing.core.PVector;
import ddf.minim.AudioOutput;
import ddf.minim.UGen;

public class UGenSymbol extends WireBox {
	
	public UGenSymbol(UGen ugen, PVector location) {
		super(ugen, rap.getTextBox(ugen.getClass().getSimpleName(), location));
	}
	
	@Override
	public void wireTo(Wireable wireable) {
		super.wireTo(wireable);
		if (wireable.object instanceof AudioOutput)
			((UGen) object).patch((AudioOutput) wireable.object);
		// if(wireable.object.getClass())t
	}
}
