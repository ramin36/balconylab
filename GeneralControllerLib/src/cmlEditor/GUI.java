package cmlEditor;

import processing.core.PFont;

/**
 * TODO:
 * problems with the new button
 * when name of a value changed. order gets messed up
 * 
 * @author Ramin
 * 
 */
public class GUI implements RAPUser {
	
	public static CMLEditor	rap			= (CMLEditor) RAP.getRAP(GUI.class);
	EditorValue				actValue	= new EditorValue();
	
	ControlP5				cp5;
	int						deactFgClr;
	
	boolean					editMode;
	int						eleH		= 60, yInd = -1, xOff = 20;
	
	Textarea				elemOvervTA;
	PFont					font;
	int						NAME		= 1, TYPE = 2, DESCRIPTION = 3, MIN = 4, INIT = 5, MAX = 6,
			ID = 7, CALLBACK = 8, NEW = 9, ADD_ = 10, DEL = 11, VALUES = 12, WRITE_XML = 13;
	Textfield				nameF, descriptionF, min_valueF, max_valueF, initF, idF, callbackF;
	Bang					newB, addB, delB, writeXML;
	
	int						prioFgClr;
	
	int						stdFgClr;
	DropdownList			typeL;
	boolean					valueChangeIgnore;
	
	// int valueId;
	
	DropdownList			valueList;
	ValueMgmt				valueMgmt;
	int						xLength		= 200;
	
	/*
	 * user pressing add
	 */
	void addVal() {
		// System.out.println(actValue.name);
		if (!actValue.name.equals("") && valueMgmt.addValue(actValue)) {
			valueChangeIgnore = true;
			valueList.addItem(actValue.name, valueMgmt.size() - 1);
			valueList.setIndex(valueMgmt.size() - 1);
			changeMode(true);
			valueChangeIgnore = false;
			// changeMode(true); this is called due adding something to the valueslist
		} else
			nameF.setFocus(true);
	}
	
	void callback(String s) {
		actValue.callback = s;
	}
	
	void changeMode(boolean edit) {
		// RAP.println("edit: " + edit);
		if (edit) { // editing, selected from list or added
			delB.setLock(false);
			addB.setLock(true);
			addB.setColorForeground(deactFgClr);
			addB.setColorActive(deactFgClr);
			delB.setColorForeground(stdFgClr);
		} else { // new value
			valueList.setValue(-1);
			delB.setLock(true);
			addB.setLock(true);
			delB.setColorForeground(deactFgClr);
			nameF.setColorForeground(prioFgClr);
			addB.setColorForeground(deactFgClr);
		}
		editMode = edit;
	}
	
	public void controlEvent(ControlEvent theEvent) {
		// System.out.println("event");
		int id = theEvent.getId();
		// System.out.println(id);
		if (id >= NAME && id <= CALLBACK)
			elemOvervTA.setText(actValue.toString());
		else if (id == VALUES && !valueChangeIgnore) {// (theEvent.isGroup()) {
			int index = (int) theEvent.getGroup().getValue();
			// System.out.println(index);
			actValue = valueMgmt.getValue(index);
			// System.out.println(actValue);
			fillValFields(actValue);
			changeMode(true);
		}
	}
	
	void createGUI() {
		
		valueMgmt = rap.valueMgmt;
		font = rap.createFont("arial", 16);
		
		cp5 = new ControlP5(rap);
		cp5.setControlFont(font, 12);
		
		cp5.getTab("default").setLabel("Value");
		
		cp5.addTab("Externals");
		
		stdFgClr = rap.color(1, 108, 158);
		prioFgClr = rap.color(200, 0, 100);
		deactFgClr = rap.color(100);
		
		createValueGUI();
		newVal();
	}
	
	void createValueGUI() {
		
		// overview
		
		elemOvervTA = cp5.addTextarea("elemOverview").setPosition(240, 40).setSize(300, 230)
				.setColorBackground(rap.color(255, 100)).setColorForeground(rap.color(255, 100))
				.setLineHeight(14).setFont(font).moveTo("default").setText("Lorem I");
		
		// edit value
		
		nameF = cp5.addTextfield("name").setPosition(xOff, getNextHeight()).setSize(130, eleH / 2)
				.setFont(font).setAutoClear(false).setId(NAME).moveTo("default");
		nameF.plugTo(this);
		
		typeL = cp5.addDropdownList("type").setPosition(xOff + 140, getActHeight() + 16)
				.setSize(60, eleH).setItemHeight(15).setId(TYPE).setBarHeight(16).moveTo("default");
		typeL.addItem("Int", 0);
		typeL.addItem("Float", 1);
		typeL.setValue(1);
		
		descriptionF = cp5.addTextfield("description").setPosition(xOff, getNextHeight())
				.setSize(200, eleH / 2).setFont(font).setAutoClear(false).setId(DESCRIPTION)
				.moveTo("default");
		descriptionF.plugTo(this);
		
		min_valueF = cp5.addTextfield("min_value").setPosition(xOff, getNextHeight())
				.setSize(60, eleH / 2).setFont(font).setAutoClear(false).setId(MIN).setText("0")
				.moveTo("default");
		min_valueF.plugTo(this);
		initF = cp5.addTextfield("init").setPosition(xOff + 70, getActHeight())
				.setSize(60, eleH / 2).setFont(font).setAutoClear(false).setValue("0.5")
				.setId(INIT).moveTo("default");
		initF.plugTo(this);
		max_valueF = cp5.addTextfield("max_value").setPosition(xOff + 140, getActHeight())
				.setSize(60, eleH / 2).setFont(font).setAutoClear(false).setText("1").setId(MAX)
				.moveTo("default");
		max_valueF.plugTo(this);
		
		idF = cp5.addTextfield("id").setPosition(xOff, getNextHeight()).setSize(60, eleH / 2)
				.setFont(font).setAutoClear(false).setId(ID).setText("0").moveTo("default");
		idF.plugTo(this);
		
		callbackF = cp5.addTextfield("callback").setPosition(xOff + 70, getActHeight())
				.setSize(130, eleH / 2).setFont(font).setAutoClear(false).setId(CALLBACK)
				.setText("").moveTo("default");
		callbackF.plugTo(this);
		
		// add, new, del
		
		newB = cp5.addBang("newVal").setPosition(20, getNextHeight()).setCaptionLabel("new")
				.setId(NEW).setSize(40, eleH / 2);
		newB.plugTo(this);
		
		addB = cp5.addBang("addVal").setPosition(70, getActHeight()).setId(ADD_)
				.setCaptionLabel("add").setSize(40, eleH / 2);
		addB.plugTo(this);
		
		delB = cp5.addBang("del").setPosition(120, getActHeight()).setId(DEL).setSize(40, eleH / 2);
		delB.plugTo(this);
		
		valueList = cp5.addDropdownList("values").setPosition(240, 290).setItemHeight(20)
				.setHeight(100).setId(VALUES).setBarHeight(15);
		
		writeXML = cp5.addBang("writeXML").setPosition(350, 275).setId(WRITE_XML)
				.setCaptionLabel("write XML").setSize(60, eleH / 2);
		writeXML.plugTo(this);
	}
	
	/*
	 * user pressing del
	 */
	void del() {
		if (!valueMgmt.remove(actValue.name))
			return;
		valueList.removeItem(actValue.name);
		valueList.setIndex(0);
		newVal();
	}
	
	void description(String d) {
		actValue.description = d;
	}
	
	void fillValFields(EditorValue v) {
		nameF.setText(v.name);
		descriptionF.setText(v.description);
		min_valueF.setText(String.valueOf(v.min));
		max_valueF.setText(String.valueOf(v.max));
		initF.setText(String.valueOf(v.initVal));
		idF.setText(String.valueOf(v.id));
		callbackF.setText(v.callback);
		elemOvervTA.setText(v.toString());
	}
	
	int getActHeight() {
		return 40 + yInd * eleH;
	}
	
	int getNextHeight() {
		return 40 + ++yInd * eleH;
	}
	
	void id(String d) {
		// println("j");
		try {
			actValue.id = Integer.valueOf(d);
		} catch (NumberFormatException exc) {
			idF.setText(String.valueOf(actValue.id));
		}
	}
	
	void init(String d) {
		
		try {
			actValue.initVal = Float.valueOf(d);
		} catch (NumberFormatException exc) {
			initF.setText(String.valueOf(actValue.initVal));
		}
	}
	
	private void makeAddable() {
		addB.setLock(false);
		addB.setColorForeground(prioFgClr);
	}
	
	void max_value(String d) {
		try {
			actValue.max = Float.valueOf(d);
		} catch (NumberFormatException exc) {
			max_valueF.setText(String.valueOf(actValue.max));
		}
	}
	
	void min_value(String d) {
		try {
			actValue.min = Float.valueOf(d);
		} catch (NumberFormatException exc) {
			min_valueF.setText(String.valueOf(actValue.min));
		}
	}
	
	// *** Value TextFields ***
	void name(String n) {
		if (valueMgmt.hasValueWithName(n))
			nameF.setText("");
		else {
			if (editMode) {
				int index = (int) valueList.getValue();
				// System.out.println(index);
				valueChangeIgnore = true;
				valueList.removeItem(actValue.name);
				// for (String[] s : valueList.getListBoxItems()) {
				// System.out.println(Arrays.toString(s));
				// }
				valueList.addItem(n, index);
				valueList.setIndex(index);
				valueChangeIgnore = false;
				valueMgmt.changeName(actValue, n);
				// nameF.setText(n);
			} else
				makeAddable();
			actValue.name = n;
		}
	}
	
	// new, add, del
	/*
	 * user pressing new
	 */
	void newVal() {
		// System.out.println("new");
		nameF.setFocus(true);
		actValue = new EditorValue();
		fillValFields(actValue);
		changeMode(false);
	}
	
	/*
	 * user presses write XML
	 */
	void writeXML() {
		// xmlOut();
	}
	
	/*
	 * void updateElement() {
	 * 
	 * 
	 * }
	 */
	
}
