package cmlEditor;

import java.io.PrintWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

public class ValuesMarsh {
	
	public static void main(String[] args) {
	}
	
	public void marsh(Values values) {
		JAXBContext jc;
		try {
			jc = JAXBContext.newInstance("values");
			Marshaller m = jc.createMarshaller();
			m.marshal(values, new PrintWriter(System.out));
		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}
}
