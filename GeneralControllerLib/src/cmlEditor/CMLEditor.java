package cmlEditor;


public class CMLEditor extends RAP {
	
	// int eleH = 60;
	// int yInd = -1, xOff = 20;
	// int xLength = 200;
	
	GUI			gui	= new GUI();
	ValueMgmt	valueMgmt;
	
	// boolean editMode = false; // t, if value is in the list already and edits change it
	
	public void controlEvent(ControlEvent theEvent) {
		gui.controlEvent(theEvent);
	}
	
	@Override
	public void draw() {
		background(50);
	}
	
	@Override
	public void keyPressed() {
		// xmlOut();
	}
	
	@Override
	public void setup() {
		super.setup();
		size(600, 400);
		valueMgmt = new ValueMgmt(this);
		gui.createGUI();
		/*
		 * cp5.addTab("Value").setBackgroundColor(color(0, 64))
		 * .setBackgroundHeight(150).setPosition(20, 20);
		 */
	}
	
	void updateValue() {
	}
	
	// public void changeMode(boolean edit) {
	// editMode = edit;
	// }
	
}
