package cmlEditor;

import java.util.ArrayList;
import java.util.HashMap;

public class ValueMgmt {
	
	static private HashMap<String, EditorValue>	map	= new HashMap<String, EditorValue>();
	public static XMLInOut						xmlInOut;
	
	// EditorValue actValue = new EditorValue();
	
	public static int nextId() {
		return map.size();
	}
	
	ArrayList<EditorValue>	values	= new ArrayList<EditorValue>();
	
	public ValueMgmt(RAP rap) {
		xmlInOut = new XMLInOut(rap);
	}
	
	boolean addValue(EditorValue val) {
		if (map.containsKey(val.name))
			return false;
		values.add(val);
		map.put(val.name, val);
		return true;
	}
	
	public void changeName(EditorValue actValue, String newName) {
		map.remove(actValue.name);
		map.put(newName, actValue);
	}
	
	EditorValue getValue(int index) {
		return values.get(index);
	}
	
	public boolean hasValueWithName(String name) {
		return map.containsKey(name);
	}
	
	void rearangeId() {
		for (int i = 0; i < values.size(); i++)
			values.get(i).id = i;
	}
	
	boolean remove(String valueName) {
		if (map.containsKey(valueName))
			return false;
		// System.out.println("remove");
		map.remove(valueName);
		for (int i = 0; i < values.size(); i++)
			if (values.get(i).name.equals(valueName)) {
				values.remove(i);
				break;
			}
		rearangeId();
		return true;
	}
	
	public int size() {
		return values.size();
	}
	
	void xmlOut() {
		xmlOut(values.toArray(new EditorValue[values.size()]));
	}
	
	public void xmlOut(EditorValue[] values) {
		XMLElement valuesXML = new XMLElement("values");
		for (EditorValue v : values)
			valuesXML.addChild(v.xmlOut());
		xmlInOut.saveElement(valuesXML, "C:/values.xml");
	}
	
}
