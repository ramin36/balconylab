package cmlEditor;

import java.lang.Thread.State;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

import processing.core.PApplet;

/**
 * saves the initial value until control value passes the initial value
 */
// boolean initialTake = true;

public class Value {
	
	public class DelayThread extends Thread {
		
		public boolean	done;
		// ValueS valS;
		public float	value;
		
		public DelayThread(Value valS) {
			super();
			// this.valS = valS;
		}
		
		@Override
		public void run() {
			try {
				Thread.sleep(values.msDelay);
				send(value);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * when Pickup is used, the value doesn't change until it passes the actual value.
	 * checked in the set.
	 */
	static boolean					usePickUp	= false;
	
	// public static RAP rap = RAP.getRAP(ValuesS.class);
	
	static Values					values;
	private Object					calcVal;
	private Method					callback;
	DelayThread						delayThread	= new DelayThread(null);
	public Field					directSet;
	boolean							getCalcV;
	public int						id;
	
	public float					incomeIntervalMin	= 0, incomeIntervalMax = 1;
	private Object					initVal;
	public float					min, max;
	
	public String					name;
	
	// Log logger = LogFactory.getLog(ValuesS.class);
	
	/**
	 * indicates if the initial slider pos is lower then the init value.
	 * value will then not change until set value is above initVal. initLow
	 * is
	 * set like this. 0: unset, 1: initVal is above ctrl. calculated val
	 * will not set until its above initVal
	 * -1: initVal is below ctrl. val will not change until its under
	 * calculated initVal
	 */
	int								pickUp;
	
	public Class<? extends Number>	type;
	
	private Object					value;
	
	public void callback() {
		if (callback != null)
			try {
				callback.invoke(Values.applet, new Object[] {});
			} catch (Exception e) {
				e.printStackTrace();
			}
	}
	
	public void defaultInit() {
		if (type == Integer.TYPE)
			initVal = (max + min) / 2;
		else if (type == Float.TYPE)
			initVal = (max + min) / 2;
	}
	
	public boolean delay(float val) {
		if (values.useDelay) {
			if (delayThread.done)
				return true;
			State state = delayThread.getState();
			if (delayThread.isAlive()) {
				delayThread.value = val;
				return false;
			} else {
				delayThread = new DelayThread(this);
				delayThread.start();
				delayThread.value = val;
				if (state == State.NEW)
					return false;
			}
		}
		return true;
	}
	
	protected void directSet() {
		if (directSet == null)
			return;
		try {
			if (type == Integer.TYPE)
				directSet.set(Values.applet, value);
			else if (type == Float.TYPE)
				directSet.set(Values.applet, value);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Object get() {
		if (getCalcV)
			return calcVal;
		else
			return value;
	}
	
	public float getFloat() {
		if (type == Integer.TYPE)
			return (Integer) get();
		if (type == Float.TYPE)
			return (Float) get();
		else
			return 0;
	}
	
	/**
	 * go around calcVal
	 * 
	 * @return
	 */
	public float getRealF() {
		if (type == Integer.TYPE)
			return (Integer) value;
		if (type == Float.TYPE)
			return (Float) value;
		else
			return 0;
	}
	
	// public void incomeMap(float min, float max) {
	// incomeIntervalMin = min;
	// incomeIntervalMax = max;
	// }
	
	public void init(String initS) {
		if (type == Integer.TYPE)
			initVal = Integer.valueOf(initS);
		else if (type == Float.TYPE)
			initVal = Float.valueOf(initS);
	}
	
	public void initVal() {
		value = initVal;
	}
	
	public float map(float val) {
		return PApplet.map(val, incomeIntervalMin, incomeIntervalMax, min, max);
	}
	
	public float mapConstrain(float val) {
		return PApplet.constrain(val, incomeIntervalMin, incomeIntervalMax);
	}
	
	public boolean pickupCheck(float mapped) {
		if (pickUp == 0)
			if (mapped < getFloat())
				pickUp = 1;
			else
				pickUp = -1;
		if (pickUp == 1 && mapped < getFloat() || pickUp == -1 && mapped > getFloat()) {
			if (values.info != null)
				values.info.add("p-" + "val: " + values.info.formatter.format(mapped)
						+ (pickUp == 1 ? " to low: " : " to high: ")
						+ values.info.formatter.format(getFloat()));
			return true;
		}
		pickUp = 2;
		return false;
	}
	
	public void send(float val) {
		if (value == null)
			return;
		val = mapConstrain(val);
		// TODO
		// if (useDelay)
		// if (value.delay(val))
		// val = value.delayThread.value;
		// else
		// return;
		float mapped = map(val);
		if (usePickUp && pickupCheck(mapped))
			return;
		// logger.info(name + " " + value + " > " + mapped);
		if (set(mapped))
			callback();
	}
	
	/**
	 * more refactoring!
	 * 
	 * @param val
	 * @return
	 */
	public boolean set(float val) {
		if (type == Integer.TYPE) {
			// int oldVal = type.cast(value).intValue(); crashes
			int oldVal = (Integer) value;
			value = (int) val;
			if (oldVal == (Integer) value)
				return false;
		} else if (type == Float.TYPE) {
			float oldVal = (Float) value;
			value = (float) val;
			if (oldVal == (Float) value)
				return false;
		}
		if (values.info != null)
			values.info.add("-s" + name + ": " + values.info.formatter.format(value));
		directSet();
		return true;
	}
	
	/**
	 * should be called by the callback. calced value will automatically be used in the get, if its set
	 * 
	 * @param calcVal
	 */
	public void setCalcVal(Object calcVal) {
		this.calcVal = calcVal;
		getCalcV = true;
	}
	
	public void setCallback(Method m) {
		callback = m;
	}
	
	public void setIncomeMinMax(float min, float max) {
		incomeIntervalMin = min;
		incomeIntervalMax = max;
	}
	
	@Override
	public String toString() {
		return name + ":" + id + " (" + type.getSimpleName() + ")> " + initVal + "[" + min + "|"
				+ max + "]" + (callback != null ? " calls: " + callback.getName() : "")
				+ (directSet != null ? " |~> " + directSet.getName() : "");
	}
	
}
