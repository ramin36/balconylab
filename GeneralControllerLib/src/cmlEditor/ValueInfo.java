package cmlEditor;

import java.text.DecimalFormat;
import java.util.ConcurrentModificationException;
import java.util.LinkedList;

/**
 * Visualization (make general Streaming) of Values
 * 
 * @author Ramin
 * 
 */
public class ValueInfo {
	
	public boolean		clearBox	= true;
	DecimalFormat		formatter	= new DecimalFormat("####.###");
	String				line		= System.getProperty("line.separator");
	
	LinkedList<String>	stack;
	public int			stackSize	= 5;
	
	Values				vals;
	
	public ValueInfo(Values vals) {
		super();
		this.vals = vals;
		stack = new LinkedList<String>();
	}
	
	public void add(String msg) {
		stack.add(msg.substring(2));
		if (stack.size() > stackSize)
			stack.removeFirst();
	}
	
	public String getInfo() {
		StringBuilder sb = new StringBuilder();
		try {
			for (String s : stack) {
				sb.append(s);
				if (stack.getLast() != s)
					sb.append(line);
			}
		} catch (ConcurrentModificationException exc) {
			return "";
		}
		return sb.toString();
	}
}
