package cmlEditor;


public class EditorValue {
	
	public String					callback	= "";
	public String					description	= "";
	public int						id			= ValueMgmt.nextId();
	public float					min			= 0, max = 1, initVal = .5f;
	public String					name		= "";
	public Class<? extends Number>	type		= Float.TYPE;
	
	public EditorValue setCallback(String callback) {
		this.callback = callback;
		return this;
	}
	
	public EditorValue setDescription(String description) {
		this.description = description;
		return this;
	}
	
	public EditorValue setId(int id) {
		this.id = id;
		return this;
	}
	
	public EditorValue setInitVal(float initVal) {
		this.initVal = initVal;
		return this;
	}
	
	public EditorValue setMax(float max) {
		this.max = max;
		return this;
	}
	
	public EditorValue setMin(float min) {
		this.min = min;
		return this;
	}
	
	public EditorValue setName(String name) {
		this.name = name;
		return this;
	}
	
	public EditorValue setType(Class<? extends Number> type) {
		this.type = type;
		return this;
	}
	
	@Override
	public String toString() {
		return name + ": ID:" + id + " (" + type.getSimpleName() + ")\n> init:" + initVal
				+ "  min/max: [" + min + " | " + max + "]"
				+ (callback != "" ? " calls: " + "\n" + callback : "") + "\n" + description;
	}
	
	XMLElement xmlOut() {
		XMLElement value = new XMLElement("value");
		value.addAttribute("name", name);
		value.addAttribute("description", description);
		value.addAttribute("id", String.valueOf(id));
		value.addAttribute("type", type.getSimpleName());
		if (type == Integer.TYPE) { // TODO. default of int: initV 0, min 0, max 1
			value.addAttribute("init", String.valueOf((int) initVal));
			// initVal = new Float(this.initVal).intValue();
			// min = new Float(this.min).intValue();
			// max = new Float(this.max).intValue();
			XMLElement range = new XMLElement("range");
			range.addAttribute("min", String.valueOf((int) min));
			range.addAttribute("max", String.valueOf((int) max));
			value.addChild(range);
		} else {
			if (initVal != 0.5)
				value.addAttribute("init", String.valueOf(initVal));
			
			if (min != 0 || max != 1) {
				XMLElement range = new XMLElement("range");
				range.addAttribute("min", String.valueOf(min));
				range.addAttribute("max", String.valueOf(max));
				value.addChild(range);
			}
		}
		return value;
	}
	
}
