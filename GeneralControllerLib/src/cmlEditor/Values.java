package cmlEditor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import deprecated.handlerMgmt;
import parser.Parser;
import processing.core.PApplet;

/**
 * collecting values. initiated by xml or (annotated values)
 * HOWTO: implements a class implementing interface ValueInput. set method... to feed the Value object
 * call init(String folder) method with the folder that contains: values.xml
 * define values in the xml
 * set the values in the caller method (with the midi control for example)
 * 
 * 
 * added: auto callback using method name as value name
 * added: auto value set, which is using reflections
 * 
 * @author Ramin
 * 
 */
public class Values {
	
	static public PApplet				applet;
	public ValueInfo					info;
	final handlerMgmt					mgmt;
	
	/**
	 * for rePickup
	 * has been removed ATM. check parser init, which gets the maxId
	 */
	// private int maxId;
	
	int									msDelay		= 100;
	
	/**
	 * if delay is used a value change is triggered "msDelay" after it is touched. when delay is expired
	 * it will use the last set value.
	 */
	public boolean						useDelay	= false;
	
	public HashMap<Integer, Value>	vals		= new HashMap<Integer, Value>();
	
	// protected HashMap<String, ValuesS> valsN = new HashMap<String, ValuesS>();
	
	// private HashMap<String, ExternalHandler> external;
	
	public Values(PApplet ap) {
		applet = ap;
		Value.values = this;
		info = new ValueInfo(this);
		mgmt = new handlerMgmt(this, ap);
	}
	
	/**
	 * creates random value-set vals (e.g. 0-127)
	 * but does not set them
	 * 
	 * @return
	 */
	public HashMap<Integer, Integer> createRandomValues() {
		return createRandomValues(vals.size());
	}
	
	public HashMap<Integer, Integer> createRandomValues(int number) {
		HashMap<Integer, Integer> vals = new HashMap<Integer, Integer>();
		int c = 0;
		ArrayList<Integer> keys = new ArrayList<Integer>();
		keys.addAll(this.vals.keySet());
		Collections.shuffle(keys);
		for (Integer i : keys) {
			Value value = getVal(i);
			vals.put(i, (int) value.incomeIntervalMin
					+ (int) (Math.random() * (value.incomeIntervalMax - value.incomeIntervalMin)));
			if (c++ == number)
				break;
		}
		return vals;
	}
	
	public Object get(final int id) {
		return vals.get(id).get();
	}
	
	//
	// public Object get(final String name) {
	// return valsN.get(name).get();
	// }
	
	/**
	 * the control values are the values mapped back to the control values, which come in to set the values
	 * 
	 * @return
	 */
	public HashMap<Integer, Integer> getControlValues() {
		HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
		for (Value val : vals.values())
			map.put(val.id, (int) PApplet.map(val.getRealF(), val.min, val.max,
					val.incomeIntervalMin, val.incomeIntervalMax));
		return map;
	}
	
	public Value getVal(final int id) {
		// if (execExternalMap)
		// return externalMap.get(id);
		// else
		return vals.get(id);
	}
	
	// public ValuesS getVal(final String name) {
	// return valsN.get(name);
	// }
	
	/**
	 * instead of using the incomInterv to map a value use a Value specific range
	 * 
	 * @param valN
	 * @param min
	 * @param max
	 */
	// public void incomeMap(String valN, float min, float max) {
	// valsN.get(valN).incomeMap(min, max);
	// }
	
	// private void callback(final ValuesS v) {
	// try {
	// if (v.callback != null) {
	// v.callback.invoke(parent, new Object[] {});
	// }
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
	
	// public void init() {
	// init(xmlValueFile, valueMatchFile);
	// }
	
	/**
	 * Reads a XML File and creates the Values map
	 * 
	 * @param folder
	 */
	// public void init(String file) {
	// init(file, valueMatchFile);
	// }
	
	/**
	 * if a control is touched, other vals using the same ctrl have to be reset to pickup
	 * WARNING: works only with right id numbering
	 * 
	 * @param num
	 */
	// public void setRepickUp(int num) {
	// int not = num / 100;
	// int idT = num % 100;
	// for (int i = 0; i < maxId; i++) {
	// if (i == not)
	// continue;
	// ValuesS s = vals.get(i * 100 + idT);
	// if (s != null)
	// s.pickUp = 0;
	// }
	// }
	
	// public void setSetterVals(HashMap<Integer, Integer> vals) {
	// boolean pu = usePickUp;
	// for (Integer id : vals.keySet())
	// set(id, vals.get(id));
	// for (Integer id : vals.keySet())
	// this.vals.get(id).callback();
	// usePickUp = pu;
	// }
	
	public void handleInput(String handlerName, float value) {
		handlerMgmt.handleInput(handlerName, value);
	}
	
	/**
	 * called by the applet to set a value
	 * 
	 * @param id
	 * @param val
	 */
	// public void set(int id, float val) {
	// set(getVal(id), val);
	// }
	
	// public void set(final String valName, final int val) {
	// set(valsN.get(valName).id, val);
	// }
	
	// public void mapSet(final String map, final int val) {
	// externalMap.get(arg0)
	// }
	
	/**
	 * depr
	 * 
	 * @param value
	 * @param val
	 */
	// private void set(ValuesS value, float val) {
	// if (value == null)
	// return;
	// val = value.mapConstrain(val);
	// // System.out.println("asking for " + id + " val: " + val);
	// if (useDelay)
	// if (value.delay(val))
	// val = value.delayThread.value;
	// else
	// return;
	// float mapped = value.map(val);
	// if (usePickUp && value.pickupCheck(mapped))
	// return;
	// // System.out.print(v.value + "> " + mapped);
	// if (value.set(mapped))
	// value.callback();
	// }
	
	/**
	 * @param valueFile
	 * @param mappingFile
	 */
	public void init(String valueFile, String mappingFile) {
		Parser parser = getParserFor(valueFile);
		if (parser != null)
			vals = parser.parseValues(valueFile);
	}
	
	/**
	 * initializes with the values defined in the XML the values and calls the callbacks
	 */
	public void initVals() {
		for (Value v : vals.values()) {
			v.initVal();
			v.directSet();
		}
		for (Value v : vals.values())
			v.callback();
	}
	
	/**
	 * sets all values, which ids are in the map with the given vals
	 * alternative ValueChange class for more gradual change
	 * 
	 * @param vals
	 */
	public void setDirectVals(HashMap<Integer, String> vals) {
		for (Integer i : vals.keySet())
			this.vals.get(i).set(Float.valueOf(vals.get(i)));
		for (Integer i : vals.keySet())
			this.vals.get(i).callback();
	}
	
}
