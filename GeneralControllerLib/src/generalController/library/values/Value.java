package generalController.library.values;

import generalController.library.common.Range;
import generalController.library.main.Properties;
import generalController.library.recorder.ProcessRecorder;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import lombok.Getter;
import lombok.ToString;
import processing.core.PApplet;

/**
 * Stores the values
 * 
 * @author Ramin
 * 
 */
@ToString
public class Value {
	
	public static PApplet	ap;
	
	@Getter
	private final String	name;
	final int				id;
	@Getter
	final String			description;
	@Getter
	final Class<?>			type;
	@Getter
	public final Range		range;
	@Getter
	final float				init;
	@Getter
	final Field				directSet;
	@Getter
	final Method			callback;
	
	float					value;
	
	public static boolean	usePickUp		= Properties.usePickUp;
	public static boolean	printPickUp		= Properties.printPickUp;
	public static boolean	printNewValue	= Properties.printNewValue;
	/**
	 * indicates if the initial value is lower then the init value.
	 * value will then not change until set value is above initVal. pickUp
	 * is set like this. 0: unset, 1: initVal is above incoming vals. calculated val
	 * will not set until its above initVal
	 * -1: initVal is below ctrl. val will not change until its under
	 * calculated initVal
	 */
	int						pickUp;
	
	public static boolean	recording;
	
	public Value(String name, int id, String description, Field directSet, Method callback,
			float init, Range range, Class<?> type) {
		this.name = name;
		this.id = id;
		this.description = description;
		this.directSet = directSet;
		this.callback = callback;
		this.init = init;
		this.range = range;
		this.type = type;
		init();
	}
	
	void init() {
		value = init;
	}
	
	/**
	 * set is called by the coresponding mapping obj to change the value
	 * 
	 * @param value
	 *            new value
	 */
	public void set(float value) {
		if (usePickUp) {
			if (pickupCheck(value))
				setByType(value);
			else
				return;
		} else
			setByType(value);
		if (directSet != null)
			directSet();
		if (callback != null)
			callback();
		forwardValueChangeToGui();
	}
	
	private void forwardValueChangeToGui() {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * an internal setter, that converts the incoming value to the corresponding type value
	 * 
	 * @param value
	 *            new value
	 */
	private void setByType(float value) {
		float oldVal = this.value;
		if (type == Integer.TYPE)
			this.value = (int) value;
		else if (type == Float.TYPE || type == Boolean.TYPE)
			this.value = (float) value;
		if (recording && oldVal != this.value)
			ProcessRecorder.addToRecord(id, this.value);
		if (printNewValue)
			printNewValue();
	}
	
	private void printNewValue() {
		if (type == Integer.TYPE)
			System.out.println(name + ": " + getInt());
		else if (type == Float.TYPE)
			System.out.println(name + ": " + getFloat());
		else if (type == Boolean.TYPE)
			System.out.println(name + ": " + getBoolean());
	}
	
	/**
	 * sets the variable in your sketch if it contains a (public) variable with the same name
	 */
	protected void directSet() {
		try {
			if (type == Integer.TYPE)
				directSet.set(ap, (int) value);
			else if (type == Float.TYPE)
				directSet.set(ap, value);
			else if (type == Boolean.TYPE)
				directSet.set(ap, getBoolean());
		} catch (IllegalAccessException e) {
			System.err.println("directSet for " + name + " denied. It's not public!");
		} catch (IllegalArgumentException e) {
			System.err.println("directSet for " + name + " denied. Wrong type");
		}
	}
	
	/**
	 * calls the callback function if it is defined (by name) or if the sketch has a function ( no parameters) with the
	 * same name as the value
	 */
	private void callback() {
		try {
			callback.invoke(ap, new Object[] {});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public float getFloat() {
		return value;
	}
	
	public int getInt() {
		return (int) value;
	}
	
	/**
	 * gets the boolean value, the boolean value is false if the value is below the middle of the range and true otherwise
	 * 
	 * @return
	 */
	public boolean getBoolean() {
		return value > range.getMid();
	}
	
	/**
	 * pickup sets the value only after the controller passes the init value
	 * 
	 * @param value
	 *            the new value
	 * @return true if the value changes, else false (no value change)
	 */
	public boolean pickupCheck(float value) {
		// System.out.println(value + " " + pickUp);
		if (type == Boolean.class)
			return booleanPickup(value > range.getMid());
		if (pickUp == 0)
			if (value < getFloat())
				pickUp = 1;
			else
				pickUp = -1;
		if (pickUp == 1 && value < getFloat() || pickUp == -1 && value > getFloat()) {
			if (printPickUp)
				System.out.println(name + " : " + value + " but pickup holds " + this.value);
			return false;
		} else
			pickUp = 2;
		return true;
	}
	
	private boolean booleanPickup(boolean value) {
		if (pickUp == 0)
			if (value != getBoolean())
				pickUp = 1;
		if (pickUp == 1 && value != getBoolean()) {
			System.out.println(name + " : " + value + " but pickup holds " + this.value);
			return false;
		} else
			pickUp = 2;
		return true;
	}
	
	public String getValueAsString() {
		return String.valueOf(get());
	}
	
	public Object get() {
		if (type == Integer.TYPE)
			return getInt();
		else if (type == Boolean.TYPE)
			return getBoolean();
		else if (type == Float.TYPE)
			return getFloat();
		else
			return getFloat();
	}
	
	public void set(Object val) {
		if (type == Integer.TYPE) {
			value = (int) Float.parseFloat(val.toString());
		} else if (type == Boolean.TYPE)
			value = (Boolean) val ? range.getMin() : range.getMax();
		else if (type == Float.TYPE)
			value = Float.parseFloat(val.toString());
	}
}
