package generalController.library.values;

import java.util.Collection;
import java.util.HashMap;

/**
 * stores all values
 * 
 * @author Ramin
 * 
 */
public class Values {
	
	HashMap<String, Value>	values			= new HashMap<String, Value>();
	HashMap<Integer, Value>	Indexedvalues	= new HashMap<Integer, Value>();
	
	boolean					overwrite		= false;
	
	public int getNextId() {
		return values.size();
	}
	
	public boolean addValue(Value value) {
		if (!values.containsKey(value) || overwrite) {
			values.put(value.getName(), value);
			Indexedvalues.put(value.id, value);
			return true;
		} else
			return false;
	}
	
	public int getInt(String valName) {
		return values.get(valName).getInt();
	}
	
	public float getFloat(String valName) {
		return values.get(valName).getFloat();
	}
	
	public boolean getBoolean(String valName) {
		return values.get(valName).getBoolean();
	}
	
	public Collection<Value> getValues() {
		return values.values();
	}
	
	public HashMap<String, Object> getValuesMap() {
		HashMap<String, Object> map = new HashMap<String, Object>();
		for (String name : values.keySet())
			map.put(name, values.get(name).get());
		return map;
	}
	
	public void set(HashMap<String, Object> record) {
		for (String vn : record.keySet()) {
			if (!values.containsKey(vn)) {
				System.err.println("ValueRecord contains value " + vn
						+ ",which is not contained in this values");
				continue;
			}
			values.get(vn).set(record.get(vn));
		}
	}
	
	public Value getValue(int id) {
		return Indexedvalues.get(id);
	}
	
	public int size() {
		return values.size();
	}
	
}
