package generalController.library.main;

import generalController.library.recorder.Recorder;
import generalController.library.values.Value;
import generalController.library.values.Values;

import java.awt.Frame;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import processing.core.PApplet;
import controlP5.ControlEvent;
import controlP5.ControlP5;
import controlP5.Group;
import controlP5.Tab;

public class ExtraWindow extends PApplet {
	
	private static final long	serialVersionUID	= 1L;
	
	Frame						f;
	
	public int					w, h;
	
	ControlP5					cp5;
	
	private Values				values;
	
	private static final int	groupHeight			= 60;
	
	ArrayList<Group>			valueGroups			= new ArrayList<Group>();
	ArrayList<Value>			valuesL				= new ArrayList<Value>();
	
	/**
	 * after creation
	 */
	private boolean				listen;
	
	public static final int		SNAPSHOT_ID			= -1;
	
	public static ExtraWindow getExtraWindow(String name, Values values) {
		try {
			ExtraWindow p = new ExtraWindow(400, min(600, groupHeight * (values.size() + 2)));
			p.f = new Frame(name);
			p.f.add(p);
			p.values = values;
			p.init();
			p.f.setSize(p.w, p.h);
			p.f.setLocation(100, 100);
			// p.f.setResizable(false);
			p.f.setVisible(true);
			return p;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private ExtraWindow(int theWidth, int theHeight) {
		w = theWidth;
		h = theHeight;
	}
	
	private void initGui() {
		cp5 = new ControlP5(this);
		cp5.addTab("mappings");
		cp5.getTab("default").activateEvent(true).setLabel("values");
		cp5.getTab("mappings").activateEvent(true);
		cp5.addTab("records").activateEvent(true);
	}
	
	void initValues(Values values) {
		this.values = values;
		createValuesGui();
		rearangeValueGroups();
		listen = true;
	}
	
	void initRecorder(Recorder recorder) {
		
	}
	
	void createRecorderGui() {
		Group g = cp5.addGroup("states");
		cp5.addBang("snapshot").setPosition(10, 10).setSize(40, 40).setId(SNAPSHOT_ID);
		g.moveTo(cp5.getTab("records"));
	}
	
	private void createValuesGui() {
		Collection<Value> list = values.getValues();
		valuesL = new ArrayList<Value>();
		Tab valuesTab = cp5.getTab("default");
		int id = 0;
		for (Iterator<Value> iter = list.iterator(); iter.hasNext();) {
			Value val = iter.next();
			if (valuesTab.getController(val.getName()) != null)
				continue;
			valuesL.add(val);
			String valName = val.getName();
			Group g = cp5.addGroup(valName);
			g.setWidth(300).activateEvent(true).setLabel(valName).setBackgroundHeight(groupHeight)
					.setBackgroundColor(color(255, 80));
			cp5.addTextlabel(valName + "-min").setText("min: " + val.range.getMin())
					.setPosition(10, 10).setGroup(g);
			cp5.addTextlabel(valName + "-max").setText("max: " + val.range.getMax())
					.setPosition(100, 10).setGroup(g);
			if (val.getType() == Boolean.TYPE)
				cp5.addToggle(valName + "-val").setPosition(10, 25).setSize(200, 20).setGroup(g)
						.setId(id++).setValue(val.getBoolean());
			else
				cp5.addSlider(valName + "-val").setPosition(10, 25).setSize(200, 20)
						.setRange(val.range.getMin(), val.range.getMax()).setGroup(g).setId(id++)
						.setValue(val.getFloat());
			g.close();
			valueGroups.add(g);
			g.moveTo(valuesTab);
		}
	}
	
	private void rearangeValueGroups() {
		int h = 30;
		for (int i = 0; i < valueGroups.size(); i++) {
			valueGroups.get(i).setPosition(10, h);
			if (valueGroups.get(i).isOpen())
				h += groupHeight;
			else
				h += 10;
		}
		
	}
	
	public void controlEvent(ControlEvent theEvent) {
		if (!listen)
			return;
		if (theEvent.isGroup()) {
			// println("got an event from group " + theEvent.getGroup().getName() + ", isOpen? "
			// + theEvent.getGroup().isOpen());
			rearangeValueGroups();
		} else if (theEvent.isController()) {
			if (theEvent.getId() >= 0)
				valuesL.get(theEvent.getId()).set(theEvent.getValue());
		}
	}
	
	@Override
	public void draw() {
		background(150);
	}
	
	@Override
	public void setup() {
		super.setup();
		size(w, h);
		f = (Frame) getAccessibleContext().getAccessibleParent();
		initGui();
		initValues(values);
		// initRecorder(recorder);
		f.addWindowListener(new WindowListener() {
			
			@Override
			public void windowActivated(WindowEvent arg0) {
			}
			
			@Override
			public void windowClosed(WindowEvent e) {
			}
			
			@Override
			public void windowClosing(WindowEvent e) {
				println("close");
				f.setVisible(false);
			}
			
			@Override
			public void windowDeactivated(WindowEvent e) {
			}
			
			@Override
			public void windowDeiconified(WindowEvent e) {
			}
			
			@Override
			public void windowIconified(WindowEvent e) {
			}
			
			@Override
			public void windowOpened(WindowEvent e) {
			}
		});
	}
	
	@Override
	public void setVisible(boolean v) {
		super.setVisible(v);
		f.setVisible(v);
	}
}
