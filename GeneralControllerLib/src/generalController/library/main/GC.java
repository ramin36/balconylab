package generalController.library.main;

import generalController.library.map.Map;
import generalController.library.values.Values;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@XmlRootElement(name = "gc")
@Getter
@Setter
@ToString
public class GC {
	
	Values	values;
	Map		map;
	
	public GC() {
	}
	
	public GC(Values values, Map map) {
		this.values = values;
		this.map = map;
	}
	
}
