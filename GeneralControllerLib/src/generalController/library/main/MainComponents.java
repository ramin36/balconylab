package generalController.library.main;

import generalController.library.builder.Builder;
import generalController.library.map.Map;
import generalController.library.parser.Loader;
import generalController.library.recorder.Recorder;
import generalController.library.values.Values;

public class MainComponents {
	
	private final Loader	loader		= new Loader();
	private final Builder	builder		= new Builder();
	private Values			values;
	private Map				map;
	private final Recorder	recorder	= new Recorder();
	
}
