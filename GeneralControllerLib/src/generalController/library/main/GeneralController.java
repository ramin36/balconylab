package generalController.library.main;

import generalController.library.builder.Builder;
import generalController.library.common.Range;
import generalController.library.map.Map;
import generalController.library.map.Mapping;
import generalController.library.parser.Loader;
import generalController.library.recorder.Recorder;
import generalController.library.values.Value;
import generalController.library.values.Values;
import processing.core.PApplet;

/**
 * Main class, interface for Processing sketch.
 * Create a GeneralController and call the load method. thats it
 * 
 * todo:
 * key controller, internal/gclib-values
 * reloading files
 * 
 * @author Ramin
 * 
 */
public class GeneralController {
	
	public static GeneralController	gc;
	private static PApplet			parent;
	
	private final Loader			loader		= new Loader();
	private final Builder			builder;
	private Values					values;
	private Map						map;
	private final Recorder			recorder	= new Recorder();
	
	private boolean					guiOn		= false;
	ExtraWindow						controllWindow;
	
	MainComponents					mainComponents;
	
	public static PApplet parent() {
		return parent;
	}
	
	public GeneralController(PApplet parent) {
		GeneralController.parent = parent;
		builder = new Builder();
		GeneralController.gc = this;
		Mapping.ap = parent;
		Value.ap = parent;
	}
	
	public void load(String gcFileName) {
		loader.load(gcFileName);
		builder.build(loader.getParsedValues(), loader.getParsedMap());
		values = Builder.getValues();
		map = Builder.getMap();
	}
	
	public void loadValues(String gcFileName) {
		loader.loadValues(gcFileName);
	}
	
	public void loadMap(String gcFileName) {
		loader.loadMap(gcFileName);
		builder.build(loader.getParsedValues(), loader.getParsedMap());
		map = Builder.getMap();
	}
	
	/**
	 * Saves the last set states (values) and initiates the values at the next start
	 * 
	 */
	public void autoSave() {
		// System.out.println("autosave-init");
		recorder.init(Builder.getValues());
		// System.out.println("autosave");
		recorder.autoSave();
	}
	
	public int getInt(String valName) {
		return values.getInt(valName);
	}
	
	public float getFloat(String valName) {
		return values.getFloat(valName);
	}
	
	public boolean getBoolean(String valName) {
		return values.getBoolean(valName);
	}
	
	public boolean addNewValue(String name) {
		return addNewValue(name, Float.class, new Range());
	}
	
	public boolean addNewValue(String name, Class<?> type, Range range) {
		return builder.newValueOnTheFly(name, type, range);
	}
	
	public boolean addNewMapping(String controllerName, String handlerName, int controllerId,
			String mapTo) {
		return addNewMapping(controllerName, handlerName, controllerId, mapTo);
	}
	
	public boolean addNewMapping(String controllerName, String handlerName, Range range,
			int controllerId, String mapTo) {
		return builder.newMappingOnTheFly(controllerName, handlerName, range, controllerId, mapTo);
	}
	
	/**
	 * creates a gc xml file
	 * TODO instead of creating a new GL-> GCXML, add new values,mapping to the existing one
	 */
	public void updateGCFile(String fileName) {
		recorder.updateGCFile(fileName, new GC(values, map));
	}
	
	public boolean startRecording() {
		return recorder.startRecording();
	}
	
	public boolean stopRecording() {
		return recorder.stopRecording();
	}
	
	public void saveLastRecord(String recordName) {
		recorder.saveLastRecord(recordName);
	}
	
	public void saveAllRecords(String recordBaseName) {
		recorder.saveAllRecords(recordBaseName);
	}
	
	public void playLastProcessRecord() {
		recorder.playLastProcessRecord();
	}
	
	public void openControlWindow() {
		if (controllWindow == null)
			controllWindow = ExtraWindow.getExtraWindow("General Controller", values);
		else
			controllWindow.setVisible(true);
		guiOn = true;
	}
	
	public static void forwardValueChangeToGui() {
		
	}
	
}
