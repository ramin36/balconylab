package generalController.library.main;

import generalController.library.common.Range;
import processing.core.PApplet;

public class GeneralTest extends PApplet {
	
	private static final long	serialVersionUID	= 1L;
	GeneralController			gc;
	public int					n;
	
	@Override
	public void draw() {
		// System.out.println(n);
		gc.getInt("a");
	}
	
	public void n() {
		// println("callback!");
	}
	
	@Override
	public void setup() {
		super.setup();
		size(500, 500);
		try {
			gc = new GeneralController(this);
			// 1
			// gc.loadValues("C:/values.xml");
			// gc.loadMap("C:/externalMap.xml");
			// 2
			// gc.load("C:/values.xml", "C:/externalMap.xml");
			// 3
			// gc.load("C:/val-map.json");
			// gc.load("C:/val-map.xml");
			gc.load("C:/x.xml");
			gc.autoSave();
			gc.addNewValue("xxx");
			gc.addNewMapping("nanoKONTROL 1 SLIDER/KNOB", "controllerChange", new Range(0, 127), 2,
					"xxx");
			// gc.printValues();
			// gc.printMap();
			// exit();
		} catch (Exception exc) {
			exc.printStackTrace();
			exit();
		}
	}
	
	@Override
	public void keyPressed() {
		if (key == 'u') // values-map-xml file update // adding new values (todo: mappings)
			gc.updateGCFile("c:/x.xml");
		else if (key == 'a')
			gc.startRecording();
		else if (key == 's') {
			gc.stopRecording();
			gc.saveLastRecord("c:/rec");
		} else if (key == 'e')
			gc.openControlWindow();
	}
}
