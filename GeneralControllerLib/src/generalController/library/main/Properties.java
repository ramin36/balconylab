package generalController.library.main;

public class Properties {
	
	// Value
	public static final boolean	usePickUp		= true;
	public static final boolean	printPickUp		= false;
	public static final boolean	printNewValue	= true;
	// Controller
	public static final boolean	notifyUnmapped	= true;
}
