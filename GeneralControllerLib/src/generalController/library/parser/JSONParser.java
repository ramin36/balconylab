package generalController.library.parser;

import generalController.library.parser.json.GCJSON;
import generalController.library.parser.json.adapter.FloatValueAdapter;
import generalController.library.parser.json.adapter.TypeAdapter;
import generalController.library.parser.parsed.ParsedGC;
import generalController.library.parser.parsed.ParsedMap;
import generalController.library.parser.parsed.ParsedValues;

import java.io.FileNotFoundException;
import java.io.FileReader;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

public class JSONParser implements Parser {
	
	@Override
	public ParsedGC parseGC(String fileName) {
		try {
			GsonBuilder builder = new GsonBuilder();
			Gson gson = builder.registerTypeAdapter(Class.class, new TypeAdapter())
					.registerTypeAdapter(Float.class, new FloatValueAdapter()).create();
			// (Class.class, );
			GCJSON gc = gson.fromJson(new FileReader(fileName), GCJSON.class);
			// System.out.println(gc);
			return gc;
		} catch (JsonSyntaxException e) {
			e.printStackTrace();
		} catch (JsonIOException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	public ParsedValues parseValues(String fileName) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public ParsedMap parseMap(String fileName) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
