package generalController.library.parser.json.value;

import generalController.library.parser.json.RangeJSON;
import generalController.library.parser.parsed.ParsedValue;
import lombok.Getter;
import lombok.ToString;

import com.google.gson.annotations.SerializedName;

@ToString
public class ValueJSON implements ParsedValue {
	
	@Getter
	private String			name;
	@Getter
	private final String	description	= "";
	@Getter
	private final String	callback	= "";
	// @Getter
	// private final String directSet = "";
	@Getter
	private Float			init		= Float.NaN;
	@SerializedName("range")
	@Getter
	private RangeJSON		parsedRange;
	@Getter
	private Class<?>		type;
	
	/**
	 * not cool. bad for extending to more types
	 */
	private void defaultInit() {
		if (type == Integer.TYPE)
			init = new Float((int) (parsedRange.getMin() + parsedRange.getMax()) / 2);
		else if (type == Float.TYPE)
			init = (parsedRange.getMin() + parsedRange.getMax()) / 2;
		else if (type == Boolean.TYPE)
			init = 0f;
	}
	
	@Override
	public void prepare() {
		prepareMissingRange();
		prepareInitValue();
	}
	
	private void prepareInitValue() {
		if (Float.isNaN(init))
			defaultInit();
	}
	
	private void prepareMissingRange() {
		if (parsedRange == null)
			parsedRange = new RangeJSON();
	}
	
}
