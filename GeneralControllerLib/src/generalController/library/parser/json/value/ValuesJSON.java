package generalController.library.parser.json.value;

import generalController.library.parser.parsed.ParsedValues;

import java.util.ArrayList;

import lombok.Getter;
import lombok.ToString;

import com.google.gson.annotations.SerializedName;

@ToString
public class ValuesJSON implements ParsedValues {
	
	@SerializedName("value")
	@Getter
	ArrayList<ValueJSON>	parsedValues;
	
	@Override
	public void prepareValues() {
		for (ValueJSON value : parsedValues)
			value.prepare();
	}
	
	@Override
	public void printParsedValues() {
		for (ValueJSON val : parsedValues)
			System.out.println(val);
	}
	
}
