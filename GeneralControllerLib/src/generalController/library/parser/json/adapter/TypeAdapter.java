package generalController.library.parser.json.adapter;

import generalController.library.parser.Constants;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

public class TypeAdapter implements JsonDeserializer<Class<?>> {
	
	@Override
	public Class<?> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
			throws JsonParseException {
		return Constants.getTypeOf(json.getAsJsonPrimitive().getAsString());
	}
	
}
