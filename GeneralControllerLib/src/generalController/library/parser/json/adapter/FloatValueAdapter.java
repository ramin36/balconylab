package generalController.library.parser.json.adapter;

import generalController.library.parser.Constants;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

public class FloatValueAdapter implements JsonDeserializer<Float> {
	
	@Override
	public Float deserialize(JsonElement json, Type typeOf, JsonDeserializationContext arg2)
			throws JsonParseException {
		try {
			float v = json.getAsFloat();
			return v;
		} catch (NumberFormatException noNumber) {
			return Constants.getValueOf(json.getAsJsonPrimitive().getAsString());
		}
	}
	
}
