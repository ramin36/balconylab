package generalController.library.parser.json.mapping;

import generalController.library.parser.parsed.ParsedController;

import java.util.ArrayList;

import lombok.Getter;

public class ControllerJSON implements ParsedController {
	
	@Getter
	String					name;
	
	@Getter
	ArrayList<HandlerJSON>	handlers	= new ArrayList<HandlerJSON>();
	
}
