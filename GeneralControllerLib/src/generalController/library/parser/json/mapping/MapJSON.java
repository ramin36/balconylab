package generalController.library.parser.json.mapping;

import generalController.library.parser.parsed.ParsedController;
import generalController.library.parser.parsed.ParsedMap;

import java.util.ArrayList;

import lombok.Getter;

import com.google.gson.annotations.SerializedName;

public class MapJSON implements ParsedMap {
	
	@Getter
	@SerializedName("controller")
	private final ArrayList<ControllerJSON>	parsedController	= new ArrayList<ControllerJSON>();
	
	@Override
	public void printParsedMap() {
		for (ParsedController controller : parsedController)
			System.out.println(controller.toString());
	}
	
}
