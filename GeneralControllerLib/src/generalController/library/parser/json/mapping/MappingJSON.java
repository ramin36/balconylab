package generalController.library.parser.json.mapping;

import generalController.library.parser.parsed.ParsedMapping;
import lombok.Getter;

public class MappingJSON implements ParsedMapping {
	
	@Getter
	Integer	ctrlId;
	
	@Getter
	String	mapTo;
	
}
