package generalController.library.parser.json.mapping;

import generalController.library.parser.json.RangeJSON;
import generalController.library.parser.parsed.ParsedHandler;

import java.util.ArrayList;

import lombok.Getter;

import com.google.gson.annotations.SerializedName;

public class HandlerJSON implements ParsedHandler {
	
	@Getter
	String					name;
	
	@SerializedName("range")
	@Getter
	RangeJSON				parsedRange;
	
	@SerializedName("mapping")
	@Getter
	ArrayList<MappingJSON>	parsedMappings	= new ArrayList<MappingJSON>();
	
}
