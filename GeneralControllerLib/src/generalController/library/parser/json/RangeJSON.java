package generalController.library.parser.json;

import generalController.library.parser.parsed.ParsedRange;
import lombok.Getter;
import lombok.ToString;

@ToString
public class RangeJSON implements ParsedRange {
	
	@Getter
	private final Float	max	= 1f;
	
	@Getter
	private final Float	min	= 0f;
	
}
