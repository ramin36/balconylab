package generalController.library.parser.json;

import generalController.library.parser.json.mapping.MapJSON;
import generalController.library.parser.json.value.ValuesJSON;
import generalController.library.parser.parsed.ParsedGC;
import lombok.Getter;
import lombok.ToString;

import com.google.gson.annotations.SerializedName;

@ToString
public class GCJSON implements ParsedGC {
	
	@SerializedName("map")
	@Getter
	private MapJSON		parsedMap;
	@SerializedName("values")
	@Getter
	private ValuesJSON	parsedValues;
	
	@Override
	public void prepare() {
		parsedValues.prepareValues();
	}
	
}
