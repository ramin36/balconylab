package generalController.library.parser;

import generalController.library.devices.builtin.OscDevice;
import generalController.library.main.GeneralController;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

import processing.core.PConstants;
import cmlEditor.Values;

/**
 * Holds some constants that might be used the init/range definition and controller ids.
 * available constants: "width"/"height" applet size, "left"/"right" for mouse buttons controller ids
 * "PI", "HALF_PI"
 * 
 * @author Ramin
 * 
 */
public class Constants {
	
	public static ArrayList<Class<? extends HasConstants>>	hasConstants	= new ArrayList<Class<? extends HasConstants>>();
	private static Class<? extends HasConstants>			nextConstantFrom;
	
	// TODO do it better!
	static {
		registerConstants(OscDevice.class);
	}
	
	/**
	 * returns types
	 * 
	 * @param typeN
	 * @return
	 */
	public static Class<?> getTypeOf(String typeN) {
		if (typeN.equals("int"))
			return Integer.TYPE;
		else if (typeN.equals("float"))
			return Float.TYPE;
		if (typeN.equals("boolean"))
			return Boolean.TYPE;
		else {
			System.err.println("unknown type: " + typeN);
			return Float.TYPE;
		}
	}
	
	public static float getValueOf(String valueN) {
		if (valueN.equals("width"))
			return GeneralController.parent().width;
		else if (valueN.equals("height") && Values.applet != null)
			return GeneralController.parent().height;
		else if (valueN.equals("left"))
			return PConstants.LEFT;
		else if (valueN.equals("right"))
			return PConstants.RIGHT;
		else if (valueN.equals("PI"))
			return PConstants.PI;
		else if (valueN.equals("TWO_PI"))
			return PConstants.TWO_PI;
		else if (valueN.equals("floatMIN"))
			return Float.MIN_VALUE;
		else if (valueN.equals("floatMAX"))
			return Float.MAX_VALUE;
		else if (valueN.equals("intMIN"))
			return Integer.MIN_VALUE;
		else if (valueN.equals("intMAX"))
			return Integer.MAX_VALUE;
		else if (hasConstantsContain(valueN))
			return getConstants(valueN);
		else {
			System.err.println("Constant " + valueN + " not available");
		}
		return 0;
	}
	
	public static void registerConstants(Class<? extends HasConstants> hasC) {
		// System.out
		// .println("Constants.registerConstants " + hasC.getName() + " added for constants");
		hasConstants.add(hasC);
	}
	
	private static float getConstants(String valueN) {
		try {
			HasConstants hasC = nextConstantFrom.getConstructor(new Class<?>[0]).newInstance(
					new Object[0]);
			return (Float) nextConstantFrom.getMethod("getValue", String.class)
					.invoke(hasC, valueN);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	private static boolean hasConstantsContain(String valueN) {
		for (Class<? extends HasConstants> c : hasConstants)
			try {
				HasConstants hasC = c.getConstructor(new Class<?>[0]).newInstance(new Object[0]);
				if ((Boolean) c.getMethod("hasConstant", String.class).invoke(hasC, valueN)) {
					nextConstantFrom = c;
					return true;
				}
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (InstantiationException e) {
				e.printStackTrace();
			}
		return false;
	}
}
