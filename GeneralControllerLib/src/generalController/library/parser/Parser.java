package generalController.library.parser;

import generalController.library.parser.parsed.ParsedGC;
import generalController.library.parser.parsed.ParsedMap;
import generalController.library.parser.parsed.ParsedValues;

/**
 * Parser interface for value or map files
 * 
 * @author Ramin
 * 
 */
public interface Parser {
	
	/**
	 * parses a general controller file, which contains a <gc> tag
	 * 
	 * @param fileName
	 * @return a ParsedGC object contains parsedValues and parsedMap
	 */
	ParsedGC parseGC(String fileName);
	
	/**
	 * parses a value file
	 * 
	 * @param fileName
	 * @return parsedValues object
	 */
	ParsedValues parseValues(String fileName);
	
	/**
	 * parses a map file
	 * 
	 * @param fileName
	 * @return parsedMap object
	 */
	ParsedMap parseMap(String fileName);
	
}
