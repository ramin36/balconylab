package generalController.library.parser.xml.mapping;

import generalController.library.map.Mapping;
import generalController.library.parser.parsed.ParsedMapping;
import generalController.library.parser.xml.adapter.IntValueAdapter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import lombok.Getter;
import lombok.ToString;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "mapping")
@ToString
@Getter
public class MappingXML implements ParsedMapping {
	
	@Getter
	@XmlAttribute
	@XmlJavaTypeAdapter(IntValueAdapter.class)
	private Integer	ctrlId;
	
	@Getter
	@XmlAttribute
	private String	mapTo;
	
	public MappingXML() {
	}
	
	public MappingXML(Mapping mapping) {
		this.ctrlId = mapping.getCtrlId();
		this.mapTo = mapping.getValue().getName();
	}
	
	// @Override
	// public void prepare(ParsedController parsedController, ParsedHandler parsedHandler) {
	// this.controller = parsedController.getName();
	// if (this.handler == null)
	// this.handler = parsedHandler.getName();
	// if (this.range == null)
	// this.range = parsedHandler.getParsedRange();
	// }
	
}
