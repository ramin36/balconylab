package generalController.library.parser.xml.mapping;

import generalController.library.controller.Controller;
import generalController.library.controller.Handler;
import generalController.library.parser.parsed.ParsedController;
import generalController.library.parser.parsed.ParsedHandler;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.ToString;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "controller")
@ToString
public class ControllerXML implements ParsedController {
	
	@Getter
	@XmlAttribute
	private String						name;
	
	@Getter
	@XmlElement(name = "handler", type = HandlerXML.class)
	private final List<ParsedHandler>	handlers	= new ArrayList<ParsedHandler>();
	
	public ControllerXML() {
	}
	
	public ControllerXML(Controller controller) {
		this.name = controller.getControllerName();
		for (Handler handler : controller.getHandlers())
			handlers.add(new HandlerXML(handler));
	}
	// @Override
	// public void prepare() {
	// for (ParsedHandler handler : this.handler)
	// handler.prepare(this);
	// }
	
	// @Override
	// public ArrayList<ParsedMapping> getParsedMappings() {
	// ArrayList<ParsedMapping> mappings = new ArrayList<ParsedMapping>();
	// for (ParsedHandler handler : this.handler)
	// mappings.addAll(handler.getParsedMappings());
	// return mappings;
	// }
	
	//
}
