package generalController.library.parser.xml.mapping;

import generalController.library.controller.Handler;
import generalController.library.map.Mapping;
import generalController.library.parser.parsed.ParsedHandler;
import generalController.library.parser.parsed.ParsedMapping;
import generalController.library.parser.parsed.ParsedRange;
import generalController.library.parser.xml.RangeXML;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.ToString;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "handler")
@ToString
public class HandlerXML implements ParsedHandler {
	
	@Getter
	@XmlAttribute
	private String							name;
	
	@XmlElement(name = "mapping", type = MappingXML.class)
	@Getter
	private final ArrayList<ParsedMapping>	parsedMappings	= new ArrayList<ParsedMapping>();
	
	@Getter
	@XmlElement(name = "range", type = RangeXML.class)
	private ParsedRange						parsedRange;
	
	// @Override
	// public void prepare(ParsedController parsedController) {
	// for (ParsedMapping mapping : parsedMappings)
	// mapping.prepare(parsedController, this);
	// }
	
	public HandlerXML() {
	}
	
	public HandlerXML(Handler handler) {
		this.name = handler.getName();
		this.parsedRange = new RangeXML(handler.getRange());
		for (Mapping mapping : handler.getMappings())
			parsedMappings.add(new MappingXML(mapping));
	}
}
