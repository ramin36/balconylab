package generalController.library.parser.xml.mapping;

import generalController.library.controller.Controller;
import generalController.library.map.Map;
import generalController.library.parser.parsed.ParsedController;
import generalController.library.parser.parsed.ParsedMap;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;
import lombok.Getter;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "map")
@Data
public class MapXML implements ParsedMap {
	
	@Getter
	@XmlElement(name = "controller", type = ControllerXML.class)
	private ArrayList<ParsedController>	parsedController	= new ArrayList<ParsedController>();
	
	public MapXML() {
	}
	
	public MapXML(Map map) {
		for (Controller controller : map.getController())
			parsedController.add(new ControllerXML(controller));
	}
	
	/**
	 * 
	 */
	// @Override
	// public void prepareMap() {
	// for (ParsedController controller : this.parsedController) {
	// controller.prepare();
	// // parsedMappings.addAll(controller.getMappings());
	// }
	// }
	
	@Override
	public void printParsedMap() {
		if (parsedController != null) {
			System.out.println("contoller: " + parsedController.size());
			for (ParsedController controller : this.parsedController)
				System.out.println(controller);
		}
	}
	
}
