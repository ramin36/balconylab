package generalController.library.parser.xml.value;

import generalController.library.parser.parsed.ParsedValues;
import generalController.library.values.Value;
import generalController.library.values.Values;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "values")
public class ValuesXML implements ParsedValues {
	
	@Getter
	@XmlElement(name = "value", type = ValueXML.class)
	private List<ValueXML>	parsedValues;
	
	public ValuesXML() {
	}
	
	public ValuesXML(Values values) {
		parsedValues = new ArrayList<ValueXML>();
		for (Iterator<Value> valueIter = values.getValues().iterator(); valueIter.hasNext();)
			parsedValues.add(new ValueXML(valueIter.next()));
	}
	
	public HashMap<String, ValueXML> getNamesMap() {
		HashMap<String, ValueXML> nameMap = new HashMap<String, ValueXML>();
		for (ValueXML val : parsedValues)
			nameMap.put(val.getName(), val);
		return nameMap;
	}
	
	@Override
	public void prepareValues() {
		for (ValueXML value : parsedValues)
			value.prepare();
	}
	
	@Override
	public void printParsedValues() {
		for (ValueXML v : parsedValues)
			System.out.println(v);
	}
	
}
