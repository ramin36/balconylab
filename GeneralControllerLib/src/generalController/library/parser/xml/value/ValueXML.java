package generalController.library.parser.xml.value;

import generalController.library.parser.Constants;
import generalController.library.parser.parsed.ParsedRange;
import generalController.library.parser.parsed.ParsedValue;
import generalController.library.parser.xml.RangeXML;
import generalController.library.parser.xml.adapter.FloatValueAdapter;
import generalController.library.values.Value;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import lombok.Getter;
import lombok.ToString;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "value")
@ToString
public class ValueXML implements ParsedValue {
	
	@Getter
	@XmlAttribute
	String		name;
	@Getter
	@XmlAttribute
	String		description	= "";
	@Getter
	@XmlAttribute
	String		callback	= "";
	@Getter
	@XmlAttribute
	@XmlJavaTypeAdapter(FloatValueAdapter.class)
	Float		init		= Float.NaN;
	@Getter
	@XmlElement(name = "range", type = RangeXML.class)
	ParsedRange	parsedRange;
	@Getter
	@XmlJavaTypeAdapter(TypeAdapter.class)
	@XmlAttribute
	Class<?>	type		= Float.TYPE;
	
	ValuesXML	parent;
	
	public ValueXML() {
	}
	
	public ValueXML(Value value) {
		this.description = value.getDescription();
		if (value.getCallback() != null)
			this.callback = value.getCallback().getName();
		this.init = value.getInit();
		this.name = value.getName();
		this.parsedRange = new RangeXML(value.range);
		this.type = value.getType();
	}
	
	/**
	 * not cool. bad for extending to more types
	 */
	private void defaultInit() {
		if (type == Integer.TYPE)
			init = new Float((int) (parsedRange.getMin() + parsedRange.getMax()) / 2);
		else if (type == Float.TYPE)
			init = (parsedRange.getMin() + parsedRange.getMax()) / 2;
		else if (type == Boolean.TYPE)
			init = 0f;
	}
	
	@Override
	public void prepare() {
		prepareMissingRange();
		prepareInitValue();
	}
	
	private void prepareInitValue() {
		if (Float.isNaN(init))
			defaultInit();
	}
	
	private void prepareMissingRange() {
		if (parsedRange == null)
			parsedRange = new RangeXML();
	}
	
	public static class TypeAdapter extends XmlAdapter<String, Class<?>> {
		
		@Override
		public String marshal(Class<?> val) throws Exception {
			return String.valueOf(val);
		}
		
		@Override
		public Class<?> unmarshal(String val) throws Exception {
			return Constants.getTypeOf(val);
		}
	}
	
}
