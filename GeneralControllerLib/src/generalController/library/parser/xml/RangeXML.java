package generalController.library.parser.xml;

import generalController.library.common.Range;
import generalController.library.parser.parsed.ParsedRange;
import generalController.library.parser.xml.adapter.FloatValueAdapter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import lombok.Getter;
import lombok.ToString;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "range")
@ToString
public class RangeXML implements ParsedRange {
	
	@Getter
	@XmlAttribute
	@XmlJavaTypeAdapter(FloatValueAdapter.class)
	private final Float	min;
	
	@Getter
	@XmlAttribute
	@XmlJavaTypeAdapter(FloatValueAdapter.class)
	private final Float	max;
	
	public RangeXML() {
		max = 1f;
		min = 0f;
	}
	
	public RangeXML(Range range) {
		this.max = range.getMax();
		this.min = range.getMin();
	}
	
}
