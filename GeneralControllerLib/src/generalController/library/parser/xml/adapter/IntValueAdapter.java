package generalController.library.parser.xml.adapter;

import generalController.library.parser.Constants;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class IntValueAdapter extends XmlAdapter<String, Integer> {
	
	@Override
	public String marshal(Integer val) throws Exception {
		return String.valueOf(val);
	}
	
	@Override
	public Integer unmarshal(String val) throws Exception {
		try {
			int v = Integer.valueOf(val);
			return v;
		} catch (NumberFormatException noNumber) {
			return (int) Constants.getValueOf(val);
		}
	}
}
