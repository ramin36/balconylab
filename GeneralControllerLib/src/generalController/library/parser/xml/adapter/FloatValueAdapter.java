package generalController.library.parser.xml.adapter;

import generalController.library.parser.Constants;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class FloatValueAdapter extends XmlAdapter<String, Float> {
	
	@Override
	public String marshal(Float val) throws Exception {
		return String.valueOf(val);
	}
	
	@Override
	public Float unmarshal(String val) throws Exception {
		try {
			float v = Float.valueOf(val);
			return v;
		} catch (NumberFormatException noNumber) {
			return Constants.getValueOf(val);
		}
	}
	
}
