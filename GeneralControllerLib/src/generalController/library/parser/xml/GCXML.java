package generalController.library.parser.xml;

import generalController.library.main.GC;
import generalController.library.parser.parsed.ParsedGC;
import generalController.library.parser.xml.mapping.MapXML;
import generalController.library.parser.xml.value.ValuesXML;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.ToString;

@ToString
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "gc")
public class GCXML implements ParsedGC {
	
	@Getter
	@XmlElement(name = "values")
	private ValuesXML	ParsedValues;
	
	@Getter
	@XmlElement(name = "map")
	private MapXML		ParsedMap;
	
	@Override
	public void prepare() {
		ParsedValues.prepareValues();
	}
	
	public GCXML() {
	}
	
	public GCXML(GC gc) {
		ParsedValues = new ValuesXML(gc.getValues());
		ParsedMap = new MapXML(gc.getMap());
	}
	
}
