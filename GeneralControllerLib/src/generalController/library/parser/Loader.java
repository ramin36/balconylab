package generalController.library.parser;

import generalController.library.main.GeneralController;
import generalController.library.parser.parsed.ParsedGC;
import generalController.library.parser.parsed.ParsedMap;
import generalController.library.parser.parsed.ParsedValues;

import java.io.File;

import lombok.Getter;

/**
 * Uses the right Parser (xml,later json) and keeps the parsedVaues, parsedMap objects
 * 
 * @author Ramin
 * 
 */
public class Loader {
	
	@Getter
	private ParsedValues	parsedValues;
	@Getter
	private ParsedMap		parsedMap;
	
	public void load(String fileName) {
		Parser parser = getParserFor(fileName);
		if (parser != null) {
			ParsedGC gc = parser.parseGC(getFileLocation(fileName));
			gc.prepare();
			parsedValues = gc.getParsedValues();
			parsedMap = gc.getParsedMap();
		}
	}
	
	public void load(String valuesFileName, String mapFileName) {
		loadValues(valuesFileName);
		loadMap(mapFileName);
		// connectMapToValues();
	}
	
	public void loadValues(String fileName) {
		Parser parser = getParserFor(fileName);
		if (parser != null) {
			parsedValues = parser.parseValues(getFileLocation(fileName));
			parsedValues.prepareValues();
		}
	}
	
	public void loadMap(String fileName) {
		Parser parser = getParserFor(fileName);
		if (parser != null)
			parsedMap = parser.parseMap(getFileLocation(fileName));
	}
	
	private Parser getParserFor(String fileName) {
		if (fileName.endsWith(".xml"))
			return new XMLParser();
		else if (fileName.endsWith(".json"))
			return new JSONParser();
		else {
			System.err.println("No Parser for this filetype");
			return null;
		}
	}
	
	private String getFileLocation(String fileName) {
		if (new File(fileName).isAbsolute())
			return fileName;
		else
			return GeneralController.parent().sketchPath + File.separator + "data" + File.separator
					+ fileName;
	}
}
