package generalController.library.parser;

public interface HasConstants {
	
	boolean hasConstant(String name);
	
	float getValue(String name);
	
}
