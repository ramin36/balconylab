package generalController.library.parser;

import generalController.library.parser.parsed.ParsedGC;
import generalController.library.parser.parsed.ParsedValues;
import generalController.library.parser.xml.GCXML;
import generalController.library.parser.xml.mapping.MapXML;
import generalController.library.parser.xml.value.ValuesXML;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

/**
 * Parser for XML files
 * 
 * @author Ramin
 * 
 */
public class XMLParser implements Parser {
	
	@Override
	public ParsedGC parseGC(String fileName) {
		GCXML gc = (GCXML) unmarsh(fileName, GCXML.class);
		return gc;
	}
	
	@Override
	public MapXML parseMap(String fileName) {
		MapXML map = (MapXML) unmarsh(fileName, MapXML.class);
		return map;
	}
	
	@Override
	public ParsedValues parseValues(String fileName) {
		ValuesXML vals = (ValuesXML) unmarsh(fileName, ValuesXML.class);
		return vals;
	}
	
	/**
	 * does the xml magic
	 * 
	 * @param fileName
	 *            filename
	 * @param clazz
	 *            the class that is unmarshed to
	 * @return an object of the passed (clazz) type
	 */
	private Object unmarsh(String fileName, Class<?> clazz) {
		File file = new File(fileName);
		JAXBContext jaxbContext;
		try {
			jaxbContext = JAXBContext.newInstance(clazz);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			return jaxbUnmarshaller.unmarshal(file);
		} catch (JAXBException e) {
			e.printStackTrace();
			return null;
		}
	}
	
}
