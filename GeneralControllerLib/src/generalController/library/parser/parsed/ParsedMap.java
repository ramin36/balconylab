package generalController.library.parser.parsed;

import java.util.ArrayList;

/**
 * A ParsedMap contains ParsedController objects
 * so far: mouse, and midi-controllers
 * 
 * @author Ramin
 * 
 */
public interface ParsedMap {
	
	ArrayList<? extends ParsedController> getParsedController();
	
	void printParsedMap();
	
}
