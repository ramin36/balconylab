package generalController.library.parser.parsed;

public interface ParsedValue {
	
	String getName();
	
	String getDescription();
	
	String getCallback();
	
	// String getDirectSet();
	
	Float getInit();
	
	ParsedRange getParsedRange();
	
	Class<?> getType();
	
	/**
	 * Prepares the Values. If a range is missing, the default range (0,1) is set
	 * and sets the init value if not set (to the middle of the range values)
	 */
	void prepare();
	
}
