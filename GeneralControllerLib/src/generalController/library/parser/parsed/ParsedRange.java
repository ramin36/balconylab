package generalController.library.parser.parsed;


/**
 * Ranges are used for values and for mappings.
 * They defines the range of input values(e.g. 0-127 for midi controllers)
 * and the range a value should take.
 * If a controller is triggered the controller value is mapped
 * from the mapping range to the value range
 * 
 * @author Ramin
 * 
 */
public interface ParsedRange {
	
	Float getMin();
	
	Float getMax();
	
}
