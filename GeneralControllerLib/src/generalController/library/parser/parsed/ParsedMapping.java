package generalController.library.parser.parsed;

/**
 * A Mapping is a actual mapping from a controller input to a value
 * Each mapping has a controller-id (mouse:LEFT,RIGHT),(midi:controller-id from 0-127),
 * which indicates the specific input and a valuename, of the corresponding
 * value
 * 
 * @author Ramin
 * 
 */
public interface ParsedMapping {
	
	Integer getCtrlId();
	
	String getMapTo();
	
}
