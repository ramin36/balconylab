package generalController.library.parser.parsed;

import java.util.List;

/**
 * A ParsedController is a controller which is used to set values. A Controller
 * includes Handlers, which indicate different input type of the specific controller
 * (mouse: move, drag) (midi: controllerChange, noteOn,noteOff),...
 * 
 * @author Ramin
 * 
 */
public interface ParsedController {
	
	String getName();
	
	List<? extends ParsedHandler> getHandlers();
	
}
