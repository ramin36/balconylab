package generalController.library.parser.parsed;

/**
 * GC (General Controller) holds the ParsedValues and ParsedMap object
 * 
 * @author Ramin
 * 
 */
public interface ParsedGC {
	
	void prepare();
	
	ParsedValues getParsedValues();
	
	ParsedMap getParsedMap();
	
}
