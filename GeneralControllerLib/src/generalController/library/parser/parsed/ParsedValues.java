package generalController.library.parser.parsed;

import java.util.List;

/**
 * contains the ParsedValue objects in a list
 * 
 * @author Ramin
 * 
 */
public interface ParsedValues {
	
	/**
	 * Prepares the Values. If a range is missing, the default range (0,1) is set
	 * and sets the init value if not set (to the middle of the range values)
	 */
	void prepareValues();
	
	/**
	 * returns the parsedValue objects
	 * 
	 * @return a list the parsedValue objects
	 */
	List<? extends ParsedValue> getParsedValues();
	
	void printParsedValues();
	
}
