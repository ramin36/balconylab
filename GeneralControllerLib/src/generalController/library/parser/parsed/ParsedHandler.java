package generalController.library.parser.parsed;

import java.util.ArrayList;

/**
 * A Handler contains all mappings of a specific controller of different input type of the specific controller
 * (mouse: move, drag) (midi: controllerChange, noteOn,noteOff),...
 * Each Handler comes with a range, since they are the same for each input type
 * 
 * @author Ramin
 * 
 */
public interface ParsedHandler {
	
	String getName();
	
	ParsedRange getParsedRange();
	
	ArrayList<? extends ParsedMapping> getParsedMappings();
	
}
