package generalController.library.builder;

import generalController.library.common.Range;
import generalController.library.map.Map;
import generalController.library.parser.parsed.ParsedController;
import generalController.library.parser.parsed.ParsedMap;
import generalController.library.parser.parsed.ParsedRange;
import generalController.library.parser.parsed.ParsedValue;
import generalController.library.parser.parsed.ParsedValues;
import generalController.library.values.Value;
import generalController.library.values.Values;
import lombok.Getter;

/**
 * builds Values and Mapping objects from Parsed Objects
 * 
 * @author Ramin
 * 
 */
public class Builder {
	
	@Getter
	static private Values			values			= new Values();
	@Getter
	static private Map				map				= new Map();
	
	private final ValueBuilder		valueBuilder	= new ValueBuilder();
	
	private final MappingBuilder	mappingBuilder	= new MappingBuilder(valueBuilder.namedValues);
	
	public void build(ParsedValues parsedValues, ParsedMap parsedMap) {
		buildValues(parsedValues);
		buildMappings(parsedMap);
	}
	
	private void buildValues(ParsedValues parsedValues) {
		// parsedValues.printParsedValues();
		for (ParsedValue parsedValue : parsedValues.getParsedValues())
			values.addValue(valueBuilder.buildValue(parsedValue, values.getNextId()));
	}
	
	private void buildMappings(ParsedMap parsedMap) {
		// parsedMap.printParsedMap();
		// TODO not sure is the map object is actually needed
		for (ParsedController parsedController : parsedMap.getParsedController())
			map.addController(mappingBuilder.buildController(parsedController));
	}
	
	//
	public static Range buildRange(ParsedRange parsedRange) {
		return new Range(parsedRange.getMin(), parsedRange.getMax());
	}
	
	public boolean newValueOnTheFly(String name, Class<?> type, Range range) {
		Value value = valueBuilder.newValueOnTheFly(name, type, range);
		if (value != null)
			return values.addValue(value);
		else
			return false;
	}
	
	public boolean newMappingOnTheFly(String controllerName, String handlerName, Range range,
			int controllerId, String mapTo) {
		return mappingBuilder.newMappingOnTheFly(controllerName, handlerName, controllerId, range,
				mapTo);
	}
}
