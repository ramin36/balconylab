package generalController.library.builder;

import generalController.library.common.Range;
import generalController.library.controller.Controller;
import generalController.library.controller.Handler;
import generalController.library.devices.Device;
import generalController.library.devices.DeviceManager;
import generalController.library.map.Mapping;
import generalController.library.parser.parsed.ParsedController;
import generalController.library.parser.parsed.ParsedHandler;
import generalController.library.parser.parsed.ParsedMapping;
import generalController.library.values.Value;

import java.util.Arrays;
import java.util.HashMap;

/**
 * Builds the controller, handlers and mappings
 * 
 * @author Ramin
 * 
 */
public class MappingBuilder {
	
	/**
	 * contains all available Values
	 */
	private final HashMap<String, Value>	namedValues;
	
	// TODO: necessary?
	private int								nextMappingId	= 0;
	
	public MappingBuilder(HashMap<String, Value> namedValues) {
		this.namedValues = namedValues;
	}
	
	public Controller buildController(ParsedController parsedController) {
		Device device = DeviceManager.getDevice(parsedController.getName());
		if (device == null)
			return null;
		Controller controller = DeviceManager.getController(parsedController.getName());
		for (ParsedHandler parsedHandler : parsedController.getHandlers()) {
			if (!device.hasHandler(parsedHandler.getName())) {
				printMissingHandler(device, controller, parsedHandler.getName());
				continue;
			}
			Handler handler = buildHandler(parsedHandler);
			controller.addHandler(handler);
		}
		return controller;
	}
	
	private Handler buildHandler(ParsedHandler parsedHandler) {
		Handler handler = new Handler(parsedHandler.getName(), Builder.buildRange(parsedHandler
				.getParsedRange()));
		for (ParsedMapping parsedMapping : parsedHandler.getParsedMappings()) {
			Mapping mapping = buildMapping(parsedMapping, handler.getRange());
			if (mapping != null)
				handler.addMapping(mapping);
			else
				printMissingMapTo(parsedHandler, parsedMapping);
		}
		return handler;
	}
	
	private Mapping buildMapping(ParsedMapping parsedMapping, Range range) {
		if (!namedValues.containsKey(parsedMapping.getMapTo())) {
			return null;
		} else {
			return new Mapping(parsedMapping.getCtrlId(), range, namedValues.get(parsedMapping
					.getMapTo()), nextMappingId());
		}
	}
	
	public boolean newMappingOnTheFly(String ControllerName, String handlerName, int controllerId,
			Range range, String mapTo) {
		Device device = DeviceManager.getDevice(ControllerName);
		if (device == null) {
			System.err.println("Device for Controller " + ControllerName + " does not exist");
			return false;
		}
		Controller controller = DeviceManager.getController(ControllerName);
		if (!device.hasHandler(handlerName)) {
			printMissingHandler(device, controller, handlerName);
			return false;
		}
		Handler handler = new Handler(handlerName, range);
		if (!namedValues.containsKey(mapTo)) {
			System.err.println("Value does not exist");
			return false;
		}
		Mapping mapping = new Mapping(controllerId, range, namedValues.get(mapTo), nextMappingId());
		handler.addMapping(mapping);
		controller.addHandler(handler);
		return true;
	}
	
	private int nextMappingId() {
		return nextMappingId++;
	}
	
	private void printMissingMapTo(ParsedHandler parsedHandler, ParsedMapping parsedMapping) {
		System.err.println("Handler: " + parsedHandler.getName() + " contains a Mapping: "
				+ parsedMapping + " where there is no corresponding value");
	}
	
	private void printMissingHandler(Device device, Controller controller, String handlerName) {
		System.err.println(controller.getControllerName() + " from device: "
				+ device.getDeviceName() + " does not offer handler " + handlerName
				+ ". This are the handlers:");
		System.err.println(Arrays.toString(device.handlerList()));
	}
	
}
