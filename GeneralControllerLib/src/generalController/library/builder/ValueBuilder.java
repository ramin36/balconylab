package generalController.library.builder;

import generalController.library.common.Range;
import generalController.library.main.GeneralController;
import generalController.library.parser.parsed.ParsedValue;
import generalController.library.values.Value;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;

/**
 * builds the Values
 * 
 * @author Ramin
 * 
 */
public class ValueBuilder {
	
	Class<?>							parentClass;
	
	protected HashMap<String, Value>	namedValues;
	
	public ValueBuilder() {
		parentClass = GeneralController.parent().getClass();
		namedValues = new HashMap<String, Value>();
	}
	
	public Value buildValue(ParsedValue pv, int id) {
		Range range = Builder.buildRange(pv.getParsedRange());
		Value value = new Value(pv.getName(), id, pv.getDescription(), getDirectSet(pv),
				getCallback(pv), pv.getInit(), range, pv.getType());
		namedValues.put(pv.getName(), value);
		return value;
	}
	
	private Method getCallback(ParsedValue pv) {
		// specific callback name
		if (!pv.getCallback().isEmpty())
			try {
				return parentClass.getMethod(pv.getCallback(), new Class<?>[] {});
			} catch (NoSuchMethodException noMethod) {
				System.err.println("method " + pv.getCallback() + " for value: " + pv.getName()
						+ " not found");
			}
		// callback with name same as value name
		else
			try {
				return parentClass.getMethod(pv.getName(), new Class<?>[] {});
			} catch (NoSuchMethodException e) {
			}
		return null;
	}
	
	protected Value newValueOnTheFly(String name, Class<?> type, Range range) {
		Value value = new Value(name, namedValues.size(), null, null, getCallback(name),
				range.getMid(), range, getBasicType(type));
		namedValues.put(name, value);
		return value;
	}
	
	protected Method getCallback(String valueName) {
		// callback with name same as value name
		try {
			return parentClass.getMethod(valueName, new Class<?>[] {});
		} catch (NoSuchMethodException e) {
		}
		return null;
	}
	
	private Field getDirectSet(ParsedValue pv) {
		try {
			return parentClass.getDeclaredField(pv.getName());
		} catch (NoSuchFieldException exc) {
		}
		return null;
	}
	
	private Class<?> getBasicType(Class<?> type) {
		if (type.equals(Boolean.class))
			return Boolean.TYPE;
		if (type.equals(Integer.class))
			return Integer.TYPE;
		if (type.equals(Float.class))
			return Float.TYPE;
		else
			System.err.println("Unknown type: " + type);
		return null;
	}
	
}
