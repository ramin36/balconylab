package generalController.library.test.json;

import java.io.FileNotFoundException;
import java.io.FileReader;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

public class JSONTest {
	
	public static void main(String[] args) {
		Gson gson = new Gson();
		try {
			A a = gson.fromJson(new FileReader("C:/jsontest.json"), A.class);
			System.out.println(a);
		} catch (JsonSyntaxException e) {
			e.printStackTrace();
		} catch (JsonIOException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
	}
	
}
