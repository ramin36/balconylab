package generalController.library.test;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

public class ClassLoading {
	
	public ClassLoading() {
		File file = new File(
				"C:\\Users\\Ramin\\workspace\\GeneralControllerLib\\bin\\devices\\builtin\\");
		
		try {
			// Convert File to a URL
			URL url = file.toURL(); // file:/c:/myclasses/
			URL[] urls = new URL[] { url };
			
			// Create a new class loader with the directory
			ClassLoader cl = new URLClassLoader(urls);
			
			// Load in the class; MyClass.class should be located in
			// the directory file:/c:/myclasses/com/mycompany
			Class cls = cl.loadClass("devices.builtin.MidiDevice");
		} catch (MalformedURLException e) {
		} catch (ClassNotFoundException e) {
		}
		System.out.println("done");
	}
	
	public static void main(String[] args) {
		System.out.println("jo");
		new ClassLoading();
	}
	
}
