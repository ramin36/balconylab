package generalController.library.test;

import generalController.library.devices.builtin.MidiDevice;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Arrays;

import processing.core.PApplet;

public class LoaderTest extends PApplet {
	
	@Override
	public void setup() {
		super.setup();
		Class c = load();
		try {
			MidiDevice md = (MidiDevice) c.newInstance();
			System.out.println(Arrays.toString(md.getControllerList()));
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void draw() {
		
	}
	
	public Class load() {
		File file = new File(
				"C:\\Users\\Ramin\\workspace\\GeneralControllerLib\\bin\\devices\\builtin\\");
		
		try {
			// Convert File to a URL
			URL url = file.toURI().toURL(); // file:/c:/myclasses/
			URL[] urls = new URL[] { url };
			
			// Create a new class loader with the directory
			ClassLoader cl = new URLClassLoader(urls);
			
			// Load in the class; MyClass.class should be located in
			// the directory file:/c:/myclasses/com/mycompany
			Class cls = cl.loadClass("devices.builtin.MidiDevice");
			return cls;
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		System.out.println("done");
		return null;
	}
	
}
