package generalController.library.map;

import generalController.library.controller.Controller;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;

/**
 * stores all mappings
 * TODO not sure if this is actually needed
 * 
 * @author Ramin
 * 
 */
public class Map {
	
	@Getter
	private final List<Controller>	controller	= new ArrayList<Controller>();
	
	public void addController(Controller controller) {
		this.controller.add(controller);
	}
	
}
