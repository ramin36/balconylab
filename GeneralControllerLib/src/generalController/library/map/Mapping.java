package generalController.library.map;

import generalController.library.common.Range;
import generalController.library.values.Value;
import lombok.Getter;
import lombok.ToString;
import processing.core.PApplet;

@ToString
public class Mapping {
	
	public static PApplet	ap;
	@Getter
	private final int		ctrlId;
	private final Range		range;
	/**
	 * connected value
	 */
	@Getter
	private final Value		value;
	/**
	 * TODO: necessary?
	 */
	private final int		id;
	
	public Mapping(int ctrlId, Range range, Value value, int id) {
		this.ctrlId = ctrlId;
		this.range = range;
		this.value = value;
		this.id = id;
	}
	
	/**
	 * called by the controller. Changes the value
	 * 
	 * @param val
	 *            incoming input value
	 */
	public void trigger(float val) {
		// The whole library is about this instruction
		value.set(PApplet.map(val, range.getMin(), range.getMax(), value.range.getMin(),
				value.range.getMax()));
	}
	
}
