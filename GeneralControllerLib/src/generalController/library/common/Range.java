package generalController.library.common;

import lombok.Getter;
import lombok.ToString;

/**
 * Ranges are used for values and for mappings.
 * They defines the range of input values(e.g. 0-127 for midi controllers)
 * and the range a value should take.
 * If a controller is triggered the controller value is mapped
 * from the mapping range to the value range
 * 
 * @author Ramin
 * 
 */
@ToString
public class Range {
	@Getter
	float	min;
	@Getter
	float	max;
	
	public Range() {
		this(0, 1);
	}
	
	public Range(float max) {
		this(0, max);
	}
	
	public Range(float min, float max) {
		this.min = min;
		this.max = max;
	}
	
	public float getMid() {
		return (max - min) / 2;
	}
}
