package generalController.library.recorder;

import generalController.library.values.Value;
import generalController.library.values.Values;

import java.util.ArrayList;

import lombok.Getter;

public class ProcessRecorder {
	
	public static Values		values;
	
	private int					mode;
	
	public static int			STOP	= 0, REC = 1, END = 2;
	
	@Getter
	ArrayList<ProcessRecord>	records	= new ArrayList<ProcessRecord>();
	
	static ProcessRecord		actRecord;
	
	public boolean start() {
		if (mode == STOP || mode == END) {
			actRecord = new ProcessRecord();
			records.add(actRecord);
			mode = REC;
			Value.recording = true;
			return true;
		} else
			return false;
	}
	
	public boolean stop() {
		if (mode == STOP || mode == END)
			return false;
		else {
			Value.recording = false;
			mode = END;
			// System.out.println(actRecord.toString());
			actRecord = null;
			return true;
		}
	}
	
	public static void addToRecord(int id, float value) {
		if (actRecord != null)
			actRecord.add(id, value);
	}
	
	public ProcessRecord getLastRecord() {
		if (records.size() > 0)
			return records.get(records.size() - 1);
		else
			return null;
	}
}
