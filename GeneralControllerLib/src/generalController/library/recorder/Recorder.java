package generalController.library.recorder;

import generalController.library.main.GC;
import generalController.library.main.GeneralController;
import generalController.library.values.Values;

import java.io.FileNotFoundException;
import java.util.HashMap;

import com.google.gson.JsonIOException;

/**
 * Main recorder class
 * 
 * @author Ramin
 * 
 */
public class Recorder {
	
	private Values							values;
	
	private HashMap<String, ValuesRecord>	storedStates;
	
	private int								snapshotId			= 0;
	
	ProcessRecorder							processRecorder		= new ProcessRecorder();
	
	ProcessRecordPlayer						processRecordPlayer	= new ProcessRecordPlayer();
	
	public void init(Values values) {
		this.values = values;
		ValuesRecord.values = values;
		ProcessRecorder.values = values;
		ProcessRecord.values = values;
		// ProcessRecordPlayer.values = values;
		storedStates = new HashMap<String, ValuesRecord>();
	}
	
	public void autoSave() {
		GeneralController.parent().registerMethod("dispose", this);
		autoload();
	}
	
	/**
	 * called when autoSave is called
	 */
	public void dispose() {
		ValuesRecord rec = createSnapshot();
		Printer.saveValuesRecord(rec, "autosave.txt");
	}
	
	public ValuesRecord createSnapshot() {
		ValuesRecord rec = new ValuesRecord();
		storedStates.put("snapshot-" + (snapshotId++), rec);
		return rec;
	}
	
	/**
	 * 
	 * 
	 * @param stateName
	 */
	public void setState(String stateName) {
		storedStates.get(stateName).load();
	}
	
	public void autoload() {
		try {
			values.set(RecordLoader.loadStateRecord("autosave.txt").getStorage());
		} catch (JsonIOException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			System.err.println("File not found");
		}
	}
	
	public void updateGCFile(String fileName, GC gc) {
		Printer.createGCXML(fileName, gc);
	}
	
	public boolean startRecording() {
		return processRecorder.start();
	}
	
	public boolean stopRecording() {
		return processRecorder.stop();
	}
	
	/**
	 * saves the last recording in a json file. (".json" does not need to be added)
	 * 
	 * @param recordName
	 */
	public void saveLastRecord(String recordName) {
		Printer.saveProcessRecord(processRecorder.getLastRecord(), recordName + ".json");
	}
	
	/**
	 * saves all recordings in json files. (".json" does not need to be added)
	 * 
	 * @param recordBaseName
	 */
	public void saveAllRecords(String recordBaseName) {
		int i = 0;
		for (ProcessRecord proRec : processRecorder.getRecords())
			Printer.saveProcessRecord(proRec, recordBaseName + "-" + (i++) + ".json");
	}
	
	public void playLastProcessRecord() {
		// TODO Auto-generated method stub
		
	}
}
