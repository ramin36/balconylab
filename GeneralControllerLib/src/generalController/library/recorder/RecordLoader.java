package generalController.library.recorder;

import generalController.library.recorder.ProcessRecord.PR;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;

public class RecordLoader {
	
	public static ValuesRecord loadStateRecord(String fileName) throws FileNotFoundException,
			JsonIOException, FileNotFoundException {
		Gson gson = new Gson();
		HashMap<String, Object> map = new HashMap<String, Object>();
		map = (HashMap<String, Object>) gson.fromJson(getReader(fileName), map.getClass());
		// for (String n : map.keySet())
		// System.out.println(n + " " + map.get(n));
		return new ValuesRecord(map);
	}
	
	public static ProcessRecord loadProcessRecord(String fileName) throws FileNotFoundException,
			JsonIOException, FileNotFoundException {
		Gson gson = new Gson();
		ArrayList<PR> list = new ArrayList<PR>();
		list = (ArrayList<PR>) gson.fromJson(getReader(fileName), list.getClass());
		return new ProcessRecord(list);
	}
	
	private static FileReader getReader(String fileName) throws FileNotFoundException {
		return new FileReader(fileName);
	}
	
}
