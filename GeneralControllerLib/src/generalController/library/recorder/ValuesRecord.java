package generalController.library.recorder;

import generalController.library.values.Value;
import generalController.library.values.Values;

import java.util.HashMap;

import lombok.Getter;

public class ValuesRecord {
	
	public static Values					values;
	@Getter
	private final HashMap<String, Object>	storage;
	
	public ValuesRecord() {
		storage = new HashMap<String, Object>();
		store();
	}
	
	public ValuesRecord(HashMap<String, Object> storage) {
		this.storage = storage;
	}
	
	public void store() {
		for (Value val : values.getValues())
			storage.put(val.getName(), val.get());
	}
	
	/**
	 * puts the stored values into the actual values
	 */
	public void load() {
		boolean usePickUp = Value.usePickUp;
		Value.usePickUp = false;
		for (Value val : values.getValues())
			val.set((Float) storage.get(val.getName()));
		Value.usePickUp = usePickUp;
	}
}
