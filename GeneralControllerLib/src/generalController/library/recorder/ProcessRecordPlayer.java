package generalController.library.recorder;

import generalController.library.main.GeneralController;

import java.util.ArrayList;

public class ProcessRecordPlayer {
	
	// public static Values values;
	
	ArrayList<ProcessRecordPlay>	playing	= new ArrayList<ProcessRecordPlay>(2);
	
	final static int				SINGLE	= 0, REPEAT = 1, BACK_AND_FORTH = 2,
			REPEAT_BACK_AND_FORTH = 2;
	
	private int						frame, millis;
	
	public void play(ProcessRecord record) {
		play(record, SINGLE);
	}
	
	public void play(ProcessRecord record, int mode) {
		playing.add(new ProcessRecordPlay(record, mode, record.getTimeMode(), ProcessRecord
				.getTime(record.getTimeMode())));
	}
	
	public void update() {
		frame = GeneralController.parent().frameCount;
		millis = GeneralController.parent().millis();
		for (int i = 0; i < playing.size(); i++)
			if (!playing.get(i).update()) {
				playing.remove(i--);
			}
	}
	
	private class ProcessRecordPlay {
		
		final ProcessRecord	processRecord;
		final int			playMode;
		final int			timeMode;
		final int			startTime;
		
		int					actualIndex;
		
		public ProcessRecordPlay(ProcessRecord processRecord, int mode, int timeMode, int startTime) {
			this.processRecord = processRecord;
			this.playMode = mode;
			this.timeMode = timeMode;
			this.startTime = startTime;
			actualIndex = 0;
		}
		
		public boolean update() {
			int now = timeMode == ProcessRecord.FRAMES ? frame : millis;
			if (playMode == SINGLE) {
				actualIndex = processRecord.ExecFromIndexToTime(actualIndex, now);
				return actualIndex < processRecord.size();
			}
			return true;
		}
	}
}
