package generalController.library.recorder;

import generalController.library.main.GC;
import generalController.library.parser.xml.GCXML;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Printer {
	
	/**
	 * saves a ValueRecord into a JSON file
	 * 
	 * @param record
	 * @param name
	 */
	public static void saveValuesRecord(ValuesRecord record, String fileName) {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		write(fileName, gson.toJson(record.getStorage()));
	}
	
	public static void saveProcessRecord(ProcessRecord record, String fileName) {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		if (record == null)
			System.err.println("There is nothing to save");
		else
			write(fileName, gson.toJson(record.getRec()));
	}
	
	private static void write(String fileName, String s) {
		try {
			BufferedWriter writer = newWriter(fileName);
			writer.write(s);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void createGCXML(String fileName, GC gc) {
		JAXBContext context;
		try {
			GCXML gcxml = new GCXML(gc);
			context = JAXBContext.newInstance(GCXML.class);
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			m.marshal(gcxml, newWriter(fileName));
			System.out.println();
		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}
	
	private static BufferedWriter newWriter(String fileName) {
		try {
			// File f = new File(fileName);
			// System.out.println(f.getAbsolutePath());
			return new BufferedWriter(new FileWriter(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
