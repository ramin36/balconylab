package generalController.library.recorder;

import generalController.library.main.GeneralController;
import generalController.library.values.Values;

import java.util.ArrayList;

import lombok.Getter;
import lombok.ToString;

public class ProcessRecord {
	
	public static Values		values;
	
	@Getter
	private final int			timeMode;
	
	private final int			startTime;
	
	public final static int		FRAMES	= 0, MILLIS = 1;
	
	@Getter
	private final ArrayList<PR>	rec;
	
	public ProcessRecord(ArrayList<PR> rec) {
		startTime = 0;
		timeMode = (int) rec.remove(0).val;
		this.rec = rec;
	}
	
	public ProcessRecord() {
		this(FRAMES);
	}
	
	public ProcessRecord(int mode) {
		timeMode = mode;
		startTime = getTime(timeMode);
		rec = new ArrayList<PR>();
		rec.add(new PR(0, -1, timeMode));
	}
	
	public static int getTime(int timeMode) {
		if (timeMode == FRAMES)
			return GeneralController.parent().frameCount;
		else
			return GeneralController.parent().millis();
	}
	
	public void add(int id, float value) {
		PR pr = new PR(getTime(timeMode) - startTime, id, value);
		int sz = rec.size();
		if (sz > 0 && rec.get(sz - 1).time == pr.time)
			rec.add(sz - 1, pr);
		else
			rec.add(pr);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (PR pr : rec) {
			sb.append(pr.toString() + "\n");
		}
		return sb.toString();
	}
	
	public int ExecFromIndexToTime(int index, int toTime) {
		for (; rec.get(index).time <= toTime; index++)
			rec.get(index).exec();
		return index;
	}
	
	public int size() {
		return rec.size();
	}
	
	@ToString
	public static class PR {
		
		final int	time;
		final int	id;
		final float	val;
		
		public PR(int time, int id, float val) {
			this.time = time;
			this.id = id;
			this.val = val;
		}
		
		public void exec() {
			values.getValue(id).set(val);
		}
		
	}
	
}
