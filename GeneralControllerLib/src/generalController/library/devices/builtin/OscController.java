package generalController.library.devices.builtin;

import generalController.library.controller.Controller;
import generalController.library.controller.Handler;
import generalController.library.map.Mapping;

import java.util.HashMap;

import oscP5.OscMessage;
import oscP5.OscP5;

public class OscController extends Controller {
	
	// OscP5 oscP5;
	
	HashMap<Integer, Mapping>	oscMsgs	= new HashMap<Integer, Mapping>();
	
	public OscController(String controllerName) {
		super(controllerName);
	}
	
	@Override
	public void addHandler(Handler handler) {
		super.addHandler(handler);
		int port = Integer.valueOf(handler.getName().substring(handler.getName().indexOf("-") + 1));
		new OscP5(this, port);
		for (Mapping mapping : handler.getMappings())
			oscMsgs.put(mapping.getCtrlId(), mapping);
	}
	
	public void oscEvent(OscMessage msg) {
		String adr = msg.addrPattern();
		String tag = msg.typetag();
		if (OscDevice.msgIdMap.containsKey(adr)) {
			int id = OscDevice.msgIdMap.get(adr);
			if (tag.equals("f"))
				oscMsgs.get(id).trigger(msg.get(0).floatValue());
			else if (tag.equals("i"))
				oscMsgs.get(id).trigger(msg.get(0).intValue());
		} else
			System.err.println("OSC message " + msg.addrPattern() + " is not mapped");
	}
}
