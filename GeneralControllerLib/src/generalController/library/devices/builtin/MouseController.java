package generalController.library.devices.builtin;

import generalController.library.controller.Controller;
import generalController.library.controller.Handler;
import generalController.library.main.GeneralController;
import generalController.library.map.Mapping;

import java.util.ArrayList;

import processing.event.MouseEvent;

/**
 * mouse External takes external with name mouseX, mouseY and their specific location.
 * it then distinguishes controllerIDs LEFT,RIGHT
 * 
 * @author Ramin
 * 
 */
public class MouseController extends Controller {
	
	private final ArrayList<Mapping>	moveX	= new ArrayList<Mapping>();
	private final ArrayList<Mapping>	moveY	= new ArrayList<Mapping>();
	private final ArrayList<Mapping>	dragX	= new ArrayList<Mapping>();
	private final ArrayList<Mapping>	dragY	= new ArrayList<Mapping>();
	
	private boolean						mouseRegistered;
	
	protected MouseController() {
		super("mouse");
	}
	
	private void registerMouse() {
		if (!mouseRegistered) {
			GeneralController.parent().registerMethod("mouseEvent", this);
			mouseRegistered = true;
		}
	}
	
	public void mouseEvent(MouseEvent event) {
		int x = event.getX();
		int y = event.getY();
		switch (event.getAction()) {
			case MouseEvent.PRESS:
				// do something for the mouse being pressed
				break;
			case MouseEvent.RELEASE:
				// do something for mouse released
				break;
			case MouseEvent.CLICK:
				// do something for mouse clicked
				break;
			case MouseEvent.DRAG:
				for (Mapping mapping : dragX)
					mapping.trigger(x);
				if (notifyUnmapped && dragX.isEmpty())
					System.out.println("unmapped dragX");
				for (Mapping mapping : dragY)
					mapping.trigger(y);
				if (notifyUnmapped && dragY.isEmpty())
					System.out.println("unmapped dragY");
				break;
			case MouseEvent.MOVE:
				for (Mapping mapping : moveX)
					mapping.trigger(x);
				if (notifyUnmapped && moveX.isEmpty())
					System.out.println("unmapped moveX");
				for (Mapping mapping : moveY)
					mapping.trigger(y);
				if (notifyUnmapped && moveY.isEmpty())
					System.out.println("unmapped moveY");
				break;
		}
	}
	
	@Override
	public void addHandler(Handler handler) {
		super.addHandler(handler);
		registerMouse();
		if (handler.getName().equals("moveX"))
			for (Mapping mapping : handler.getMappings())
				moveX.add(mapping);
		if (handler.getName().equals("moveY"))
			for (Mapping mapping : handler.getMappings())
				moveY.add(mapping);
		if (handler.getName().equals("dragX"))
			for (Mapping mapping : handler.getMappings())
				dragX.add(mapping);
		if (handler.getName().equals("dragY"))
			for (Mapping mapping : handler.getMappings())
				dragY.add(mapping);
	}
	
}
