package generalController.library.devices.builtin;

import generalController.library.controller.Controller;
import generalController.library.devices.Device;

import java.util.Arrays;

public class MouseDevice implements Device {
	
	private final String	deviceName		= "mouse";
	private final String[]	controller		= { "mouse" };
	
	private final String[]	handlerNames	= { "moveX", "moveY", "dragX", "dragY", "wheel" };
	
	MouseController			mouseController;
	
	@Override
	public String getDeviceName() {
		return deviceName;
	}
	
	@Override
	public String[] getControllerList() {
		return controller;
	}
	
	@Override
	public Controller getController(String controllerName) {
		if (mouseController == null)
			mouseController = new MouseController();
		return mouseController;
	}
	
	@Override
	public boolean hasHandler(String handlerName) {
		return Arrays.asList(handlerNames).contains(handlerName);
	}
	
	@Override
	public String[] handlerList() {
		return handlerNames;
	}
	
}
