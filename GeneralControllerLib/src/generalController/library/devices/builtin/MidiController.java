package generalController.library.devices.builtin;

import generalController.library.controller.Controller;
import generalController.library.controller.Handler;
import generalController.library.map.Mapping;

import java.util.HashMap;

public class MidiController extends Controller {
	
	// TODO: channels are ignored so far
	
	HashMap<Integer, Mapping>	controllerChange	= new HashMap<Integer, Mapping>();
	HashMap<Integer, Mapping>	noteOff				= new HashMap<Integer, Mapping>();
	HashMap<Integer, Mapping>	noteOn				= new HashMap<Integer, Mapping>();
	
	public MidiController(String name) {
		super(name);
	}
	
	public void controllerChange(int channel, int number, int value) {
		// System.out.println("controllerChange " + channel + " " + number + " " + value + " ");
		Mapping mapping = controllerChange.get(number);
		if (mapping != null)
			mapping.trigger(value);
		else if (notifyUnmapped)
			System.out.println("Unampped controllerChange channel:" + channel + " number:" + number
					+ " value:" + value);
	}
	
	public void noteOff(int channel, int pitch, int velocity) {
		// System.out.println("noteOff " + channel + " " + pitch + " " + velocity);
		Mapping mapping = noteOff.get(pitch);
		if (mapping != null)
			mapping.trigger(velocity);
		else if (notifyUnmapped)
			System.out.println("Unampped noteOff channel:" + channel + " pitch:" + pitch
					+ " velocity" + velocity);
	}
	
	public void noteOn(int channel, int pitch, int velocity) {
		// System.out.println("noteOn " + channel + " " + pitch + " " + velocity);
		Mapping mapping = noteOff.get(pitch);
		if (mapping != null)
			mapping.trigger(velocity);
		else if (notifyUnmapped)
			System.out.println("Unampped noteOn channel:" + channel + " pitch:" + pitch
					+ " velocity" + velocity);
	}
	
	@Override
	public void addHandler(Handler handler) {
		super.addHandler(handler);
		HashMap<Integer, Mapping> map = null;
		if (handler.getName().equals("controllerChange"))
			map = controllerChange;
		else if (handler.getName().equals("noteOff"))
			map = noteOff;
		else if (handler.getName().equals("noteOn"))
			map = noteOn;
		for (Mapping mapping : handler.getMappings()) {
			map.put(mapping.getCtrlId(), mapping);
			// System.out.println("ids added: " + mapping.getCtrlId() + " " + mapping);
		}
	}
	
}
