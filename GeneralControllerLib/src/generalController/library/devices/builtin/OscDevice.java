package generalController.library.devices.builtin;

import generalController.library.controller.Controller;
import generalController.library.devices.Device;
import generalController.library.parser.HasConstants;

import java.util.HashMap;
import java.util.regex.Pattern;

public class OscDevice implements Device, HasConstants {
	
	final String								name			= "osc";
	
	final String[]								controller		= { "osc" };
	
	final String[]								handlerNames	= { "port-xxxxx" };
	
	private int									nextCtrlId		= 0;
	
	protected static HashMap<String, Integer>	msgIdMap		= new HashMap<String, Integer>();
	
	OscController								oscController;
	
	@Override
	public String getDeviceName() {
		return name;
	}
	
	@Override
	public String[] getControllerList() {
		return controller;
	}
	
	@Override
	public Controller getController(String controllerName) {
		if (oscController == null)
			oscController = new OscController(controllerName);
		return oscController;
	}
	
	@Override
	public String[] handlerList() {
		return handlerNames;
	}
	
	@Override
	public boolean hasHandler(String handlerName) {
		return Pattern.matches("port-\\d+", handlerName);
	}
	
	@Override
	public boolean hasConstant(String name) {
		return name.startsWith("/");
	}
	
	@Override
	public float getValue(String name) {
		msgIdMap.put(name, nextCtrlId);
		return nextCtrlId++;
	}
	
}
