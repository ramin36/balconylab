package generalController.library.devices.builtin;

import generalController.library.controller.Controller;
import generalController.library.devices.Device;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import themidibus.MidiBus;

public class MidiDevice implements Device {
	
	private static String			deviceName		= "midi";
	
	private static String[]			handlerNames	= { "controllerChange", "noteOff", "noteOn" };	// { "controllerChange-#", "noteOff-#", "noteOn-#" };
																									
	// TODO: later: get the channel by the handlername
	HashMap<String, MidiController>	createdController;
	
	@Override
	public String getDeviceName() {
		return deviceName;
	}
	
	@Override
	public String[] getControllerList() {
		return MidiBus.availableInputs();
	}
	
	@Override
	public Controller getController(String controllerName) {
		MidiController controller;
		controller = getHahedController(controllerName);
		if (controller != null)
			return controller;
		controller = new MidiController(controllerName);
		MidiBus midibus = new MidiBus(controller);
		midibus.addInput(controllerName);
		createdController.put(controllerName, controller);
		return controller;
	}
	
	private MidiController getHahedController(String controllerName) {
		if (createdController == null)
			createdController = new HashMap<String, MidiController>();
		return createdController.get(controllerName);
	}
	
	@Override
	public boolean hasHandler(String handlerName) {
		int indexOfMinus = handlerName.indexOf("-");
		String pureName;
		if (indexOfMinus == -1) // accept it anyways
			pureName = handlerName;
		else
			try {
				pureName = handlerName.substring(0, indexOfMinus);
				// System.out.println(pureName);
				Integer.parseInt(handlerName.substring(indexOfMinus));
			} catch (NumberFormatException exc) {
				return false;
			}
		List<String> handlerNames = Arrays.asList(MidiDevice.handlerNames);
		return handlerNames.contains(pureName);
	}
	
	@Override
	public String[] handlerList() {
		return handlerNames;
	}
	
}
