package generalController.library.devices;

import generalController.library.controller.Controller;

public interface Device {
	
	String getDeviceName();
	
	String[] getControllerList();
	
	Controller getController(String controllerName);
	
	String[] handlerList();
	
	boolean hasHandler(String handlerName);
	
}
