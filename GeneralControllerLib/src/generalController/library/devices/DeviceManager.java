package generalController.library.devices;

import generalController.library.controller.Controller;

import java.util.HashMap;

/**
 * Contains all possible Devices. To extend the library by adding new controller (libraries) to it
 * create a class implementing the Device interface, which creates the connection to the controller
 * and a Class that inherits from the Controller class and put them into the builtin package.
 * 
 * @author Ramin
 * 
 */
public class DeviceManager {
	
	/**
	 * contains all possible controller, provided by the Devices
	 */
	private static HashMap<String, Device>	controllerList	= DeviceLoader
																	.loadAvailableController();
	
	/**
	 * Returns the device for a passed Controllername (e.g Mouse, AKAI-MidiKeyBoard,...)
	 * 
	 * @param controllerName
	 * @return
	 */
	public static Device getDevice(String controllerName) {
		Device device = controllerList.get(controllerName);
		if (device == null) {
			System.err.println("There is no Device offering controller: " + controllerName
					+ ". Available controllers are: ");
			System.err.println(controllerList.keySet().toString());
			return null;
		}
		return device;
	}
	
	/**
	 * Gets a Controller from the corresponding device
	 * 
	 * @param controllerName
	 *            name of the controller
	 * @return an object that inherits from the abstract class Controller
	 */
	public static Controller getController(String controllerName) {
		Device device = getDevice(controllerName);
		return device.getController(controllerName);
	}
	
}
