package generalController.library.devices;

import generalController.library.devices.builtin.MidiDevice;
import generalController.library.devices.builtin.MouseDevice;
import generalController.library.devices.builtin.OscDevice;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Loads the device. It was initially intended that the classes are dynamically loaded, so that new devices classes
 * could be written without changing this library.
 * but this doesnt work atm. So all new Device/Controller classes need to be in the library in the devices.builtin package
 * 
 * @author Ramin
 * 
 */
public class DeviceLoader {
	
	public static ArrayList<Class<? extends Device>>	deviceClasses	= new ArrayList<Class<? extends Device>>();
	
	public static HashMap<String, Device> loadAvailableController() {
		deviceClasses.add(MidiDevice.class);
		deviceClasses.add(MouseDevice.class);
		deviceClasses.add(OscDevice.class);
		return getControllerMapFromDeviceList(deviceClasses);
	}
	
	// DYNAMIC DEVICE CLASS LOADER
	
	/**
	 * OLD VERSION. LOADS Device classes from a folder. not working in packes;
	 * loads all available controllers, from the class in this package
	 * TODO needs to work in a jar file as well // OS
	 * TODO add external classes on the fly
	 */
	public static HashMap<String, Device> loadAvailableControllerOld() {
		// TreeSet<Device> devices = new TreeSet<Device>();
		File folder = getFolder();
		Class<? extends Device>[] deviceClasses = loadEXTClasses(folder, "Device");
		return getControllerMapFromDeviceList(Arrays.asList(deviceClasses));
	}
	
	private static HashMap<String, Device> getControllerMapFromDeviceList(
			List<Class<? extends Device>> deviceClasses) {
		HashMap<String, Device> controller = new HashMap<String, Device>();
		for (Class<? extends Device> clazz : deviceClasses) {
			Device device;
			try {
				device = clazz.newInstance();
				for (String controllerName : device.getControllerList()) {
					controller.put(controllerName, device);
				}
			} catch (Exception e) {
			}
		}
		return controller;
	}
	
	private static File getFolder() {
		String folder = Device.class.getPackage().getName() + "/builtin";
		folder = folder.replaceAll("\\.", "/");
		return new File(folder);
	}
	
	@SuppressWarnings("unchecked")
	private static Class<? extends Device>[] loadEXTClasses(File path, String interfaceName) {
		String actClass = "";
		try {
			ArrayList<Class<? extends Device>> classlist = new ArrayList<Class<? extends Device>>();
			// Find all classes in the given folder
			ArrayList<String> listFiles = new ArrayList<String>();
			String[] files = path.list();
			for (String s : files) {
				// System.out.println(s);
				if (s.toLowerCase().endsWith(".class")) {
					listFiles.add(s.substring(0, s.length() - 6));
				}
			}
			// Change replacement depending on OS
			String pckg = path.getPath().replace(System.getProperty("file.separator"), ".") + ".";
			// Classloader to access and instanciate classes
			URL urlpath = path.toURI().toURL();
			URLClassLoader loader = new URLClassLoader(new URL[] { urlpath });
			for (int i = 0; i < listFiles.size(); i++) {
				actClass = listFiles.get(i);
				Class<?> cls = loader.loadClass(pckg + actClass);
				if (ClassImplements(cls, interfaceName)) {
					// if (cls.getSuperclass().getSimpleName().equals(superclassName))
					// System.out.println("added : " + cls);
					classlist.add((Class<? extends Device>) cls);
				}
			}
			loader.close();
			return classlist.toArray(new Class[0]);
			
		} catch (Exception e) {
			// e.printStackTrace();
			System.out.println("Class Loading Error! " + actClass);
		}
		return null;
	}
	
	private static boolean ClassImplements(Class<?> cls, String superclassName) {
		Class<?>[] interfaces = cls.getInterfaces();
		for (Class<?> interf : interfaces)
			if (interf.getSimpleName().equals(superclassName))
				return true;
		return false;
	}
	
}
