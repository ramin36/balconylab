package generalController.library.controller;

import generalController.library.common.Range;
import generalController.library.map.Mapping;

import java.util.ArrayList;

import lombok.Getter;

/**
 * A Hanlder holds all mappings from one input type(mouse:move, dragged),(midi:controllerChange,noteOn,noteOff),...
 * A Handler also has a range, since all inputs from one type have the same range
 * 
 * @author Ramin
 * 
 */
public class Handler {
	
	@Getter
	String				name;
	@Getter
	Range				range;
	@Getter
	ArrayList<Mapping>	mappings	= new ArrayList<Mapping>();
	
	public Handler(String name, Range range) {
		this.name = name;
		this.range = range;
	}
	
	public void addMapping(Mapping mapping) {
		mappings.add(mapping);
	}
}