package generalController.library.controller;

import generalController.library.main.Properties;

import java.util.ArrayList;

import lombok.Getter;

/**
 * A Controller handles the mappings. If a handler is added all included mappings are connected so
 * that they trigger to incoming inputs
 * 
 * @author Ramin
 * 
 */
public abstract class Controller {
	
	@Getter
	private final String				controllerName;
	
	// TODO implement that into the parser, or setable over the main GC-class
	// @Setter
	static protected boolean			notifyUnmapped	= Properties.notifyUnmapped;
	
	/**
	 * only used to create xml or json files.
	 */
	@Getter
	private final ArrayList<Handler>	handlers		= new ArrayList<Handler>();
	
	public Controller(String controllerName) {
		this.controllerName = controllerName;
	}
	
	/**
	 * takes the mappings out of the handler and puts them in its structure
	 * 
	 * @param handler
	 */
	public void addHandler(Handler handler) {
		handlers.add(handler);
	}
	
}
